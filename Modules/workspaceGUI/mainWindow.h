#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QMenu>
#include <QAction>
#include <QMenuBar>
#include <QString>
#include <QFileDialog>
#include <QSettings>
#include "configuration.h"
#include "workspaceEditor.h"

extern GlobalConfiguration *globalConf;
extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;
extern HardwareConfiguration *hardwareConf;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QStringList arguments, QWidget *parent = 0);

    WorkspaceEditor *workspaceGui;
    QVBoxLayout *mainLayout;

    QMenu *menuSaveLoad;
    QMenu *menuSave;
    QAction *actionSave;
    QAction *actionSaveAs;
    QAction *actionLoad;

};

#endif // MAINWINDOW_H
