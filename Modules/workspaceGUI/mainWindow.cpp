#include "mainWindow.h"

MainWindow::MainWindow(QStringList arguments, QWidget *parent) : QMainWindow(parent)
{
    networkConf=NULL;
    moduleConf=NULL;
    nTrodeTable=NULL;
    streamConf=NULL;
    spikeConf=NULL;
    headerConf=NULL;
    hardwareConf=NULL;
    globalConf=NULL;


    workspaceGui = new WorkspaceEditor(M_STAND_ALONE, this);

    menuSaveLoad = new QMenu();
    menuSaveLoad->setTitle("Save/Load");
    menuSaveLoad->setEnabled(true);
    menuBar()->addAction(menuSaveLoad->menuAction());

    menuSave = new QMenu();
    menuSave->setTitle("Save");
    menuSaveLoad->addAction(menuSave->menuAction());

    actionSave = new QAction(0);
    actionSave->setText("Save");
    actionSave->setShortcut(QKeySequence(tr("Ctrl+S")));
    addAction(actionSave);
    actionSaveAs = new QAction(0);
    actionSaveAs->setText("Save As");
    actionSaveAs->setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
    addAction(actionSaveAs);
    menuSave->addAction(actionSave);
    menuSave->addAction(actionSaveAs);

    actionLoad = new QAction(0);
    actionLoad->setText("Load");
    actionLoad->setShortcut(QKeySequence(tr("Ctrl+O")));
    addAction(actionLoad);
    menuSaveLoad->addAction(actionLoad);

    connect(actionSave, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveToXML()));
    connect(actionSaveAs, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveAsToXML()));
    connect(actionLoad, SIGNAL(triggered(bool)), workspaceGui, SLOT(buttonLoadPressed()));

    setCentralWidget(workspaceGui);
    setWindowTitle(tr("Workspace Editor"));

}
