#ifndef AVTWRAPPER_H
#define AVTWRAPPER_H

#include <QtWidgets>
#include "abstractCamera.h"

#ifdef __APPLE__
#define _OSX
#define _x64
#endif

#ifdef linux
#define _LINUX
#define _x64
#endif

#ifdef WIN32
#define _WINDOWS
#endif

#ifdef _WINDOWS
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define PVDECL           __stdcall
//#include "StdAfx.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Winsock2.h>
#include <Windows.h>
#endif

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

#include <PvApi.h>
#include <ImageLib.h>

#ifdef _WINDOWS
#define STDCALL __stdcall
#else
#define STDCALL
//#define TRUE     0
#endif



#define MAX_CAMERA_LIST 20
// number of frames to be used
#define FRAMESCOUNT 20

// camera's data
typedef struct
{
    unsigned long   UID;
    tPvHandle       Handle;
    tPvFrame        Frames[FRAMESCOUNT];
    tPvUint32       Counter;
    bool            Abort;
    unsigned long   Discarded; //Count of missing frames.
    char            Filename[20];

} tCamera;

struct CameraIPName
{
    QString cameraName;
    unsigned long ipAddress;
    QString firmware;
};

class AvtCameraController : public AbstractCamera {

Q_OBJECT

public:
    AvtCameraController(bool ptpenabled=false);
    ~AvtCameraController();
    QStringList availableCameras(); //Get a list of available device names

private:
    tCamera cameraInfo;
    bool isCameraOpen;
    bool isCameraAcquiring;
    bool initialized;
    bool isBlackAndWhite;
    bool bayerDemosaicNeeded;
    QTimer frameTriggerTimer;
    QVector<CameraIPName> detectedCameras;
    QVector<QRgb> colorTable;
    int frameIndex;
    QTimer blinkTimer;
    QTimer startBlinkTimer;

    int oldFrameCount;
    int currentCameraID;

    bool hasReset;

    bool ptpChecksEnabled;
    bool ptpCapableCamera;
    bool ptpInitialized;
    QTimer ptpChecker;
    void resetSystem();

public slots:
    bool open(int cameraID); //opens a camera using the index of the list from availableCameras()
    void close(); //closes the camera
    bool start(); //starts acquistion
    void stop(); //stops acquisition
    void blinkAcquire();
    void resetCurrentCamera();


private slots:
//    void getFrame();
    void getAvailableFrames();
    void endBlink();
    void startBlink();

    void ResetPTP();
    void checkPTPStatus();

signals:
    void PTPFullyInitialized();
    void PTPInitializing();
    void PTPCalibrating();
    void PTPNotProperlySetup();
    void PTPWrongConfiguration();
    void PTPWarningMessage(QString);
};

#endif // AVTWRAPPER_H
