/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QPainter>
#include "videoEncoder.h"


X264VideoEncoder::X264VideoEncoder(QObject *parent) {

   //initVars();
   //initCodec();
    videoFileOpen=false;
    timestampFileOpen = false;
    convertCtx = NULL;
    picture_buf=NULL;
    videoFile = NULL;

}

X264VideoEncoder::~X264VideoEncoder() {

}

void X264VideoEncoder::endThread() {
    emit finished();
}

bool X264VideoEncoder::createFile(QString fileName,unsigned width,unsigned height,AbstractCamera::videoFmt format,unsigned fps)
{
   //This function sets up a new encoder, and opens a file to dump the encoded movie in.

   Width=width;
   Height=height;
   qDebug() << "Video encoder: creating file with width " << width << "and height " << height;
   // very fast is slower than ultrafast, but better quality
   //x264_param_default_preset(&param, "veryfast", "zerolatency");
   //x264_param_default_preset(&param, "ultrafast", "zerolatency");
   x264_param_default_preset(&param, "superfast", "zerolatency");

   param.b_vfr_input = 0; // Variable framerate (vfr) causes a 1-frame lag in encoder, drops last frame in stream

   param.i_threads = 1;
   param.i_width = width;
   param.i_height = height;
   param.i_fps_num = fps; //frame rate.  This sets the playback speed.  TODO: make non-hardcoded
   param.i_fps_den = 1;
   param.i_timebase_num = fps;
   param.i_timebase_den = 1;

   // Intra refres:
   param.i_keyint_max = fps; //max number of frames between key frames (should be between fps and 2*fps)
   param.b_intra_refresh = 1;
   //Image quality control:
   param.rc.i_rc_method = X264_RC_CRF; //rate control, CQP (constant mass), CRF (constant bit rate), ABR (average bit rate)

   param.rc.f_rf_constant = 25; //25 is high quality, 35 is fairly low
   param.rc.f_rf_constant_max = 35;//doing above plus 10
   //For streaming:
   param.b_repeat_headers = 1;
   param.b_annexb = 1;

   //new
   param.i_csp = X264_CSP_I420;

   x264_param_apply_profile(&param, "baseline");


   //Create encoder
   encoder = x264_encoder_open(&param);
   pic_in = new x264_picture_t;
   pic_out = new x264_picture_t;

   //allocate memory for one input frame
   x264_picture_alloc(pic_in, param.i_csp, Width, Height);

   switch (format) {
   case AbstractCamera::Fmt_RGB32:
	   qDebug() << "Setting format to RGB32";
       convertCtx = sws_getContext(Width, Height, AV_PIX_FMT_BGRA, Width, Height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
       break;
   case AbstractCamera::Fmt_RGB24:
	   qDebug() << "Setting format to RGB24";
       convertCtx = sws_getContext(Width, Height, AV_PIX_FMT_RGB24, Width, Height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
       break;
   case AbstractCamera::Fmt_ARGB32:
       qDebug() << "Setting format to ARGB32";
       convertCtx = sws_getContext(Width, Height, AV_PIX_FMT_BGRA, Width, Height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
       break;

   default:
       qDebug() << "Error: encoder received invalid format";
       return false;
   }

   //convertCtx = sws_getContext(Width, Height, PIX_FMT_RGB24, out_w, out_h, PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
   //convertCtx = sws_getContext(Width, Height, PIX_FMT_BGRA, Width, Height, PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);
   //convertCtx = sws_getContext(Width, Height, PIX_FMT_RGB24, Width, Height, PIX_FMT_YUV420P, SWS_FAST_BILINEAR, NULL, NULL, NULL);

   videoFile = new QFile;
   videoFile->setFileName(fileName);

   //Create file
   if (!videoFile->open(QIODevice::WriteOnly)) {
       qDebug() << "Video file " << videoFile->fileName() << "could not be opened";
       return false;
   }

   pts = 0;

   videoFileOpen = true;

   return true;


}

bool X264VideoEncoder::encodeImage(const QImage &img) {
    //Function to encode one frame and write to disk
    if (videoFileOpen) {

        if(img.isNull()){
            qDebug() << "Error: QImage is null, cannot encode frame to file";
            return false;
        }
        //Convert QImage to x264_picture_t format
        uint8_t *srcplanes[3];
        srcplanes[0]=(uint8_t*)img.bits();
        srcplanes[1]=0;
        srcplanes[2]=0;

        int srcstride[3];
        srcstride[0]=img.bytesPerLine();
        srcstride[1]=0;
        srcstride[2]=0;

        sws_scale(convertCtx, srcplanes, srcstride, 0, Height, pic_in->img.plane, pic_in->img.i_stride);

        //pts is important for correct playback rate.  Might not be correct.
        //pic_in->i_pts = pts;
        ++pts; // increment timestamp even if encode fails
        pic_in->i_dts = pts;


        //Encode the image
        x264_nal_t* nals = NULL;
        int num_nals = 0;
        int frame_size = x264_encoder_encode(encoder, &nals, &num_nals, pic_in, pic_out);

        if(frame_size < 0) {
            qDebug() << "error: x264_encoder_encode failed.";
            return false;
        }

       if(!nals) {
            qDebug() << "error: x264_encoder_encode returned no valid nals.";
            return false;
       }

       //Write the encoded image to disk
       if(videoFile->write((char*)nals[0].p_payload,frame_size) <= 0) {
           qDebug() << "Error while trying to write nal";
           return false;
       }

       return true;

    } else {
        return false;
    }

}

bool X264VideoEncoder::close() {

    if(encoder) {
        //Encode the image
        x264_nal_t* nals = NULL;
        int num_nals = 0;

        //Flush any delayed frames
        while( x264_encoder_delayed_frames( encoder ) ) {
            int frame_size = x264_encoder_encode(encoder, &nals, &num_nals, NULL, pic_out);
            if(!videoFile->write((char*)nals[0].p_payload,frame_size)) {
                qDebug() << "Error flushing frames to file";
                break;
            }
        }

        videoFile->flush();

        x264_encoder_close(encoder);
        x264_picture_clean(pic_in);

        encoder = NULL;
    }

  if (picture_buf) {
    delete[] picture_buf;
    picture_buf=0;
  }

  if(convertCtx) {
    sws_freeContext(convertCtx);
    convertCtx = NULL;
  }

  if(videoFile) {
    videoFile->close();
    delete videoFile;
  }

  if (timestampFileOpen) {
      timestampFile->flush();
      hwSyncFile->flush();
      timestampFile->close();
      hwSyncFile->close();

      delete timestampFile;
      delete hwSyncFile;
      timestampFileOpen = false;
  }

  videoFileOpen=false;

  return true;
}


bool X264VideoEncoder::createTimestampFile(QString filenameIn, quint32 clockRate) {
    //Create a new timestmap file

    if (!timestampFileOpen) {

        timestampFile = new QFile;
        timestampFile->setFileName(filenameIn);

        hwSyncFile = new QFile;
        hwSyncFile->setFileName(filenameIn+".cameraHWSync");

        //Create file
        if (!timestampFile->open(QIODevice::WriteOnly)) {
            return false;
        }
        if (!hwSyncFile->open(QIODevice::WriteOnly)) {
            return false;
        }


        timestampFileOpen = true;
        timestampFile->write("<Start settings>\n");
        QString clockRateLine = QString("Clock rate: %1\n").arg(clockRate);
        timestampFile->write(clockRateLine.toLocal8Bit());
        timestampFile->write("<End settings>\n");

        hwSyncFile->write("<Start settings>\n");
        clockRateLine = QString("Clock rate: %1\n").arg(clockRate);
        hwSyncFile->write(clockRateLine.toLocal8Bit());
        QString fieldsLine = QString("Fields: <PosTimestamp uint32><HWframeCount uint32><HWTimestamp uint64>\n");
        hwSyncFile->write(fieldsLine.toLocal8Bit());
        hwSyncFile->write("<End settings>\n");

    }

    return true;
}


void X264VideoEncoder::writeTimestamp(quint32 t,quint32 hwFrameCount,quint64 hwTimestamp) {
    if (timestampFileOpen) {

        //char *charPtr = (char*)(&t);
        //outStream->writeRawData(charPtr,4);
        QDataStream outStream(timestampFile); //link outStream to the file
        outStream.setByteOrder(QDataStream::LittleEndian);
        outStream << t;
        timestampFile->flush();

        QDataStream outStream2(hwSyncFile);
        outStream2.setByteOrder(QDataStream::LittleEndian);
        outStream2 << t << hwFrameCount << hwTimestamp;
        hwSyncFile->flush();

    }
}






