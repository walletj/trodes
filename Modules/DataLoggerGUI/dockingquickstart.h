#ifndef DOCKINGQUICKSTART_H
#define DOCKINGQUICKSTART_H

#include <QtWidgets>
#include "disklistwidget.h"

class DockingSettings : public QWidget
{
    Q_OBJECT
public:
    DockingSettings(LoggerRecording rec, QWidget *parent = nullptr);
};

class DockingQuickstart : public QWidget
{
    Q_OBJECT
public:
    explicit DockingQuickstart(LoggerRecording rec, QWidget *parent = nullptr);
    ~DockingQuickstart();

signals:

    void okPressed(QString temprootname);
public slots:

private:
    LoggerRecording rec;
    QLineEdit *chanmapLineEdit;
    QLineEdit *fileLineEdit;
    void loadSettings();
    void saveSettings();

private slots:
    void AcceptAndStart();
    void SelectRecPath();
};

#endif // DOCKINGQUICKSTART_H
