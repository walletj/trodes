#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include <QtWidgets>

class FileBrowser : public QWidget
{
    Q_OBJECT
public:
    explicit FileBrowser(const QString name, const QString baseDir, bool showfiles = true, QWidget *parent = nullptr);
    QString getBasePath() const;
    QString getSelectedPath() const;
    void setSelectedPath(const QString &value);
    void setBasePath(const QString &value);

    int getColumnWidth() const;
    void setColumnWidth(int w);
private:
    QString myName;
    QString basePath;
    QString selectedPath;

    QVBoxLayout         *layout;
    QLabel              *selectedDirLabel;
    QFileSystemModel    *dirmodel;
    QTreeView           *fileBrowser;

    QString resolvePath(const QString &path);
signals:
    void pathSelected(const QString directory);

public slots:
    void navigateToPath(const QString &path);

private slots:
    void rowClicked(const QModelIndex &index);
    void rowDoubleClicked(const QModelIndex &index);
};

#endif // FILEBROWSER_H
