#ifndef DOCKCONFIGWIDGET_H
#define DOCKCONFIGWIDGET_H

#include <QtWidgets>

class DockConfigWidget : public QDialog
{
    Q_OBJECT
public:
    explicit DockConfigWidget(QWidget *parent = nullptr);
    void accept();

signals:

public slots:

private:
    QVector<QLabel*> labels;
    QVector<QLineEdit*> textfields;
};

#endif // DOCKCONFIGWIDGET_H
