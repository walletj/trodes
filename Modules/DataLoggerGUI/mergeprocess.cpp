#include "mergeprocess.h"

MergeProcess::MergeProcess(QString datfile, QString recfile, QString trodesconf, QString outfile, int numchan, QWidget *parent)
    : AbstractProcess("MergeSDRecording", parent),
      datFile(QDir::toNativeSeparators(datfile)),
      recFile(QDir::toNativeSeparators(recfile)),
      trodesConf(QDir::toNativeSeparators(trodesconf)),
      mergedOutputFile(QDir::toNativeSeparators(outfile)),
      numchan(numchan)

{
}

void MergeProcess::start(){
    start_merge();
}

void MergeProcess::start_merge(){
//    QDir outdir(QFileInfo(recFile).absoluteDir());
    QStringList args;
    args << "-rec" << QFileInfo(recFile).filePath();
    args << "-sd" << QFileInfo(datFile).filePath();
    args << "-mergeconf" << QFileInfo(trodesConf).filePath();
    args << "-numchan" << QString::number(numchan);
    args << "-output" << QFileInfo(mergedOutputFile).baseName();
    args << "-outputdirectory" << QFileInfo(mergedOutputFile).absoluteDir().absolutePath() + "/";
//    args << "-output" << outdir.relativeFilePath(mergedOutputFile); //hack to get mergesdrecording to work properly and write file to correct directory
//    qDebug() << QString(".") + QDir::separator() + "mergesdrecording" << args;
    emit newOutputLine(QString(".") + QDir::separator() + "mergesdrecording "+args.join(" "));
    process->start(QString(".") + QDir::separator() + "mergesdrecording", args);
}

void MergeProcess::moveFile(int code){
//    if(code == 0){
//        QDir dummy;
//        QFile out(outFile);
//        if(out.exists()){
//            out.remove();
//        }
//        if(!dummy.rename(temppath, outFile)){
//            emit processfinished(-1);
//        }
//    }
}
