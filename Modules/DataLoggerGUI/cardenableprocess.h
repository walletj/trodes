#ifndef CARDENABLEPROCESS_H
#define CARDENABLEPROCESS_H

#include "abstractprocess.h"

class CardEnableProcess : public AbstractProcess
{
    Q_OBJECT
public:
    explicit CardEnableProcess(QString device, QWidget *parent = nullptr);
    void start();

private:
    QString device;
    void customReadOutput(const QString &line);
    void win_start_cardenable();
    void lin_start_cardenable();
    void mac_start_cardenable();
    void docking_cardenable();
};

#endif // CARDENABLEPROCESS_H
