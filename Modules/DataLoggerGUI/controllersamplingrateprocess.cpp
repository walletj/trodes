#include "controllersamplingrateprocess.h"

ControllerSamplingRateProcess::ControllerSamplingRateProcess(QWidget *parent)
    : AbstractProcess ("ControllerSamplingRate", parent),
      mcu_success(false),
      dock_success(false),
      setting_khzrate(false),
      input_khzrate(30)
{

}

ControllerSamplingRateProcess::ControllerSamplingRateProcess(int khzrate, QWidget *parent)
    : AbstractProcess ("ControllerSamplingRate", parent),
      mcu_success(false),
      dock_success(false),
      setting_khzrate(true),
      input_khzrate(khzrate)
{

}

void ControllerSamplingRateProcess::start()
{
    //attempt both, and report which ones were successful
    dock_start_khzrate();
    waitForFinished(2000);
    if(process->exitCode()==0){
        dock_success = true;
    }
    mcu_start_khzrate();
    waitForFinished(2000);
    if(process->exitCode()==0){
        mcu_success = true;
    }
}

void ControllerSamplingRateProcess::mcu_start_khzrate()
{
    QStringList args;
    if(setting_khzrate){
        args << QString::number(input_khzrate);
    }

    process->start(MCUUTILPATH + "/set_samplingrate_mcu", args);
}

void ControllerSamplingRateProcess::dock_start_khzrate()
{
    QStringList args;
    if(setting_khzrate){
        args << "-k" << QString::number(input_khzrate);
    }

    process->start(DOCKINGPATH, args);
}

void ControllerSamplingRateProcess::customReadOutput(const QString &line)
{

}
