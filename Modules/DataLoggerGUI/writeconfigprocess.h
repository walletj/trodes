#ifndef WRITECONFIGPROCESS_H
#define WRITECONFIGPROCESS_H

#include "abstractprocess.h"

class WriteConfigProcess : public AbstractProcess
{
    Q_OBJECT
public:
    WriteConfigProcess(QString device, QString cfg, QWidget *parent=nullptr);
    void start();

private:
    QString device;
    QString cfgfile;
    void win_start_writeconfig();
    void lin_start_writeconfig();
    void mac_start_writeconfig();
    void docking_writeconfig();
protected:
    void customReadOutput(const QString &line);
};

#endif // WRITECONFIGPROCESS_H
