#include "trodesDataInterface.h"
#include <algorithm>
#include "time.h"
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

TrodesMessageInterface::TrodesMessageInterface(QString cfgFileName, QString srvAddr, QString srvPort, QObject *parent) :
    QObject(parent)
    , configFileName(cfgFileName)
    , serverAddress(srvAddr)
    , serverPort(srvPort)
{
      if (!configFileName.isEmpty())
          nsParseTrodesConfig(configFileName); // this creates a bunch of global structs
}

TrodesMessageInterface::~TrodesMessageInterface()
{

}

void TrodesMessageInterface::setup()
{
    //------------------------------------------
    /*
    Module connection protocol:
    1) Trodes (or other master GUI) launches module X, or module X is started directly by the user
    2) Module X defines the provided and needed datatypes
    3) Module X creates a client and connects to Trodes (or the master GUI)
    4) When trodes responds with a module ID, a module server is automatically started if the module has any data available
    5) Module X gives Trodes it’s DataTypesAvailable structure, and it’s data server address
    6) Trodes fills its DataAvailable table entries with the given data, and checks that there are no repeats with other modules
    7) Trodes sends the current DataAvailable list to all currently connected modules.
    ...............
    8) Each module starts one client per needed data type and connects to the proper server
    */

    // Create the  moduleNet structure. This is required for all modules
    trodesModuleInterface = new TrodesModuleNetwork();
    // Define the data type that this module provides
    trodesModuleInterface->dataNeeded = /* TRODESDATATYPE_CONTINUOUS | TRODESDATATYPE_DIGITALIO  | */ TRODESDATATYPE_POSITION | TRODESDATATYPE_SPIKES;
    trodesModuleInterface->useQTSocketsForData = false;

    //Find server address of Trodes (or the master GUI)
    bool connectionSuccess;
    if ((!serverAddress.isEmpty()) &&  (!serverPort.isEmpty())) {
        //The address of the server was specified in the command line, so we use that
        connectionSuccess = trodesModuleInterface->trodesClientConnect(serverAddress,serverPort.toUInt(), true);
    } else {
        //Try to find an existing Trodes server or look in config file.  This will eventually be done with Zeroconf.
        connectionSuccess = trodesModuleInterface->trodesClientConnect();
    }

    if (connectionSuccess) {
        connect(trodesModuleInterface->trodesClient,SIGNAL(quitCommandReceived()),this,SLOT(quitNow()));
        //connect(trodesModuleInterface->trodesClient, &TrodesClient::quitCommandReceived, this, &TrodesMessageInterface::quitNow);
        //connect(trodesModuleInterface, SIGNAL(updatedDataAvaialble()), this, SLOT(dataAvailableUpdated()));
        TrodesDataInterface *dataInterface = new TrodesDataInterface();
        QThread *dataThread = new QThread();
        dataInterface->moveToThread(dataThread);

        connect(dataInterface, &TrodesDataInterface::statusMessage, this, &TrodesMessageInterface::statusMessage);
        connect(dataInterface, &TrodesDataInterface::newTimestamp, this, &TrodesMessageInterface::newTimestamp);
        connect(dataInterface, &TrodesDataInterface::newContinuousMessage, this, &TrodesMessageInterface::newContinuousMessage);
        connect(dataInterface, SIGNAL(newPosition(qint16,qint16,qint16,qreal)), this, SIGNAL(newPosition(qint16,qint16,qint16,qreal)));
        connect(dataInterface, SIGNAL(newVelocity(qreal)),this,SIGNAL(newVelocity(qreal)));

        //MARK: EVENT
        connect(trodesModuleInterface->trodesClient, SIGNAL(eventListReceived(QVector<TrodesEvent>)),this, SIGNAL(eventListReceived(QVector<TrodesEvent>)));

        if (SHOW_LATENCY_TRACKING) {
            connect(trodesModuleInterface->trodesClient, SIGNAL(currentTimeReceived(quint32)), this,SIGNAL(timeRecieved(quint32)));
            connect(dataInterface, SIGNAL(packetsSent(int)), this, SIGNAL(numPacketsSent(int)));
            connect(dataInterface, SIGNAL(newTimestamp(quint8,quint32)), trodesModuleInterface->trodesClient, SLOT(sendTimeRequest()));
        }
        connect(dataThread, &QThread::started, dataInterface, &TrodesDataInterface::Run);
        dataThread->start();
        connect(trodesModuleInterface, &TrodesModuleNetwork::startDataClient, dataInterface, &TrodesDataInterface::connectDataClient);

    }

    if (!connectionSuccess) {
        qDebug() << "Unable to connect with Trodes";
        emit quitNow();
    } else {
        qDebug() << "Starting up";
    }

    trodesModuleInterface->sendModuleName("Simple Communicator");
    //trodesModuleInterface->sendTimeRateRequest();
    //trodesModuleInterface->send
    emit statusMessage("Messaging connection started");

    Run();

}

void TrodesMessageInterface::quitNow()
{
//    QMetaObject::invokeMethod(trodesModuleInterface->trodesClient, "sendQuit", Qt::BlockingQueuedConnection);
    trodesModuleInterface->disconnectClient();
    QThread::msleep(250);
    emit quitReceived();
}

void TrodesMessageInterface::Run()
{
    while (1) {
        this->thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        QThread::usleep(100);
    }
}


TrodesClientSocket::TrodesClientSocket(quint8 dType, int uData, QTcpSocket *&sock, QObject *parent) :
    QObject(parent),
    tcpSocket(sock),
    udpSocket(NULL),
    dataType(dType),
    userData(uData),
    nativeSock(-1)
{
    connect(tcpSocket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(changeSockState(QAbstractSocket::SocketState)));
    socketType = TRODESSOCKETTYPE_TCPIP;
    sockState = sock->state();
}

TrodesClientSocket::TrodesClientSocket(quint8 dType, int uData, QUdpSocket *&sock, QObject *parent) :
    QObject(parent),
    tcpSocket(NULL),
    udpSocket(sock),
    dataType(dType),
    userData(uData),
    nativeSock(-1)
{
    connect(udpSocket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(changeSockState(QAbstractSocket::SocketState)));
    socketType = TRODESSOCKETTYPE_UDP;
    sockState = sock->state();
    //sockState = QAbstractSocket::UnconnectedState;
}

TrodesClientSocket::TrodesClientSocket(quint8 dType, int uData, qint16 sockType, int sock, QObject *parent) :
    QObject(parent),
    socketType(sockType),
    tcpSocket(NULL),
    udpSocket(NULL),
    dataType(dType),
    userData(uData),
    nativeSock(sock)
{
    //connect(tcpSocket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(changeSockState(QAbstractSocket::SocketState)));
    sockState = QAbstractSocket::UnconnectedState;
}

TrodesClientSocket::~TrodesClientSocket()
{

}

void TrodesClientSocket::changeSockState(QAbstractSocket::SocketState state) {
    sockState = state;
}

bool TrodesClientSocket::waitForReadyRead(int msecs)
{
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        return tcpSocket->waitForReadyRead(msecs);
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        return udpSocket->waitForReadyRead(msecs);
    }
    else {
        return false;
    }
}

TrodesDataInterface::TrodesDataInterface(QObject *parent) : QObject(parent)
{
    posStreamLatencyToken = 0;
    sLatencyToken = 5;
    continuousDataBlockSize = spikeConf->ntrodes.length() * sizeof(int16_t);

    headerSize = sizeof(quint8) + sizeof(quint32); // Message type, message size
    // for CONTINUOUS messages
    continuousBufferSize = sizeof(quint32) + sizeof(qint16) + sizeof(struct timespec); // timestamp, data, send time
    continuousData.resize(spikeConf->ntrodes.length());
    continuousTimestamps.resize(spikeConf->ntrodes.length());
    // for BLOCK_CONTINUOUS messages
    blockContBufferSize = sizeof(quint32) + spikeConf->ntrodes.length()*sizeof(int16_t); // timestamp, data, send time
    blockData.resize(spikeConf->ntrodes.length());
    // for POSITION messages
    positionDataBufferSize = 9/*data*/ + 1+4/*extra stuff in sendMessage added to data*/;

    // for POSITION messages
    dioDataBufferSize = 4/*timestamp*/ + 4/*port*/ + 1 /*input*/ + 1 /*value*/;

    // for SPIKE messages
    spikeDataBufferSize = MAX_SPIKE_POINTS * sizeof(qint16)/*data*/ + sizeof(quint32) /*timestamp*/;
    minDataBufferSize = qMin(spikeDataBufferSize, qMin(dioDataBufferSize, qMin(blockContBufferSize, qMin(continuousBufferSize, positionDataBufferSize))));
    maxDataBufferSize = qMax(spikeDataBufferSize, qMax(dioDataBufferSize, qMax(blockContBufferSize, qMax(continuousBufferSize, positionDataBufferSize))));

    dataBuffer.resize(maxDataBufferSize + headerSize); // Maximum size of a block continuous data buffer
    t1 = 0;
    t2 = 0;
    packetsRecieved = 0;

#ifdef NATIVE_SOCKETS
    maxfd = 0;
#endif
}

TrodesDataInterface::~TrodesDataInterface()
{
#ifdef NATIVE_SOCKETS
    /* Close sockets */
    for (int i = 0; i < socketList.length(); i++)
        close(socketList.at(i)->nativeSock);
#endif
}

TrodesClient* TrodesDataInterface::createClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort,
                  int dataType, int userData)
{
    TrodesClient *tc;
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        tc = new TrodesClient(socketType, hostname, tcpPort, false, this);
        tc->connectToHost();
        if (tc->isConnected()) {
            qDebug() << "Tcp data connected succesfully on port" << tc->tcpSocket->localPort();
            tc->setDataType(dataType, userData); // Triggers server to set up a new data handler
            socketList.append(new TrodesClientSocket(dataType, userData, tc->tcpSocket));
            //connect readyRead() signal with function that read's the socket's incomming data
            connect(socketList.back()->tcpSocket, SIGNAL(readyRead()), socketList.back(), SLOT(readSocket()));
            connect(socketList.back(),SIGNAL(sig_readSocket(TrodesClientSocket*)),this,SLOT(readFromSocket(TrodesClientSocket*)));

        }
        else
            qDebug() << "Error connecting module with TCP";
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        tc = new TrodesClient(socketType, hostname, udpPort, false, this);
        tc->udpSocket->connectToHost(QHostAddress(hostname), udpPort);
        qDebug() << "UDP data putatively connected succesfully";
        socketList.append(new TrodesClientSocket(dataType, userData, tc->udpSocket));
        //connect readyRead() signal with function that read's the socket's incomming data
        connect(socketList.back()->udpSocket, SIGNAL(readyRead()), socketList.back(), SLOT(readSocket()));
        connect(socketList.back(),SIGNAL(sig_readSocket(TrodesClientSocket*)),this,SLOT(readFromSocket(TrodesClientSocket*)));
    }
    else
        tc = NULL;

    return tc;
}

#ifdef NATIVE_SOCKETS

TrodesNativeClient *TrodesDataInterface::createNativeClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort, int dataType, int userData)
{
    TrodesNativeClient *tc;
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        tc = new TrodesNativeClient(socketType, hostname, tcpPort, this);
        tc->connectToHost();
        if (tc->isConnected()) {
            tc->setDataType(dataType, userData); // Triggers server to set up a new data handler
            socketList.append(new TrodesClientSocket(dataType, userData, TRODESSOCKETTYPE_TCPIP, tc->getDescriptor()));


            maxfd = qMax(maxfd, socketList.last()->nativeSock);
            qDebug() << "Tcp data connected succesfully on port" << tc->getPort() << " MaxFD now" << maxfd;
        }
        else
            qDebug() << "Error connecting module with TCP";
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        tc = new TrodesNativeClient(socketType, hostname, udpPort, this);
        tc->connectToHost();
        socketList.append(new TrodesClientSocket(dataType, userData, TRODESSOCKETTYPE_UDP, tc->getDescriptor()));
        maxfd = qMax(maxfd, socketList.last()->nativeSock);
        qDebug() << "UDP data putatively connected succesfully to " << tc->getHostname()
                 << " port " << tc->getPort() << " MaxFD now" << maxfd;
    }
    else
        tc = NULL;

    return tc;

}

#else

#define createNativeClient createClient

#endif


void TrodesDataInterface::connectDataClient(DataTypeSpec *da, int currentDataType)
{
    int cIdx;
/** #ifdef NATIVE_SOCKETS
    TrodesNativeClient *tc;
#else
    TrodesClient *tc;
#endif **/
    TrodesClient *tc;

    socketType = da->socketType;

    if (currentDataType == TRODESDATATYPE_CONTINUOUS) {
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            if ( (cIdx = da->contNTrodeIndexList.indexOf(n)) != -1 ) {// INDEXLIST is a list of indices into the spikeConf->ntrodes structure
                int udpPort = 0;
                if (socketType == TRODESSOCKETTYPE_UDP)
                    udpPort = da->contNTrodeUDPPortList.at(cIdx);
                if( (tc = createClient(socketType, da->hostName, da->hostPort, udpPort,
                               currentDataType, n)) != NULL) {
                    tc->setNTrodeId(spikeConf->ntrodes[n]->nTrodeId);
                    tc->setNTrodeIndex(n);
                    tc->sendDecimationValue(10);
                    emit statusMessage(QString("Added client for ntrode at index %1").arg(n));
                    tc->turnOnDataStreaming();
                }
            }
        }
    }
    else if (currentDataType == TRODESDATATYPE_SPIKES) {
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            if ( (cIdx = da->spikeNTrodeIndexList.indexOf(n)) != -1 ) {// INDEXLIST is a list of indices into the spikeConf->ntrodes structure
                int udpPort = 0;
                if (socketType == TRODESSOCKETTYPE_UDP)
                    udpPort = da->spikeNTrodeUDPPortList.at(cIdx);
                if( (tc = createClient(socketType, da->hostName, da->hostPort, udpPort,
                               currentDataType, n)) != NULL) {
                    tc->setNTrodeId(spikeConf->ntrodes[n]->nTrodeId);
                    tc->setNTrodeIndex(n);
                    tc->turnOnDataStreaming();
                }
            }
        }
    }
    else if (currentDataType == TRODESDATATYPE_DIGITALIO) {
        qDebug() << "Digital IO dataAvailable host" << da->hostName << " " << da->hostPort;
        if ( (tc = createClient(socketType, da->hostName, da->hostPort, da->digitalIOUDPPort,
                          currentDataType, 0)) != NULL) {
            tc->turnOnDataStreaming();
        }
    }
    else if (currentDataType == TRODESDATATYPE_POSITION) {
        if ((tc = createClient(socketType,da->hostName,da->hostPort,da->positionUDPPort,currentDataType,0))!= NULL) {
            tc->turnOnDataStreaming();
        }
    }
    else if (currentDataType == TRODESDATATYPE_BLOCK_CONTINUOUS) {
        qDebug() << "Block continuous dataAvailable host" << da->hostName << " " << da->hostPort;
        if ( (tc = createClient(socketType, da->hostName, da->hostPort, da->blockContinuousUDPPort,
                          currentDataType, 0)) != NULL) {
            tc->sendDecimationValue(10);
            emit statusMessage(QString("Set decimation to 10."));
            tc->turnOnDataStreaming();
        }
    }
}

void TrodesDataInterface::Run()
{
    //This is where the old run loop was, it is no longer necessary.
}

void TrodesDataInterface::readFromSocket(TrodesClientSocket *sock) {
    if (sock->socketType == TRODESSOCKETTYPE_TCPIP) {
        //qDebug() << "TCP Message Received";
        tcpDataMessageReceived(sock);
    }
    else if (sock->socketType == TRODESSOCKETTYPE_UDP) {
        //qDebug() << "UDP Message Received";
        udpDataMessageReceived(sock);
    }
}

void TrodesDataInterface::udpDataMessageReceived(TrodesClientSocket *client)
{
    quint8 messageType;
    quint32 messageSize;

    TrodesDataStream msgStream(&dataBuffer, QIODevice::ReadWrite);
    msgStream.setVersion(TrodesDataStream::Qt_4_0);

    QByteArray errorBuffer;
    static int counter = 0;
    static double avgBytesAvail = 0.0;

    quint64 bytesAvail = 0;
    qint64 datagramSize;

    bytesAvail = client->udpSocket->bytesAvailable();

    while (bytesAvail) {
        // we need to read in the whole datagram at once to avoid losing data
        datagramSize = client->udpSocket->pendingDatagramSize();
        if (datagramSize >= (qint64) minDataBufferSize + headerSize) {
//            client->udpSocket->readDatagram(dataBuffer.data(), datagramSize);
            client->udpSocket->read(dataBuffer.data(), datagramSize);
            msgStream.device()->reset();
            msgStream >> messageType;
            msgStream >> messageSize;

            avgBytesAvail = 0.9999*avgBytesAvail + 0.0001*bytesAvail;
            if (++counter >= 300000) {
                qDebug() << "Average samples (bytes/6) available " << avgBytesAvail/datagramSize;
                counter = 0;
            }
        }
        else {
            errorBuffer.resize(datagramSize);
            client->udpSocket->read(errorBuffer.data(), datagramSize);
//            client->udpSocket->readDatagram(errorBuffer.data(), datagramSize);
            qDebug() << "Recevied unexpected datagram (too small " <<
                        datagramSize << "). Message: " << errorBuffer;
            return;
        }
        processMessage(messageType, messageSize, msgStream, client->userData);
        bytesAvail = client->udpSocket->bytesAvailable();
    }
}

void TrodesDataInterface::tcpDataMessageReceived(TrodesClientSocket *client)
{
    quint8 messageType;
    quint32 messageSize;
    TrodesDataStream msgStream(&dataBuffer, QIODevice::ReadWrite);
    msgStream.setVersion(TrodesDataStream::Qt_4_0);

    QByteArray errorBuffer;

    quint64 bytesAvail = 0;

    bytesAvail = client->tcpSocket->bytesAvailable();

    while (bytesAvail) {
        if (bytesAvail < headerSize + minDataBufferSize) {// a buffer's worth of data isn't available
            return;
        }
        else {
            client->tcpSocket->read(dataBuffer.data(), headerSize);
            msgStream.device()->reset();
            msgStream >> messageType;
            msgStream >> messageSize;

            if (messageSize > maxDataBufferSize) {
                qDebug() << "Input size too big " << messageSize;
                errorBuffer.resize(messageSize);
                qint64 bytesRead = client->tcpSocket->read(errorBuffer.data(), messageSize);
                if (bytesRead < messageSize)
                    qDebug() << "Error reading error buffer";
                return;
            }

            client->tcpSocket->read(dataBuffer.data(), messageSize);
            msgStream.device()->reset(); // inStream will now be pointing to the message
        }

        processMessage(messageType, messageSize, msgStream, client->userData);
        bytesAvail = client->tcpSocket->bytesAvailable();

    }
}

#ifdef NATIVE_SOCKETS
void TrodesDataInterface::nativeUdpDataMessageReceived(TrodesClientSocket *client)
{
    int readLen;
    quint8 messageType;
    quint32 messageSize;
    TrodesDataStream msgStream(&dataBuffer, QIODevice::ReadWrite);
    msgStream.setVersion(TrodesDataStream::Qt_4_0);

    if ((readLen = recvfrom(client->nativeSock, dataBuffer.data(), dataBuffer.length(), 0, NULL, 0)) > dataBuffer.length()) {
        qDebug() << "[TrodesDataInterface] Dropped data in native UDP read";
    }
    else {
        msgStream.device()->reset();
        msgStream >> messageType;
        msgStream >> messageSize;
        processMessage(messageType, messageSize, msgStream, client->userData);
    }

}

void TrodesDataInterface::nativeTcpDataMessageReceived(TrodesClientSocket *client)
{
    int readLen;
    static quint8 messageType = 0;
    static quint32 messageSize = 0;
    static int sizeRemaining = headerSize;
    TrodesDataStream msgStream(&dataBuffer, QIODevice::ReadWrite);
    msgStream.setVersion(TrodesDataStream::Qt_4_0);

    enum MessageState {partial_header, partial_message};
    static MessageState state = partial_header;

    static char* buf = dataBuffer.data();

    if (state == partial_header) {
        if ((readLen = recv(client->nativeSock, buf, sizeRemaining, 0)) <= 0) {
            qDebug() << "[TrodesDataInterface] Error in native TCP read read 1";
            return;
        }
        if (readLen == sizeRemaining) {
            state = partial_message;
            msgStream.device()->reset();
            msgStream >> messageType;
            msgStream >> messageSize;
            sizeRemaining = messageSize;
            buf = dataBuffer.data();
        }
        else {
            sizeRemaining = sizeRemaining - readLen;
            buf = buf + readLen;
        }
    }
    if (state == partial_message) {
        if ((readLen = recv(client->nativeSock, buf, sizeRemaining, 0)) <= 0) {
            qDebug() << "[TrodesDataInterface] Error in native TCP read error 2";
            return;
        }
        if (readLen == sizeRemaining) {
            state = partial_header;
            msgStream.device()->reset();
            processMessage(messageType, messageSize, msgStream, client->userData);
            buf = dataBuffer.data();
            sizeRemaining = headerSize;
        }
        else {
            sizeRemaining = sizeRemaining - readLen;
            buf = buf + readLen;
        }
    }
}

#endif

// https://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

void TrodesDataInterface::processMessage(quint8 messageType, quint32 messageSize, TrodesDataStream &msgStream, int userData)
{
    //packetsRecieved++;
    //qDebug() << "@@Packet Recieved #[" << packetsRecieved << "]";

    quint32 timestamp;
    qint16 dataValue; // For CONTINUOUS
    struct timespec send_time, recv_time;
    static int counter = 3000;
    static double min_nsec = 0;
    static double max_nsec = 0;
    static double mean_nsec = 0;
    double diff_nsec;

    if (messageType == TRODESDATATYPE_CONTINUOUS) {
        if(messageSize != continuousBufferSize)
            qDebug() << "Wierd message size for continuous data " << messageSize << continuousBufferSize;
        msgStream >> timestamp;
        msgStream >> dataValue;
        msgStream.readRawData((char *) &send_time, sizeof(send_time));
        //msgStream >> send_time;
        continuousData[userData] = dataValue;
#ifdef __linux__
        clock_gettime(CLOCK_MONOTONIC, &recv_time);
#elif __MACH__  // OS X does not have clock_gettime, use clock_get_time
        // see https://gist.github.com/jbenet/1087739
        // and http://stackoverflow.com/questions/11680461/monotonic-clock-on-osx
        clock_serv_t cclock;
        mach_timespec_t mts;
        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
        clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);
        recv_time.tv_sec = mts.tv_sec;
        recv_time.tv_nsec = mts.tv_nsec;
#endif
//        continuousTimestamps[userData] = diff(send_time, recv_time).tv_nsec;

        diff_nsec = diff(send_time, recv_time).tv_nsec + diff(send_time, recv_time).tv_sec * 1.0e9;
        min_nsec = qMin(min_nsec, diff_nsec);
        max_nsec = qMax(max_nsec, diff_nsec);
        mean_nsec = (1- 0.00001)*mean_nsec + 0.00001 * diff_nsec;

        emit newTimestamp(TRODESDATATYPE_CONTINUOUS, timestamp);

        if (userData == 0 ) {
            if (counter-- < 0) {
                counter = 3000;
                qDebug() << "Mean, Min, Max nsecs" << mean_nsec << min_nsec << max_nsec;
            }
            //            uint32_t minval = *std::min_element(continuousTimestamps.constBegin(), continuousTimestamps.constEnd());
            //            uint32_t maxval = *std::max_element(continuousTimestamps.constBegin(), continuousTimestamps.constEnd());
            //            emit newContinuousMessage(QString("Timestamp max difference: %1 [%2, %3]")
            //                                      .arg(maxval-minval).arg(minval).arg(maxval));
        }
    }
    else if (messageType == TRODESDATATYPE_BLOCK_CONTINUOUS) {
        if(messageSize != blockContBufferSize)
            qDebug() << "Wierd message size for block continuous data " << messageSize << blockContBufferSize;
        msgStream >> timestamp;
        //inStream >> blockData;
        emit newTimestamp(TRODESDATATYPE_BLOCK_CONTINUOUS, timestamp);

    }
    else if (messageType == TRODESDATATYPE_DIGITALIO) {
        msgStream >> timestamp;
        emit newTimestamp(TRODESDATATYPE_DIGITALIO, timestamp);

    }
    else if (messageType == TRODESDATATYPE_POSITION) {

        if (TRODES_VERSION > 142) {
            int flg, numOfPacketsSent;
            uint8_t camNum, zoneID;
            qint16 x,y,linSeg, numLins, segNum, numNodes;
            qint32 diff;
            qreal linPos, px1, px2, py1, py2, velocity, zx, zy;
            QPointF p1, p2;

            int packetTime;
            //qreal packetTime2;
            //initialize default values for all variables
            flg = PPT_NULL;
            packetTime = -1;
            //packetTime2 = -1.0;
            camNum = -1;
            x = 0;
            y = 0;
            linSeg = -1;
            linPos = -1;

            //extract first flg, position data will always start with a flag describing the incomming data
            msgStream >> flg;
            //qDebug() << " ";
            //qDebug() << " flg read: " << flg;
            //run through data until end of stream flag (PPT_NULL) is found;
            while (flg != PPT_NULL) {
                switch (flg) {
                case PPT_Header: {
                    msgStream >> timestamp;
                    msgStream >> camNum;
                    msgStream >> numOfPacketsSent;
                    t2 = timestamp;
                    diff = ((t2 - t1)/30);
                    t1 = t2;
                    if ((timestamp > 0 || diff > 0) && SHOW_LATENCY_TRACKING) {
                        latencyAvg.insert(diff);
                        qDebug() << "Timestamp (" << timestamp << "), Latency [" << diff << "] - avg [" << latencyAvg.average() << "]";
                    }


                    break;
                }
                case PPT_2DPos: {
                    //t2 = (std::chrono::system_clock::now().time_since_epoch().count() / 1000);
                    msgStream >> packetTime;
                    //msgStream >> packetTime2;
                    msgStream >> x;
                    msgStream >> y;

                    if (packetTime > -1 && benchConfig->printPositionStreaming()) {
                        int timeDiff = QTime::currentTime().msecsSinceStartOfDay() - packetTime;
                        timeLatencyAvg.insert(timeDiff);

                        if (posStreamLatencyToken == 0)
                            qDebug() << "Time recieved (" << packetTime << "), Latency [" << timeDiff << "] - avg [" << timeLatencyAvg.average() << "] - movAvg [" << timeLatencyAvg.movingAverage() << "] -- Max/min[" << timeLatencyAvg.getMax() << "/" << timeLatencyAvg.getMin() << "]";
                        posStreamLatencyToken = (posStreamLatencyToken+1)%benchConfig->getFreqPositionStream();

                    }
                    //qDebug() << "-recieved 2D positioning data (" << x << "," << y << ") [latency: " << (t2-t1) << "]";
                    //t1 = t2;
                    break;
                }
                case PPT_Lin: {
                    msgStream >> linSeg;
                    msgStream >> linPos;
                    //qDebug() << "-recieved linear data [" << linSeg << ":" << linPos << "]";
                    break;
                }
                case PPT_LinTrack: {
                    msgStream >> numLins;
                    qDebug() << "Linear Track Data received: ";
                    //for all lines attached to the data packet
                    for (int i = 0; i < numLins; i++) {
                        msgStream >> segNum;
                        msgStream >> px1;
                        msgStream >> py1;
                        msgStream >> px2;
                        msgStream >> py2;
                        qDebug() << "-line: " << segNum;
                        qDebug() << "   -p1(" << px1 << "," << py1 << ") || p2(" << px2 << "," << py2 << ")";
                    }
                    break;
                }
                case PPT_Zone: {
                    msgStream >> zoneID;
                    msgStream >> numNodes;
                    qDebug() << "Zone Data Received:";
                    for (int i = 0; i < numNodes; i++) {
                        msgStream >> zx;
                        msgStream >> zy;
                        qDebug() << "   Line " << i << " (" << zx << "," << zy << ")";
                    }
                    break;
                }
                case PPT_Velocity: {
                    msgStream >> velocity;
                    qDebug() << "velocity recieved -" << velocity;
                    emit newVelocity(velocity);
                    break;
                }
                default: {
                    qDebug() << "Error: Invalid flag read (processMessage()) flg: " << flg;
                    break;
                }
                }
                msgStream >> flg;
                //qDebug() << " flg read: " << flg;
            } //end while()

            emit newPosition(x,y,linSeg,linPos);
            emit newTimestamp(TRODESDATATYPE_POSITION, timestamp);
            emit packetsSent(numOfPacketsSent);
            }

        else {
            qint16 x,y;
            uint8_t camNum;
            msgStream >> timestamp;
            msgStream >> x;
            msgStream >> y;
            msgStream >> camNum;

            emit newPosition(x,y, 0, 0);
            emit newTimestamp(TRODESDATATYPE_POSITION, timestamp);

        }

    }
    else if (messageType == TRODESDATATYPE_SPIKES) {
        msgStream >> timestamp ;
        int numNTrodes = spikeConf->ntrodes[userData]->hw_chan.length(); //retreive number of channels of sending NTrode
        int numDataPoints = POINTSINWAVEFORM * numNTrodes;
        int16_t dPoint;
        //extract waveform, datapoint by data point
        for (int i = 0; i < numDataPoints; i++) {
            msgStream >> dPoint; //store this in an array if you want the waveform
        }
        //benchmarking to determine latency of spike data
        if (benchConfig->isRecordingSysTime() && benchConfig->printSpikeReceived()) {
            int sysTime;
            msgStream >> sysTime;
            int diff = QTime::currentTime().msecsSinceStartOfDay() - sysTime;
            spikeLatencyAvg.insert(diff);
            if (sLatencyToken == 0 && benchConfig->printSpikeReceived())
                qDebug() << "Spike data received, Latency[" << diff << "] -avg[" << spikeLatencyAvg.average() << "] -movAvg[" << spikeLatencyAvg.movingAverage() << "] --Max/Min[" << spikeLatencyAvg.getMax() << "/" << spikeLatencyAvg.getMin() << "]";
            sLatencyToken = (sLatencyToken+1)%benchConfig->getFreqSpikeReceived();
        }
        emit newTimestamp(TRODESDATATYPE_SPIKES, timestamp);
    }
    else {
        qDebug() << "Input Type:" << messageType << ". Input Size:" << messageSize;
    }
}

#ifdef NATIVE_SOCKETS
TrodesNativeClient::TrodesNativeClient(int sockType, QString hName, quint16 p, QObject *parent) :
    QObject(parent),
    socketType(sockType),
    connected(false),
    hostname(hName), port(p),
    nTrodeId(-1), nTrodeIndex(-1)
{
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        qSock = new QTcpSocket(this);
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        qSock = new QUdpSocket(this);
    }


//    /* Construct the server address structure */
//    memset(&servAddr, 0, sizeof(servAddr));    /* Zero out structure */
//    servAddr.sin_family = AF_INET;                 /* Internet addr family */
//    inet_pton(AF_INET, (const char*) hostname.toLatin1().data(), &servAddr.sin_addr);
//    servAddr.sin_port   = htons(port);     /* Server port */
}

void TrodesNativeClient::connectToHost()
{
    int ntries = 0;
    // Connect for both TCP and UDP. For UDP this doesn't actually do anything but store the addressing info
    while (ntries < 10) {
        qSock->connectToHost(QHostAddress(hostname),port);
        if (!(qSock->waitForConnected(10))) {
            qDebug() << "Unable to connect trodes client socket" <<
                        getHostname() << ":" << getPort() << "; retrying";
            QObject().thread()->usleep(1000000);
            ntries++;
        }
        else {
            connected = true;
            sock = qSock->socketDescriptor(); // This won't work until we connect
            break;
        }
    }

}

bool TrodesNativeClient::isConnected()
{
    return connected;
}

void TrodesNativeClient::setDataType(quint8 dataType, qint16 userData)
{
    //This is used to set the dataType for a messageHandler that connects to the main trodes server.
    //We set the local datatype and send that information back to trodes.
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite); //Each message has a header
    //msg.setVersion(TrodesDataStream::Qt_4_0); // WHY DON'T WE NEED THIS HERE?
    msg << dataType; //The first part of the header is the data type
    msg << userData;
    sendMessage(TRODESMESSAGE_SETDATATYPE, temp);
}

void TrodesNativeClient::setNTrodeId(int trodeId)
{
    nTrodeId = trodeId;
}

void TrodesNativeClient::setNTrodeIndex(int index)
{
    nTrodeIndex = index;
}

void TrodesNativeClient::sendDecimationValue(quint16 dec)
{
    sendMessage(TRODESMESSAGE_SETDECIMATION, dec);
}

void TrodesNativeClient::turnOnDataStreaming()
{
    QByteArray temp(0);
    sendMessage((quint8)TRODESMESSAGE_TURNONDATASTREAM, temp);
}

void TrodesNativeClient::turnOffDataStreaming()
{
    QByteArray temp(0);
    sendMessage((quint8)TRODESMESSAGE_TURNOFFDATASTREAM, temp);
}

int TrodesNativeClient::getPort()
{
    return port;
}

QString TrodesNativeClient::getHostname()
{
    return hostname;
}

int TrodesNativeClient::getDescriptor()
{
    return sock;
}

void TrodesNativeClient::sendMessage(quint8 dataType, QByteArray message)
{
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite); //Each message has a header
    //msg.setVersion(TrodesDataStream::Qt_4_0); // WHY DON'T WE NEED THIS HERE?
    msg << dataType; //The first part of the header is the data type

//    if (message.length() > 0) {
        //The next part is a 32-bit value indicating how large the rest of the message is in bytes
        msg << (quint32)message.size();
        msg.writeRawData(message.data(), message.length());
//    }

    int bytesSent;

    if ((bytesSent = send(sock, temp.constData(), temp.length(), 0)) != temp.length())
        qDebug() << "[TrodesNativeClient] sendto() sent a different number of bytes than expected." <<
                    sock << bytesSent << message.length() << std::strerror(errno);
}



template<class T>
void TrodesNativeClient::sendMessage(quint8 messageType, T message)
{
    // put the message variable in a ByteArray
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite);
    msg << message;
    sendMessage(messageType, temp);
}

#endif

void TrodesClientSocket::readSocket(void) {
    emit sig_readSocket(this);
}
