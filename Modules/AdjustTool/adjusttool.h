#ifndef ADJUSTTOOL_H
#define ADJUSTTOOL_H

#include "trodesSocket.h"


class AdjustTool : public QObject {
    Q_OBJECT
public:
    AdjustTool(QStringList arguments, QWidget* parent);

    //int loadFromXML(QDomNode &adjustConfNode);
    //void saveToXML(QDomDocument &doc, QDomElement &rootNode);

    TrodesModuleNetwork *moduleNet;
    TrodesServer *fsDataServer;

private:

    TrodesModuleNetwork *moduleNet;
    TrodesServer *fsDataServer;

    bool trodes;
    QString configFileName;
    QString backendPath;

};

#endif // ADJUSTTOOL_H
