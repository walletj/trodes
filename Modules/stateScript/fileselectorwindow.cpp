/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "fileselectorwindow.h"
#include "QGridLayout"
#include "QMessageBox"

FileSelectorWindow::FileSelectorWindow(QWidget *parent)
{
    isEditorOpen = false;

    QGridLayout *mainLayout = new QGridLayout;
    QLabel *protocolLabel = new QLabel("<b>Protocols</b>");
    protocolLabel->setToolTip("StateScript protocol sent to hardware");
    protocolDirLabel = new QLabel();
    protocolDirLabel->setToolTip("StateScript protocol sent to hardware");
    stateScriptSelector = new QListView();
    connect(stateScriptSelector,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(stateScriptDoubleClick(QModelIndex)));
    connect(stateScriptSelector,SIGNAL(clicked(QModelIndex)),SIGNAL(stateScriptSelectorClicked()));

    /*
    QFileSystemModel *scfilemodel = new QFileSystemModel;
    scFilePath.setCurrent("/Users/karlssonm/Src/stateScript/scripts");
    scfilemodel->setRootPath(scFilePath.path());
    stateScriptSelector->setModel(scfilemodel);
    stateScriptSelector->setRootIndex(scfilemodel->index(QDir::currentPath()));
    */
    QLabel *observerLabel = new QLabel("<b>Callbacks</b>");
    observerLabel->setToolTip("Callback function used by observer scripting language");
    observerDirLabel = new QLabel();
    observerDirLabel->setToolTip("Callback function used by observer scripting language");
    localScriptSelector = new QListView();
    connect(localScriptSelector,SIGNAL(clicked(QModelIndex)),SIGNAL(localScriptSelectorClicked()));

    QFont font;
    font.setPointSize(10);
    stateScriptSelector->setFont(font);
    localScriptSelector->setFont(font);
    protocolLabel->setFont(font);
    observerLabel->setFont(font);

    mainLayout->addWidget(protocolLabel,0,0,Qt::AlignCenter);
    mainLayout->addWidget(observerLabel,0,1,Qt::AlignCenter);
    mainLayout->addWidget(protocolDirLabel,1,0,Qt::AlignCenter);
    mainLayout->addWidget(observerDirLabel,1,1,Qt::AlignCenter);
    mainLayout->addWidget(stateScriptSelector,2,0);
    mainLayout->addWidget(localScriptSelector,2,1);

#ifdef __APPLE__
    editorPath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/../../../stateScriptEditor.app/Contents/MacOS/stateScriptEditor");
#else
    editorPath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/stateScriptEditor");
#endif

    setLayout(mainLayout);
}

void FileSelectorWindow::resizeEvent(QResizeEvent *event) {
//    QRect currentGeometry = this->geometry();
//    QFont font;

    /*if (currentGeometry.width() < 500) {
        font.setPointSize(8);
        stateScriptSelector->setFont(font);
        localScriptSelector->setFont(font);
    } else {
        font.setPointSize(12);
        stateScriptSelector->setFont(font);
        localScriptSelector->setFont(font);
    }*/
}

QString FileSelectorWindow::getCurrentStateScriptFolder() {
    return scFilePath;
}

QString FileSelectorWindow::getCurrentStateScriptSelection() {
    return stateScriptSelector->currentIndex().data().toString();
}

QString FileSelectorWindow::getCurrentLocalScriptFolder() {
    return localFilePath;
}

QString FileSelectorWindow::getCurrentLocalScriptSelection() {
    return localScriptSelector->currentIndex().data().toString();
}

void FileSelectorWindow::enableLocalSelector() {
    localScriptSelector->setEnabled(true);
}

void FileSelectorWindow::disableLocalSelector() {
    localScriptSelector->setEnabled(false);
}

void FileSelectorWindow::stateScriptDoubleClick(QModelIndex index) {
    //if the user double-clicks on a script, the editor program starts and opens the script

    QStringList arglist;
    QString tmpPath = QDir::toNativeSeparators(scFilePath+"/"+index.data().toString());


    arglist << tmpPath;



    QProcess* editor = new QProcess(this);
    editor->setProcessChannelMode(QProcess::ForwardedChannels);
    connect(editor,SIGNAL(started()),this,SLOT(setEditorOpen()));
    //connect(editor,SIGNAL(finished(int)),this,SLOT(setEditorClosed(int)));
    connect(editor,SIGNAL(finished(int)),editor,SLOT(deleteLater()));

    /*QMessageBox::information(
        this,
        tr("Opening editor"),
        editorPath);*/
    editor->start(editorPath, arglist);


    //This version sends the open command to the existing editor if it is open (opens a new tab).  Doesn't work well on linux for some reason.
    /*
    if (!isEditorOpen) {
        qDebug() << editorPath;

        editorProgram = new QProcess(this);
        editorProgram->setProcessChannelMode(QProcess::ForwardedChannels);
        connect(editorProgram,SIGNAL(started()),this,SLOT(setEditorOpen()));
        connect(editorProgram,SIGNAL(finished(int)),this,SLOT(setEditorClosed(int)));
        editorProgram->start(editorPath, arglist);
    } else {
        editorProgram->write(QString(tmpPath+"\n").toLocal8Bit());
    }*/

}

void FileSelectorWindow::setEditorOpen() {
    qDebug() << "Editor open";
    isEditorOpen = true;
}

void FileSelectorWindow::setEditorClosed(int code) {
    qDebug() << "Editor closed with exit code" << code;
    isEditorOpen = false;
    editorProgram->deleteLater();
}

void FileSelectorWindow::changeStateScriptFolder(QString folderName) {
    QFileSystemModel *filemodel = new QFileSystemModel;
    scFilePath = folderName;
    QDir tempdir;
    tempdir.setCurrent(folderName);
    filemodel->setRootPath(tempdir.path());
    filemodel->setFilter(QDir::Files);

    stateScriptSelector->setModel(filemodel);
    stateScriptSelector->setRootIndex(filemodel->index(QDir::currentPath()));
    protocolDirLabel->setText("<i>"+folderName+"</i>");
}

void FileSelectorWindow::changeLocalScriptFolder(QString folderName) {
    QFileSystemModel *filemodel = new QFileSystemModel;
    localFilePath = folderName;
    QDir tempdir;
    tempdir.setCurrent(folderName);
    filemodel->setRootPath(tempdir.path());
    filemodel->setFilter(QDir::Files);

    localScriptSelector->setModel(filemodel);
    localScriptSelector->setRootIndex(filemodel->index(QDir::currentPath()));
    observerDirLabel->setText("<i>"+folderName+ "</i>");
}
