
#include <string.h>
#include <stdio.h>
#include <windows.h>
#include "diskio.h"




/*-----------------------------------------------------------------------*/
/* Main                                                                  */


int main (int argc, char *argv[])
{
	unsigned char res;
  int pd, i, j, count;
  FILE *fp;
  unsigned char Buff[32768];			/* Working buffer */
  char fname[100];

  
  // turn off output buffering
  setvbuf(stdout, 0, _IONBF, 0);
	printf("\n*** readConfig 1.0 ***\n");

  pd = 1;
  if (argc > 1)
    pd = atoi(argv[1]);
  
	if (!assign_drives(pd)) {
		printf("\nUsage: pcheck <phy drv#>\n");
		return 2;
	}

  
  // read the configuration sector
  res = disk_read(0, Buff, 0, 1);
  if (res) {
    printf("rc=%d\n", (WORD)res);
    return 2;
  }

  count = 0;
  for (i = 0; i < 32; i++) {
    printf("Group %02d: ", i);
    if (Buff[i]) {
      printf("Card(s) ");
      for (j = 0; j < 8; j++) {
        if ((Buff[i] >> j) & 0x01) {
          count++;
          if (j == 0)
            printf("%d",j);
          else
            printf(",%d",j);
        }
      }
      printf(" enabled\n");
    }
  }
  printf("\n%d channels enabled, packet size = %d\n", count, 2*count+6);

  if (argc > 2) {
    sprintf(fname,"%s",argv[2]);
    fp = fopen(fname, "w");
    if (fp != NULL) {
      for (i = 0; i < 32; i++) {
        for (j = 7; j >=0; j--) {
          if ((Buff[i] >> j) & 0x01)
            fprintf(fp, "1");
          else
            fprintf(fp,"0");
        }
        fprintf(fp,"\n");
      }
      fclose(fp);
    }
  }
}
  


