function [threshtmp, v, m, s, lockout] = newRealtimeRippleAlgorithm(yt, rippleData, thresh, nostim_mask)

y = rippleData;

lastval = zeros(20,size(y,2));

tmpy = abs(y);
v = zeros(size(y));
newV = zeros(size(y,1),1);

m = zeros(size(y,1),1);
s = zeros(size(y,1),1);

lockout = zeros(size(y,1),1);
%m = zeros(1,size(y,2));
%s = zeros(1,size(y,2));

rt = [];
i = 2;
while (i <= size(y,1))

    posgain = mean(lastval);
    lastval(1:19,:) = lastval(2:20,:);
    df = tmpy(i,:) - v(i-1,:);
    %	threshtmp2 = min(m{t}(i) + 2 * s{t}(i), 45);
    %	if (tmpy(i) > threshtmp)
    if (df > 0)
        gain = 1.2;
        v(i,:) = v(i-1,:) + df .* posgain;
    else
        gain = 0.2;
        v(i,:) = v(i-1,:) + df .* gain;
    end
    lastval(20,:) = gain;
    newV(i) = sqrt(sum(v(i,:).^2));

    dm = newV(i) - m(i-1);
    m(i) = m(i-1) + dm * 0.0001;
    ds = abs(newV(i) - m(i)) - s(i-1);
    s(i) = s(i-1) + ds * 0.0001;
    
    %threshtmp = m(i,:) + thresh .* s(i,:);
    threshtmp = m(20000,:) + thresh .* s(20000,:);

        % lock in mean and std in 10000 samples
%     if(( i < 10000))
%         dm = tmpy(i,:) - m;
%         m = m + dm * 0.001;
%         ds = abs(tmpy(i,:) - m) - s;
%         s = s + ds * 0.001;
%         threshtmp = m + thresh .* s;
%         
    if ((i > 20000) && (newV(i) > threshtmp) && ~nostim_mask(i))
        rt = [rt yt(i,:)];
        tmpm = m(i);
        tmps = s(i);
        tmpy(i:i+374,:) = 0;
        lockout(i:i+374,:) = 1;
        i = i + 374;
        m(i) = tmpm;
        s(i) = tmps;
    end
    i = i + 1;
end
threshtmp = m(end) + thresh .* s(end);
sprintf('thresh = %d\n', threshtmp)
lockout = lockout(1:size(y,1));
m = m(1:size(y,1));
s = s(1:size(y,1));

end

