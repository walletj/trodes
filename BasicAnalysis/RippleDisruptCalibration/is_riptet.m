function y=is_riptet(x)
if isfield(x, 'descrip')
    y = strcmp(x.descrip,'riptet');
else
    y = logical(0);
end