
set(0, 'DefaulttextInterpreter', 'none')

figid_tet_indiv = 4
subplot_ii = 5;

directory = '/opt/data36/daliu/DL20/stimcalib';
rec_filename = '/20160705_20_stimcal_BD_1000ua_4sd_174';
channel = [1,1;2,1;3,3;4,2;5,3;7,3;11,4;14,3;16,2;13,4];

%channel = [5,1;8,4;13,1;16,3;12,1];
%channel = [17,1;17,2;17,3;17,14;18,5;18,7;19,4;19,8;19,9;20,7;20,16];

dio_stim_channel = 21 + 21;

% directory = '/opt/data36/daliu/DL15/stimcalib/';
% rec_filename = '/20160519_Kanye_stimcal_BD_150';
% channel = [5,1;8,4;13,1;16,3;12,1];
% dio_stim_channel = 21 + 21;

% 
% directory = '/opt/data36/daliu/DL15/tree/';
% rec_filename = '20160502_lemonade_stimcalib_2b3b_600ua_in_Cell_Layer.rec';
% channel = [1,1;2,1;3,1;4,1];

%channel = [1,1;2,1;3,1;5,1;7,1;8,1;9,1;11,1;13,1;14,1;16,1;12,1];
%channel = [17,1;17,2;17,3;17,14;18,5;18,7;19,4;19,8;19,9;20,7;20,16];
%dio_stim_channel = 20 + 5;
%channel = [1,1;2,1;7,1];
%channel = [17,1;18,5;19,8;;20,16];
% dio_stim_channel = 21+21;

% directory = '/opt/data36/daliu/DL15/wtrack/';
% rec_filename = '20160427_DL15_r1_stim';
% channel = [1,1;2,1;3,1;5,1;7,1;8,1;9,1;11,1;13,1;14,1;16,1;12,1];
% dio_stim_channel = 20 + 5;

% directory = '/opt/data36/daliu/DL15/stimcalib/';
% rec_filename = '20160426_stimcalib1_150ua.rec';
% channel = [1,1;2,1;3,1;5,1;7,1;8,1;9,1;11,1;13,1;14,1;16,1;12,1];
% dio_stim_channel = 20 + 5;


% directory = '/opt/data36/daliu/DL11/wtrack/';
% rec_filename = '20160315_3_wtrack';
% channel = [1,1;3,1;4,1;6,1;8,2;12,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/opt/data36/daliu/DL11/stim_calib/';
% rec_filename = '20160311_stimcalib1_800uA_23b.rec';
% channel = [1,1;3,1;4,1;6,1;8,2;12,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/DL11/wtrack/';
% rec_filename = '20160321_5_wtrack.rec';
% channel = [1,1;3,1;4,1;6,1;8,2;12,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/DL11/debugging/';
% rec_filename = '20160314_lintrack1_debug_stim.rec';
% channel = [1,1;4,1;6,1;8,2;12,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/DL11/stim_calib/';
% rec_filename = '20160321_stimcalib1_700uA_23b.rec';
% channel = [1,1;3,1;4,1;6,1;8,2;12,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/DL11/stim_calib/';
% rec_filename = '20160310_stimcalib4_200uA_23b.rec';
% channel = [1,1;4,1;6,1;8,1;13,1;14,1;15,1;16,1;11,3];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/templeton/at_haight/stimcalibration/';
% rec_filename = '20160216_stimcalib2_stimce_500ua_4.5std.rec';
% channel = [2,2;10,3;11,1;13,1;14,3;15,3;16,1;17,3;12,4];
% dio_stim_channel = 21 + 21;

% directory = '/mnt/hotswap/templeton/at_haight/';
% rec_filename = 'templeton_02-11-2016(Testrun3_400ua_CE_boxCalibr_RipFiltOnly-Fixed).rec';
% channel = [2,2;11,1;13,1;14,1;15,1;16,1;17,1;12,4];
% dio_stim_channel = 21 + 21;
% 
% directory = '/opt/data36/daliu/templeton/homeswitch/';
% rec_filename = '20151213_stim2m_175ua_r1';
%  channel = [2,2;11,1;13,1;14,1;15,1;16,1;17,1;12,4];
% %channel = [18,2;18,10;18,11;18,12;19,6;20,8;20,10;20,13;21,2;21,5;21,13;12,4];
% dio_stim_channel = 16 + 8;

% directory = '/opt/data36/daliu/DL10/wtrack/';
% rec_filename = '20160326_4_wtrack';
% channel = [4,1;7,1;12,1;15,1;17,1; 28,1; 29,1 ;2,4];
% dio_stim_channel = 16 + 5;

% directory = '/opt/data36/daliu/DL10/stim_calib/';
% rec_filename = '20160326_stimcalib1_50uA_ce';
% channel = [4,1;7,1;12,1;15,1;17,1; 28,1; 29,1 ;2,4];
% dio_stim_channel = 16 + 5;

% directory = '/mnt/hotswap/HPC_4prb/';
% rec_filename = '20160216_stimac_200ua_newcable.rec';
% channel = [13,1;29,1;2,1];
% dio_stim_channel = 21 + 16;


%directory = '/opt/data36/daliu/trodes/templeton/';
%dio_stim_channel = 8 + 16;

%rec_filename = '20151205_stimcalibration_stim2m_175uA.rec';
%channel = [1,1;2,1;3,1;6,1;10,1;16,1;12,4];

%rec_filename = '20151206_stim2m_175uA_r2.rec'
%channel = [1,1;3,1;8,1;9,1;10,1;16,1;12,4];

%rec_filename = '20151209_dummy_stimtest_sleep.rec'
%channel = [1,1;2,1;3,1;9,1;15,1;16,1;12,4];

%rec_filename = '20151211_homeswitch1_stimcalib_175uAref6_real.rec';
%channel = [2,1;10,1;11,1;15,1;16,1;17,1;6,2];

%rec_filename = '20151213_stim2m_175ua_r1.rec';
%channel = [2,1;10,1;11,1;15,1;16,1;17,1;6,2];

fsdata_thresh_raw =    [76, 48;
                    63, 39;
                    78, 49;
                    75, 47;
                    79, 50;
                    72.8, 45.1];
fsdata_thresh = (fsdata_thresh_raw(:,1) + 4.5 * fsdata_thresh_raw(:,2)) * 0.195;

disp('loading tetrodes');
tic;
% last channel in list is to dereference
import_trodes = readTrodesFileContinuous(strcat(directory, rec_filename, '.rec'),channel,0);

toc;

%%
% import_pos = readTrodesExtractedDataFile(strcat(directory, rec_filename, '.1.videoPositionTracking'));
% pos_time = import_pos.fields(1).data;
% pos_x = double(import_pos.fields(2).data + import_pos.fields(4).data)/2;
% pos_y = double(import_pos.fields(3).data + import_pos.fields(5).data)/2;
% pos_timestep = diff(double(pos_time));
% inst_vel = sqrt(double(((diff(pos_x)* 0.194) .^2) + ((diff(pos_y) * 0.194) .^ 2)))./30;

%%

cutoff_low = 600;
cutoff_high = 6000;
filt_order = 8;

fs = import_trodes.samplingRate;
% design filter
[b_an,a_an] = besself(filt_order,[2*pi*cutoff_low,2*pi*cutoff_high]);
[b,a] = bilinear(b_an,a_an,fs);

%% Calculate stim times
% assuming clean stim DIO with no bounce
disp('extracting stim times');
tic;
dio = readTrodesFileDigitalChannels(strcat(directory, rec_filename, '.rec'));
toc;

%%
dio_time = dio.timestamps;
stim_dio = dio.channelData(dio_stim_channel).data;

stim_time_ind = find(diff(stim_dio)>0);
stim_time = dio_time(stim_time_ind);

%%
% disp('reading csv');
% tic;
% %fsdata_filename = '20151205_stimcalibration_stim2m_175uA_c1s1_FSDATASAVE.csv';
% %fsdata_filename = '20151206_stim2m_175uA_r2_FSDATASTATE.csv';
% %fsdata_filename = '20151209_dummy_stimtest_sleep_FSDATASTATE.csv';
% %fsdata_filename = '20151211_homeswitch1_stimcalib_175uAref6_real.csv';
% fsdata_filename = '20151213_rippleFilterState_r1.csv';
% for
% fsdata = csvread(strcat(directory,fsdata_filename));
% toc;


%%
% Last channel used to dereference
cont_data_deref = (import_trodes.channelData(:,1:end-1) - ...
    repmat(import_trodes.channelData(:,end),[1,size(import_trodes.channelData,2)-1]));
%cont_data_deref = import_trodes.channelData(:,1:end-1);
timestamps = import_trodes.timestamps;
spike_filter_data_all = filter(b,a,cont_data_deref);


samples_per_ms = 30;

win_size = 4;           %ms
win_center = 1;         %ms
threshold = 50;         %uV
refractory = 1*samples_per_ms;
samples_behind = win_center*samples_per_ms;
samples_ahead = (win_size-win_center)*samples_per_ms;

all_waves = {};
all_spktime_ind = {};
disp('starting spike filter and thresholding');
tic;
for ii = 1:size(spike_filter_data_all,2)
    spike_filter_data = spike_filter_data_all(:,ii);
    spk_thresh = find(spike_filter_data > threshold);
    spk_thresh_refractory_ind = [1 (find(diff(spk_thresh) > refractory) + 1)'];

    spktime_ind = spk_thresh(spk_thresh_refractory_ind);

    spk_ind = cell2mat(arrayfun(@(x)(x-samples_behind:x+samples_ahead-1),spktime_ind,'UniformOutput',false));

    spk_ind = spk_ind(find(~max(spk_ind <= 0 | spk_ind > size(spike_filter_data,1),[],2)),:);

    spk_trans = spike_filter_data';
    waves = spk_trans(spk_ind);
    
    all_spktime_ind{ii} = spktime_ind;
    all_waves{ii} = waves;
end
toc;

%%
disp('ripple filtering');
tic;
% ripple_cutoff_low = 150;
% ripple_cutoff_high = 250;
% ripple_filt_order = 8;
% %[rip_b_an, rip_a_an] = besself(ripple_filt_order,[2*pi*ripple_cutoff_low,2*pi*ripple_cutoff_high]);
% %[rip_b,rip_a] = bilinear(rip_b_an,rip_a_an,fs);
% 
% [rip_b, rip_a] = butter(ripple_filt_order,[ripple_cutoff_low, ripple_cutoff_high]/(fs/2));

ripplefiltstruct = load('ripplefilter.mat');
rip_kernel = ripplefiltstruct.ripplefilter.kernel;

ripple_filter_data_all = filter(rip_kernel, 1, cont_data_deref);
ripple_hilbert_data_all = hilbert(ripple_filter_data_all);
ripple_env_data_all = abs(ripple_hilbert_data_all);

% smoothing_width = 0.004;
% 
% kernel = gaussian(smoothing_width*fs, ceil(8*smoothing_width*fs));
% % change to half gaussian to smooth forward in time
% kernel(1:length(kernel)/2) = zeros(length(kernel)/2,1);
% ripple_env_smooth_data_all = zeros(size(ripple_env_data_all));
% for ii = 1:6
%     ripple_env_smooth_data_all(:,ii) = smoothvect(ripple_env_data_all(:,ii), kernel);
% end

ripple_env_std_all = std(ripple_env_data_all);
ripple_env_base_all = mean(ripple_env_data_all);
ripple_env_thresh_all = ripple_env_base_all + 2 * ripple_env_std_all;
toc;


%%
figure(1);
clf;
ha = tight_subplot(size(channel,1)-1,1,[.005 .03],[.03 .01],[.03 .01]);

%x_start = 264;
stim_ii = 10;
x_start = stim_time(stim_ii)-0.3;
%x_start = 2175
x_jump = 2;
xrange = [x_start,x_start+x_jump];

for ii=1:size(channel,1)-1
    axes(ha(ii));
    plot_ii = ii;
    plot_time_range = [x_start,x_start+20];
    %fsdata_tmp = fsdata(fsdata(:,2)==channel(ii,1)-1,:);
    %fsdata_tmp(:,1) = fsdata_tmp(:,1)/30000;
    %fsdata_tmp = fsdata_tmp(fsdata_tmp(:,1) > plot_time_range(1) & fsdata_tmp(:,1) < plot_time_range(2),:);
    
    tmp_timestamps = timestamps(timestamps > plot_time_range(1) & timestamps < plot_time_range(2));
    tmp_cont_data = cont_data_deref(timestamps > plot_time_range(1) & timestamps < plot_time_range(2),plot_ii);
    tmp_spike_filter_data = spike_filter_data_all(timestamps > plot_time_range(1) & timestamps < plot_time_range(2),plot_ii);
    tmp_ripple_filt_data = ripple_filter_data_all(timestamps > plot_time_range(1) & timestamps < plot_time_range(2),plot_ii);
    tmp_ripple_env_data = ripple_env_data_all(timestamps > plot_time_range(1) & timestamps < plot_time_range(2),plot_ii);
    %tmp_ripple_env_smooth_data = ripple_env_smooth_data_all(timestamps > plot_time_range(1) & timestamps < plot_time_range(2),plot_ii);

    tmp_rippletime_over_thresh = tmp_timestamps(tmp_ripple_env_data > ripple_env_thresh_all(plot_ii));
    
    %fsdata_stimtime = fsdata_tmp(find(diff(fsdata_tmp(:,1))>0.2),1);
    %fsdata_stimtime = fsdata_tmp(find(diff(fsdata_tmp(:,4))>0),1);
    %plot(tmp_timestamps, tmp_ripple_filt_data);
    plot(tmp_timestamps, tmp_cont_data, 'Color',[0.8,0.8,0.8]);
    hold on;
    plot(tmp_timestamps, tmp_spike_filter_data, 'Color',[0.4,0.4,0.4]);
    %plot(tmp_timestamps, tmp_ripple_env_data, 'r', 'LineWidth', 2);
    %plot(fsdata_tmp(:,1),fsdata_tmp(:,6)*0.195, 'g', 'LineWidth', 2);
    plot(timestamps(all_spktime_ind{plot_ii}),repmat([80],[length(all_spktime_ind{plot_ii}),1]),'r*');
    plot(stim_time, repmat([100],[length(stim_time),1]),'c^', 'MarkerSize', 8);
    %plot(fsdata_stimtime, repmat([90],[length(fsdata_stimtime),1]),'mo', 'MarkerSize', 8);
    
    plot(tmp_rippletime_over_thresh, repmat([110],[length(tmp_rippletime_over_thresh),1]),'b.') 
    
    %plot(plot_time_range, [fsdata_thresh(plot_ii), fsdata_thresh(plot_ii)], '--');
    
    xlim(xrange)
    ylim([-50,200])
    grid on;

%     if ii < size(channel,1)-1
%         set(gca,'XTick',[]);
%     end
    hold off;
end
linkaxes(ha);
%%
xrange = xrange + x_jump-0.5;
% stim_ii = stim_ii + 1;
% x_start = stim_time(stim_ii)-0.5;
% x_jump = 2;
% xrange = [x_start,x_start+x_jump];
for ii=1:size(channel,1)-1
    axes(ha(ii));
    xlim(xrange);
end
%%
win_min = -250*30;
win_max = 250*30;
artifact_min = -2*30;
artifact_max = 6*30;
hist_binsize = 2;
hist_range = [-250, 250];
all_stim_spktime_ind = [];
figure(figid_tet_indiv);
for ii=1:size(all_spktime_ind,2)
    tet_stim_spktime_ind = [];
    disp(['starting stim window multiunit calculation for ', ii]);
    tic;
    %spktime_ind = all_spktime_ind{ii}(max(all_waves{ii},[],2) < 600);
    spktime_ind = all_spktime_ind{ii};
    spk_near_stim = lookup(spktime_ind,stim_time_ind);
    for stim_jj = 1:length(stim_time)
        
        tet_stim_spktime_ind = [tet_stim_spktime_ind; ...
            (spktime_ind((spktime_ind > stim_time_ind(stim_jj) + win_min & ...
            spktime_ind < stim_time_ind(stim_jj) + artifact_min) | ...
            (spktime_ind > stim_time_ind(stim_jj) + artifact_max & ...
            spktime_ind < stim_time_ind(stim_jj) + win_max)) - stim_time_ind(stim_jj))];
    end
    all_stim_spktime_ind = [all_stim_spktime_ind; tet_stim_spktime_ind];

    subplot(ceil(size(all_spktime_ind,2)/2), 2, ii);
    [tet_stim_mult_hist,tet_stim_mult_hist_edges] = histcounts(tet_stim_spktime_ind/30, [hist_range(1):hist_binsize:hist_range(2)]);
    bar(tet_stim_mult_hist_edges(1:end-1),tet_stim_mult_hist./length(stim_time)/(hist_binsize/1000),0.5,'FaceColor',[0.5,0.5,1],'EdgeColor',[0,0,0],'LineWidth',1);
    xlim(hist_range);
    
    toc;
end
figure(2)
subplot(5,1,subplot_ii)

%histogram(all_stim_spktime_ind/30, [-500:2:500]);
[stim_mult_hist,stim_mult_hist_edges] = histcounts(all_stim_spktime_ind/30, [hist_range(1):hist_binsize:hist_range(2)]);
bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(stim_time)/(hist_binsize/1000),0.5,'FaceColor',[0.5,0.5,1],'EdgeColor',[0,0,0],'LineWidth',1);
title(sprintf('%s (%d)',rec_filename, length(stim_time)));
xlim(hist_range);
%% Plotting ripple envelope
figure(3);
env_samples_before = 0.15 * fs;
env_samples_after = 0.1 * fs;

subplot(2,1,1)
rip_env_set_ind = cell2mat(arrayfun(@(x)(x-env_samples_before:x+env_samples_after-1),stim_time_ind,'UniformOutput',false));
rip_env_set_timestamps = (-env_samples_before:env_samples_after-1)/fs;
for tet_ii = 1:6
    tmp_ripple_env_data = ripple_env_data_all(:,tet_ii);
    rip_env_set = tmp_ripple_env_data(int64(rip_env_set_ind));
    mean_rip_env_set = mean(rip_env_set,1);
    plot(rip_env_set_timestamps,mean_rip_env_set);
    hold on;
end
plot([0,0],[0,350],'g--');
ylim([0,50]);
hold off;
title('150-250Hz Ripple Envelop Unsmoothed');   

subplot(2,1,2)

fsdata_tmp = fsdata(fsdata(:,2)==channel(1,1)-1,:);
fsdata_tmp(:,1) = fsdata_tmp(:,1)/30000;
fsdata_stimtime_ind = find(diff(fsdata_tmp(:,4))>0);
fsdata_stimtime = fsdata_tmp(fsdata_stimtime_ind,1);

fsdata_env_samples_before = 0.15 * 1500;
fsdata_env_samples_after = 0.1 * 1500;

fsdata_env_set_ind = cell2mat(arrayfun(@(x)(x-fsdata_env_samples_before:x+fsdata_env_samples_after-1),fsdata_stimtime_ind,'UniformOutput',false));
fsdata_env_set_timestamps = (-fsdata_env_samples_before:fsdata_env_samples_after-1)/1500;
for tet_ii = 1:6
    fsdata_tmp = fsdata(fsdata(:,2)==channel(tet_ii,1)-1,:);
    fsdata_tmp(:,1) = fsdata_tmp(:,1)/30000;
    fsdata_tmp = fsdata_tmp(:,6);
    fsdata_env_set = fsdata_tmp(int64(fsdata_env_set_ind));
    mean_fsdata_env_set = mean(fsdata_env_set,1);
    plot(fsdata_env_set_timestamps,mean_fsdata_env_set);
    hold on;
end
hold off;
title('100-400Hz FSData Envelop');


%%
figure(5)
%ax1 = subplot(2,1,1)
plot(timestamps, ripple_filter_data_all(:,1),'b');
hold on;
plot(stim_time, repmat([100],[length(stim_time),1]),'r*', 'MarkerSize', 8);
hold off;
%ax2 = subplot(2,1,2)
%pos_kernel = gaussian(1*30, ceil(4*1*30));
%plot(pos_time(1:end-1)/30000,smoothvect(inst_vel,pos_kernel));
%linkaxes([ax1,ax2],'x')
