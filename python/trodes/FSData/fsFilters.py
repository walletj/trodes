import trodes.FSData.fsDataUtil as fsUtils
import time
import struct
import math
import numpy as np
from collections import deque
import logging
import sys

from trodes.FSData.datatypes import LFPPoint, SpikePoint, PosPoint, DigIOStateChange, SystemTimePoint, RawPosPoint

sys.path.append('../Modules/FSGui')
import fsShared

# import trodes.FSData.fsShared as fsShared

from IPython.terminal.debugger import TerminalPdb
bp = TerminalPdb(color_scheme='linux').set_trace

NFILT = 19  # the number of points in the IIR ripple filter
NSPEED_FILT_POINTS = 15  # the number of points in the speed filter
NLAST_VALS = 20  # the number of values to average over for the gain of the ripple filter

fNumerator = [2.435723358568172431e-02,
              -1.229133831328424326e-01,
              2.832924715801946602e-01,
              -4.629092463232863941e-01,
              6.834398182647745124e-01,
              -8.526143367711925825e-01,
              8.137704425816699727e-01,
              -6.516133270563613245e-01,
              4.138371933419512372e-01,
              2.165520280363200556e-14,
              -4.138371933419890403e-01,
              6.516133270563868596e-01,
              -8.137704425816841836e-01,
              8.526143367711996879e-01,
              -6.834398182647782871e-01,
              4.629092463232882815e-01,
              -2.832924715801954929e-01,
              1.229133831328426407e-01,
              -2.435723358568174512e-02]

fDenominator = [1.000000000000000000e+00,
                -7.449887056735371438e+00,
                2.866742370538527496e+01,
                -7.644272470167831557e+01,
                1.585893197862293391e+02,
                -2.703338821178639932e+02,
                3.898186201116285474e+02,
                -4.840217978093359079e+02,
                5.230782138295531922e+02,
                -4.945387299274730140e+02,
                4.094389697124813665e+02,
                -2.960738943482194827e+02,
                1.857150345772943751e+02,
                -9.980204002570326338e+01,
                4.505294594295533273e+01,
                -1.655156422615593215e+01,
                4.683913633549676270e+00,
                -9.165841559639211766e-01,
                9.461443242601841330e-02]

speedFilterValues = np.array([0.0779,
                              0.0775,
                              0.0768,
                              0.0758,
                              0.0745,
                              0.0728,
                              0.0709,
                              0.0688,
                              0.0663,
                              0.0637,
                              0.0610,
                              0.0581,
                              0.0551,
                              0.0520,
                              0.0488])


class SingleChanRippleFilter:
    def __init__(self):
        self.pos_gain = 0.0
        self.ntrode_id = -1  # the user assigned number of this nTrode
        self.enabled = 0  # true if this Ntrode is enabled
        global NFILT
        global NLAST_VALS
        self.ripple_mean = 0.0
        self.ripple_std = 0.0
        self.f_x = [0.0] * NFILT
        self.f_y = [0.0] * NFILT
        self.filtind = 0
        self.last_val = deque([0.0] * NLAST_VALS)
        # self.lvind = 0
        self.current_val = 0.0
        self.current_thresh = 0.0

    def reinit(self):
        global NFILT
        global NLAST_VALS
        self.ripple_mean = 0.0
        self.ripple_std = 0.0
        self.f_x = [0.0] * NFILT
        self.f_y = [0.0] * NFILT
        self.filtind = 0
        self.last_val = deque([0.0] * NLAST_VALS)
        # self.lvind = 0
        self.current_val = 0.0
        self.current_thresh = 0.0


class RippleFilter(fsUtils.BinaryRecordBase):
    def __init__(self, num_ntrodes):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        # current time, ntrode index, trigger, in lockout, rd, currentval
        super().__init__(rec_id=1,
                         rec_labels=['current_time', 'ntrode_index', 'stim_triggered', 'lockout', 'rd', 'current_val'],
                         rec_format='Ii??dd')

        self.num_ntrodes = num_ntrodes
        self.filt_num = fNumerator
        self.filt_den = fDenominator
        self.chan_ripple_filter = [SingleChanRippleFilter() for i in range(num_ntrodes)]
        self.param = fsShared.RippleFilterParameters()
        self.custom_ripple_baseline_mean = [0 for i in range(num_ntrodes)]  # user set means for each nTrode if desired
        self.custom_ripple_baseline_std = [0 for i in range(num_ntrodes)]  # user set stdevs for each nTrode if desired
        self.param.enabled = False
        self.trigger_stim = False
        self.in_lockout = False
        self.last_rip_time = 0
        self.current_time = 0
        self.counter = 0
        self.reinit()

    def reinit(self):
        self.trigger_stim = False
        self.in_lockout = False
        self.last_rip_time = 0
        self.reset_ripple_data()
        self.reset_ripple_counters()

    def reset_ripple_data(self):
        for c in self.chan_ripple_filter:
            c.reinit()

    def reset_ripple_counters(self):
        self.counter = 0

    def update_ripple_params(self, data):
        # copy the parameters into the local parms structure. Is there a better way to do this?
        # double ripCoeff1
        # double ripCoeff2;
        # double ripple_threshold;
        # int sampDivisor
        # int n_above_thresh;
        # int lockoutTime;
        # int detectNoRippleTime;
        # int dioGatePort
        # bool detectNoRipples;
        # bool dioGate
        # bool enabled;
        # bool useCustomBaseline;     Use whatever custom baseline values are set in double* customRippleBaseline...
        # bool updateCustomBaseline;  Set custom baseline values from SingleChanRippleFilter estimates and send to FSGUI

        ptmp = struct.unpack('=dddiiiii?????xxxxxxx', data)  # NOTE: the pad bytes required trial and error
        self.class_log.info('ripple parm size {}'.format(len(ptmp)))
        self.param.ripCoeff1 = ptmp[0]
        self.param.ripCoeff2 = ptmp[1]
        self.param.ripple_threshold = ptmp[2]
        self.param.sampDivisor = ptmp[3]
        self.param.n_above_thresh = ptmp[4]
        self.param.lockoutTime = ptmp[5]
        self.param.detectNoRippleTime = ptmp[6]
        self.param.dioGatePort = ptmp[7]
        self.param.detectNoRipples = ptmp[8]
        self.param.dioGate = ptmp[9]
        self.param.enabled = ptmp[10]
        self.param.useCustomBaseline = ptmp[11]
        self.param.updateCustomBaseline = ptmp[12]
        self.class_log.info('ripple enabled {}'.format(self.param.enabled))

    def set_custom_ripple_mean(self, data):
        # the data are numNTrodes doubles, so we create a string the the right number of 'd's in a string
        parsestring = 'd' * self.num_ntrodes
        ptmp = struct.unpack(parsestring, data)
        self.custom_ripple_baseline_mean = list(ptmp)
        for i in range(self.num_ntrodes):
            crf = self.chan_ripple_filter[i]
            crf.ripple_mean = ptmp[i]
            # also update the threshold for this channel
            crf.current_thresh = crf.ripple_mean + crf.ripple_std * self.param.ripple_threshold

    def set_custom_ripple_std(self, data):
        # the data are numNTrodes doubles, so we create a string the the right number of 'd's in a string
        parsestring = 'd' * self.num_ntrodes
        ptmp = struct.unpack(parsestring, data)
        self.custom_ripple_baseline_std = list(ptmp)
        for i in range(self.num_ntrodes):
            crf = self.chan_ripple_filter[i]
            crf.ripple_std = ptmp[i]
            # also update the threshold for this channel
            crf.current_thresh = crf.ripple_mean + crf.ripple_std * self.param.ripple_threshold

    def update_custom_ripple_baseline(self):
        for i in range(self.num_ntrodes):
            self.custom_ripple_baseline_mean[i] = self.chan_ripple_filter[i].ripple_mean
            self.custom_ripple_baseline_std[i] = self.chan_ripple_filter[i].ripple_std

    def get_custom_ripple_mean(self):
        return self.custom_ripple_baseline_mean

    def get_custom_ripple_std(self):
        return self.custom_ripple_baseline_std

    def process_ripple_data(self, ntrode_index, d, stim_enabled, current_time, last_stim_time):

        self.current_time = current_time

        if self.current_time - last_stim_time < self.param.lockoutTime:
            self.in_lockout = True
        else:
            self.in_lockout = False

        crf = self.chan_ripple_filter[ntrode_index]
        # print('in ProcessRippleData, ntindex', nTrodeIndex, 'inLo', self.inLockout, 'counter', self.counter)
        if self.in_lockout:
            # filter a scaled version of the data to ensure continuity
            rd = self.filter_channel(crf, ((current_time - last_stim_time) / self.param.lockoutTime) * d)
            crf.current_val = crf.ripple_mean
            self.trigger_stim = False

            # current time, ntrode index, trigger, in lockout, rd, currentval
            self.write_record(self.rec_id, self.current_time, ntrode_index, self.trigger_stim,
                              self.in_lockout, rd, crf.current_thresh)

            return self.trigger_stim

        rd = self.filter_channel(crf, d)
        # we need time to build up the estimate of the ripple power, so we wait at 10000 samples
        # if (self.counter < 10000):
        #     self.counter += 1
        #     self.stimOn = False
        #     return self.stimOn
        # process the data if we get to this point
        y = abs(rd)

        # print('y =', y)
        # only update the values if we are not stimulating
        if not stim_enabled:
            crf.ripple_mean += (y - crf.ripple_mean) / self.param.sampDivisor
            crf.ripple_std += (abs(y - crf.ripple_mean) - crf.ripple_std) / self.param.sampDivisor
            if not self.param.useCustomBaseline:  # only update the threshold if we're not using a custom baseline
                crf.current_thresh = crf.ripple_mean + crf.ripple_std * self.param.ripple_threshold
            # print('ntrode', crf.nTrodeId, 'mean', crf.rippleMean)

        # track the rising and falling of the signal
        df = y - crf.current_val
        if df > 0:
            gain = self.param.ripCoeff1
            crf.pos_gain = self.update_last_val(crf, gain)
            crf.current_val += df * crf.pos_gain
        else:
            gain = self.param.ripCoeff2
            crf.pos_gain = self.update_last_val(crf, gain)
            crf.current_val += df * gain

        # set stimOn based on the number of channels above their thresholds
        self.trigger_stim = (self.n_above_ripple_thresh() >= self.param.n_above_thresh)
        # print('ProcessRipData, stimOn: ', self.stimOn)

        if self.rec_writer_enabled and not self.rec_writer.closed:
            # current time, ntrode index, trigger, in lockout, rd, currentval
            self.rec_writer.write_rec(self.rec_id, self.current_time, ntrode_index, self.trigger_stim,
                                      self.in_lockout, rd, crf.current_thresh)

        if self.trigger_stim:
            # record the time of the last ripple
            self.last_rip_time = current_time
        if not self.param.detectNoRipples:
            return self.trigger_stim
        else:
            return (current_time - self.last_rip_time) > self.param.detectNoRippleTime

    def filter_channel(self, crf, d):
        # return the results of filtering the current value and update the filter values
        val = 0.0
        crf.f_x.pop()
        crf.f_x.insert(0, d)
        crf.f_y.pop()
        crf.f_y.insert(0, 0.0)
        # crf.fX[crf.filtind] = d
        # crf.fY[crf.filtind] = 0
        # apply the IIR filter this should be done with a dot product eventually
        for i in range(NFILT):
            # jind = (crf.filtind + i) % NFILT
            val = val + crf.f_x[i] * self.filt_num[i] - crf.f_y[i] * self.filt_den[i]
        crf.f_y[0] = val
        return val

    def update_last_val(self, crf, d):
        # return the new gain for positive increments based on the gains from the last 20 points
        # mn = np.mean(crf.lastVal)
        mn = sum(crf.last_val) / NLAST_VALS
        # print('process ', data)
        crf.last_val.popleft()
        crf.last_val.append(d)
        return mn

    def n_above_ripple_thresh(self):
        n_above = 0
        for i in range(self.num_ntrodes):
            if self.chan_ripple_filter[i].enabled:
                if self.param.useCustomBaseline and \
                        (self.chan_ripple_filter[i].current_val > self.chan_ripple_filter[i].current_thresh):
                    n_above += 1
                elif self.chan_ripple_filter[i].current_val > self.chan_ripple_filter[i].current_thresh:
                    n_above += 1
        return n_above

    def get_status_text_bytes(self):
        s = bytearray()
        # the rest of the array is the status string
        if self.param.enabled:
            s.extend('Ripple Filter Enabled\n\n'.encode('utf-8'))
            for i in range(self.num_ntrodes):
                crf = self.chan_ripple_filter[i]
                if crf.enabled:
                    if self.param.useCustomBaseline:
                        s.extend('{}: *FIXED* mean (std): {:.2f} ({:.2f})\n'.
                                 format(crf.ntrode_id,
                                        self.custom_ripple_baseline_mean[i],
                                        self.custom_ripple_baseline_std[i]).encode('utf-8'))
                    else:
                        s.extend('{}: Ripple mean (std): {:.2f} ({:.2f})\n'.
                                 format(crf.ntrode_id,
                                        crf.ripple_mean,
                                        crf.ripple_std).encode('utf-8'))

            s.extend('\nTimestamps since last {}\n'.format(self.current_time - self.last_rip_time).encode('utf-8'))
        else:
            s.extend('Ripple Filter Disabled\n'.encode('utf-8'))

        return s


class SpatialFilter(fsUtils.BinaryRecordBase):
    def __init__(self):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        super().__init__(rec_id=2,
                         rec_labels=['current_time', 'xpos', 'ypos', 'speed'],
                         rec_format='IHHd')

        self.param = fsShared.SpatialFilterParameters()
        self.speed_filt = speedFilterValues
        self.param.enabled = False
        self.current_time = 0

        self.in_lockout = False
        self.stim_on = False            # True when pos, vel, and state change criteron met
        self.xpos = 0
        self.ypos = 0
        # reset the speed related variables
        self.speed = [0.0] * NSPEED_FILT_POINTS
        self.ind = NSPEED_FILT_POINTS - 1
        self.lastx = 0
        self.lasty = 0
        self.last_state_change = 0
        self.reinit()

    def reinit(self):
        self.in_lockout = False
        self.stim_on = False
        self.xpos = 0
        self.ypos = 0
        # reset the speed related variables
        self.speed = [0.0] * NSPEED_FILT_POINTS
        self.ind = NSPEED_FILT_POINTS - 1
        self.lastx = 0
        self.lasty = 0
        self.last_state_change = 0

    def update_spatial_params(self, data):
        # copy the parameters into the local parms structure. Is there a better way to do this?
        #    int lowerLeftX;
        #    int lowerLeftY;
        #    int upperRightX;
        #    int upperRightY;
        #    double minSpeed;
        #    double maxSpeed;
        #   double cmPerPix;
        #    int   lockoutTime;
        #     bool enabled;

        self.class_log.info('updating spatial parms, len = {}'.format(len(data)))
        ptmp = struct.unpack('=iiiidddi?xxx',
                             data)  # note that the = prevents additional alignment bytes from being added
        self.param.lowerLeftX = ptmp[0]
        self.param.lowerLeftY = ptmp[1]
        self.param.upperRightX = ptmp[2]
        self.param.upperRightY = ptmp[3]
        self.param.minSpeed = ptmp[4]
        self.param.maxSpeed = ptmp[5]
        self.param.cmPerPix = ptmp[6]
        self.param.lockoutTime = ptmp[7]
        self.param.enabled = ptmp[8]
        self.class_log.info('spatial filter enabled {}'.format(self.param.enabled))

    def process_pos_data(self, xpos, ypos, timestamp):
        print(xpos, ypos)
        self.current_time = timestamp

        if self.current_time - self.last_state_change < self.param.lockoutTime:
            self.in_lockout = True
        else:
            self.in_lockout = False

        self.xpos = xpos
        self.ypos = ypos

        # calculate speed
        animal_speed = self.filter_pos_speed(xpos, ypos)

        self.write_record(self.rec_id, self.current_time, self.xpos, self.ypos, animal_speed)

        # check if speed is in window
        if (animal_speed < self.param.minSpeed) or (animal_speed > self.param.maxSpeed):
            tmp_stim = False
        else:
            # check to see if the location is in the specified box
            tmp_stim = ((self.xpos >= self.param.lowerLeftX) and
                        (self.xpos <= self.param.upperRightX) and
                        (self.ypos >= self.param.lowerLeftY) and
                        (self.ypos <= self.param.upperRightY))

        # stim state should not update if lockout is on
        if self.in_lockout:
            return self.stim_on

        # if stim state changes, then save the time.  Used to calculate lockout
        if tmp_stim != self.stim_on:
            self.last_state_change = self.current_time

        self.stim_on = tmp_stim

        return self.stim_on

    def get_status_text_bytes(self):
        s = bytearray()
        # the rest of the array is the status string
        if self.param.enabled:
            s.extend('Spatial Filter Enabled\n'.encode('utf-8'))
        else:
            s.extend('Spatial Filter Disabled\n'.encode('utf-8'))
        s.extend('Animal Location: {}, {}\n'.format(self.xpos, self.ypos).encode('utf-8'))
        if self.param.enabled:
            if self.stim_on:
                s.extend('Filter Triggered\n'.encode('utf-8'))
            else:
                s.extend('Filter not triggered\n'.encode('utf-8'))

        return s

    def filter_pos_speed(self, x_pos, y_pos):
        tmpind = 0
        smooth_spd = 0.0

        # Calculate instantaneous speed and adjust to cm/sec */

        self.speed[self.ind] = ((x_pos * self.param.cmPerPix - self.lastx) *
                                (x_pos * self.param.cmPerPix - self.lastx) +
                                (y_pos * self.param.cmPerPix - self.lasty) *
                                (y_pos * self.param.cmPerPix - self.lasty))

        # make sure this can't crash
        if self.speed[self.ind] != 0:
            self.speed[self.ind] = math.sqrt(self.speed[self.ind]) * 30.0

        self.lastx = x_pos * self.param.cmPerPix
        self.lasty = y_pos * self.param.cmPerPix

        # apply the filter to the speed points */
        for i in range(NSPEED_FILT_POINTS):
            tmpind = (self.ind + i) % NSPEED_FILT_POINTS
            smooth_spd += self.speed[tmpind] * self.speed_filt[i]
        self.ind -= 1
        if self.ind < 0:
            self.ind = NSPEED_FILT_POINTS - 1

        return smooth_spd


class ThetaFilter:
    def __init__(self):
        self.stimOn = False
        self.parm = fsShared.ThetaFilterParameters()
        self.inLockout = False
        self.reinit()

    def reinit(self):
        self.stimOn = False
        self.inLockout = False


class LatencyFilter(fsUtils.BinaryRecordBase):
    def __init__(self):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        # latency in samples
        super().__init__(rec_id=4,
                         rec_labels=['ntrode_triggered', 'latency_samples'],
                         rec_format='ii')

        self.lastDIOState = False
        self.param = fsShared.LatencyParameters()
        self.param.internal.enabled = False
        self.counter = 0
        self.triggered = False
        self.triggered_ntrode_id = -1
        self.lastTriggerTime = 0
        self.firstPulseDetected = False
        self.lastLatency = 0
        self.latencySum = 0
        self.nMeasurements = 0
        self.meanLatency = 0
        self.maxLatency = 0
        self.minLatency = 100000000

    def reinit(self):
        self.counter = 0
        self.triggered = False
        self.lastTriggerTime = 0
        self.firstPulseDetected = False
        self.lastLatency = 0
        self.latencySum = 0
        self.nMeasurements = 0
        self.maxLatency = 0
        self.minLatency = 100000000

    def update_latency_params(self, data):
        self.class_log.info('updating latency parms, len = {}'.format(len(data)))
        ptmp = struct.unpack('=?xHii?xxx', data)
        self.param.internal.enabled = ptmp[0]
        self.param.internal.stateScriptFnNum = ptmp[1]
        self.param.internal.outputDIOPort = ptmp[2]
        self.param.internal.testInterval = ptmp[3]
        self.param.hpc.enabled = ptmp[4]
        self.class_log.info('updating latency parms {}'.format(ptmp))

        if not self.param.internal.enabled:
            self.reinit()

    def process_cont_data(self, timestamp, ntrode_id):
        if self.param.internal.enabled:
            # print(timestamp, ' ', self.lastTriggerTime, ' ', self.parm.internal.testInterval, ' ', self.triggered)
            if (timestamp - self.lastTriggerTime) > self.param.internal.testInterval:
                if not self.triggered:
                    # print('fsProcessData: ProcessData() Trigger DIO, time', timestamp, '\n')
                    self.triggered = True
                    self.lastTriggerTime = timestamp
                    self.triggered_ntrode_id = ntrode_id
                    return True
        elif self.param.hpc.enabled:
            self.class_log.info('hpc enabled')
        # TODO: HPC Latency test
        return False

    def process_dio_data(self, digOutState, timestamp, port, io_dir, state):
        if port == self.param.internal.outputDIOPort:
            if self.triggered and state:
                self.lastLatency = timestamp - self.lastTriggerTime

                self.write_record(self.rec_id, self.triggered_ntrode_id, self.lastLatency)

                self.latencySum += self.lastLatency
                self.maxLatency = max((self.maxLatency, self.lastLatency))
                self.minLatency = min((self.minLatency, self.lastLatency))
                self.nMeasurements += 1
                self.class_log.info('Hit {} {}'.format(timestamp, self.lastLatency))
                self.triggered = False
                return True
        return False

    # def sendLatencyData(self, time):
    #    latencystr = '{}'.format(time)
    #    fsguisock.send_fsgui_message(FS_LATENCY_DATA, latencystr)

    def get_status_text_bytes(self):
        s = bytearray()
        if self.param.internal.enabled:
            if self.nMeasurements == 0:
                self.meanLatency = -1
            else:
                self.meanLatency = self.latencySum / self.nMeasurements
            s.extend('Latency Test Enabled'.encode('utf-8'))
            s.extend(
                'Last latency: {} samples = {} ms at 30 KHz\n'.format(self.lastLatency, self.lastLatency / 30.0).encode(
                    'utf-8'))
            s.extend(
                'Mean latency: {} samples = {} ms at 30 KHz\n'.format(self.meanLatency, self.meanLatency / 30.0).encode(
                    'utf-8'))
            s.extend(
                'Max latency: {} samples = {} ms at 30 KHz\n'.format(self.maxLatency, self.maxLatency / 30.0).encode(
                    'utf-8'))
            s.extend(
                'Min latency: {} samples = {} ms at 30 KHz\n'.format(self.minLatency, self.minLatency / 30.0).encode(
                    'utf-8'))
        else:
            s.extend('Latency Test Disabled\n'.encode('utf-8'))

        return s


class RTFilterManager(fsUtils.BinaryRecordBase):
    def __init__(self, supervisor, numntrodes):
        self.class_log = logging.getLogger(name='{}.{}'.format(self.__class__.__module__, self.__class__.__name__))

        self.record_manager = None

        # timestamp, tv_sec, tv_nsec, monotonic clock, ripple enabled, latency enabled
        super().__init__(rec_id=10,
                         rec_labels=['timestamp', 'tv_sec', 'tv_nsec', 'monotonic',
                                     'ripple_enabled', 'latency_enabled'],
                         rec_format='Iqqd??')

        self.supervisor = supervisor
        self.num_ntrodes = numntrodes
        self.n_din_ports = 0
        self.n_dout_ports = 0
        self.din_state = []
        self.dout_state = []
        self.stim_enabled = False
        self.latency_test_enabled = False
        self.cont_ntrode_enabled = [False for i in range(numntrodes)]
        self.spike_ntrode_enabled = [False for i in range(numntrodes)]
        self.last_stim_time = 0
        self.timestamp = 0
        self.ripple_filter = RippleFilter(self.num_ntrodes)
        self.spatial_filter = SpatialFilter()
        self.theta_filter = ThetaFilter()
        self.latency_filter = LatencyFilter()

        self.record_keepers = [self, self.ripple_filter, self.spatial_filter, self.latency_filter]

        self.last_timestamp = 0
        self.last_stim_state = False

    def reset_realtime_processing(self):
        self.ripple_filter.reinit()
        self.spatial_filter.reinit()
        self.latency_filter.reinit()
        self.last_stim_time = 0

    def get_status_text_bytes(self):
        s = bytearray()
        if self.stim_enabled:
            if self.timestamp - self.last_stim_time > 30000:
                s.extend('Feedback ENABLED\n'.encode('utf-8'))
            else:
                s.extend('Feedback TRIGGERED\n'.encode('utf-8'))
        else:
            s.extend('Feedback OFF\n'.encode('utf-8'))
        return s

    def get_stim_state(self):
        stim = False
        if self.stim_enabled:
            stim = ((not self.spatial_filter.param.enabled or self.spatial_filter.stim_on) and
                    (not self.ripple_filter.param.enabled or self.ripple_filter.trigger_stim))
        return stim

    def create_record_manager(self, label, save_dir, file_prefix, file_postfix):
        self.record_manager = fsUtils.BinaryRecordsManager(label=label, save_dir=save_dir,
                                                           file_prefix=file_prefix,
                                                           file_postfix=file_postfix)
        self.class_log.info('Created BinaryRecordsManager {} with file prefix {}'.
                            format(self.record_manager.label, self.record_manager.file_prefix))

        for record_class in self.record_keepers:    # type: fsUtils.BinaryRecordBase
            self.record_manager.register_rec_type(rec_id=record_class.rec_id,
                                                  rec_labels=record_class.rec_labels,
                                                  rec_struct_fmt=record_class.rec_format)

        new_record_writer = self.record_manager.new_writer()

        for record_class in self.record_keepers:
            record_class.set_record_writer(new_record_writer)

    def start_recording(self):
        self.class_log.info('Received start command for recording')
        for record_class in self.record_keepers:
            record_class.start_record_writing()

    def stop_recording(self):
        self.class_log.info('Received stop command for recording')
        for record_class in self.record_keepers:
            record_class.stop_record_writing()

    def close_record_manager(self):
        self.record_manager.close()
        self.class_log.info('Closed record manager')

    def processdata(self, data_client_info, data):
        # print('process ', data.datatype)
        self.timestamp = data.timestamp

        if isinstance(data, LFPPoint):

            if self.latency_test_enabled:
                if self.latency_filter.process_cont_data(timestamp=self.timestamp,
                                                         ntrode_id=data_client_info.nTrodeIndex):
                    self.supervisor.send_shortcut_message(
                        function_num=self.latency_filter.param.internal.stateScriptFnNum)

            if self.ripple_filter.param.enabled:
                self.ripple_filter.process_ripple_data(data_client_info.nTrodeIndex, data.data,
                                                       self.stim_enabled, self.timestamp, self.last_stim_time)

        elif isinstance(data, SpikePoint):
            self.class_log.info('got spike: timestamp {} len {}'.format(data.timestamp, len(data.data)))

        elif isinstance(data, PosPoint):
            if self.spatial_filter.param.enabled:
                self.spatial_filter.process_pos_data(data.x, data.y, data.timestamp)

        elif isinstance(data, RawPosPoint):
            if self.spatial_filter.param.enabled:
                self.spatial_filter.process_pos_data(data.x1, data.y1, data.timestamp)

        elif isinstance(data, DigIOStateChange):
            if self.latency_filter.param.internal.enabled:
                if self.latency_filter.process_dio_data(self.dout_state, self.timestamp, data.port,
                                                        data.io_dir, data.state):
                    # Trigger function again
                    self.supervisor.send_shortcut_message(self.latency_filter.param.internal.stateScriptFnNum)
                # update the state vectors
                if data.io_dir:
                    self.din_state[data.port-1] = data.state
                else:
                    self.dout_state[data.port-1] = data.state

        elif isinstance(data, SystemTimePoint):
            self.write_record(self.rec_id, data.timestamp, data.tv_sec, data.tv_nsec,
                              time.monotonic(), self.ripple_filter.param.enabled,
                              self.latency_test_enabled)

        # check if filters allow for stim
        stim = self.get_stim_state()

        if self.ripple_filter.param.enabled:
            # When using ripple filter, stim is triggered and not stopped.
            if stim:
                self.last_stim_time = self.timestamp
                self.supervisor.start_stimulation()
        else:
            if stim and not self.last_stim_state:
                self.last_stim_time = self.timestamp
                self.supervisor.start_stimulation()
            elif not stim and self.last_stim_state:
                self.class_log.info('end stim')
                # we had beens stimulating and now we need to stop
                self.supervisor.stop_stimulation()

        self.last_stim_state = stim
