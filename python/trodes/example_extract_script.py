""" Example script for extracting Trodes rec files.

This script, following the Frank Lab directory structure for raw ephys data, will automate extraction
of Trode's rec files into its exploded datatype components, and then will convert the files to an hdf5 storage
to be used for further preprocessing and analysis.

Warning:
    It is recommended when starting that you run each of these commands in an
    interactive python session (ipython or jupyter notebook) so you can inspect
    each extraction step to make sure the files are being created.  Do not run a batch
    script like this until you're sure everything will work properly for your data.

    Depending on your computer, how you parallelize extraction,
    and the amount of data, this process can take many hours to run.
"""

import trodes.trodes_data as td
import time

start_time = time.time()
# setup TrodesAnimalInfo
anim = td.TrodesAnimalInfo('/typhoon/jason/', 'kanye')

# setup raw data extractor
extractor = td.ExtractRawTrodesData(anim)

# parallel extraction of all avaliable dates and epochs
extractor.extract_analog(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)
extractor.extract_dio(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)
extractor.extract_lfp(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)
extractor.extract_mda(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)
extractor.extract_spikes(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)
extractor.extract_time(anim.get_raw_dates(), anim.get_raw_epochs_unionset(), parallel_instances=8)

# prepare directories
extractor.prepare_mountain_dir(anim.get_raw_dates(), anim.get_raw_epochs_unionset())
extractor.prepare_pos_dir(anim.get_raw_dates(), anim.get_raw_epochs_unionset())

end_extract_time = time.time()
print('Extract time {}'.format(end_extract_time-start_time))

# reload TrodesAnimalInfo to get directory structures created during extraction
del anim
anim = td.TrodesAnimalInfo('/typhoon/jason/', 'kanye')

# setup preprocess converter
importer = td.TrodesPreprocessingToAnalysis(anim)

# convert each binaries into hdf5 files
dio_dates = anim.preproc_dio_paths['date'].unique()
for date in dio_dates:
    importer.convert_dio_day(date)

lfp_dates = anim.preproc_LFP_paths['date'].unique()
for date in lfp_dates:
    importer.convert_lfp_day(date)

pos_dates = anim.preproc_pos_paths['date'].unique()
for date in pos_dates:
    importer.convert_pos_day(date)

spk_dates = anim.preproc_spike_paths['date'].unique()
for date in spk_dates:
    importer.convert_spike_day(date, parallel_instances=12)

end_time = time.time()
print('All time {}'.format(end_time-start_time))

