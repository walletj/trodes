/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "streamProcessorThread.h"
#include "globalObjects.h"
#include "trodesSocket.h"
#include "time.h"


StreamProcessorManager::StreamProcessorManager(QWidget *parent, TrodesConfigurationPointers c_ptrs) :
    QObject(parent),
    neuralDataMinMax(nullptr),
    auxDataMinMax(nullptr),
    displaySpikeTicks(false),
    conf_ptrs(c_ptrs)
{
//    neuralDataMinMax.resize(hardwareConf->NCHAN); //some might be dead.
//    neuralDataMinMax = new vertex2d*[hardwareConf->NCHAN]; // some might be dead
//    for (int i=0; i < hardwareConf->NCHAN; i++)
//        neuralDataMinMax[i] = new vertex2d[EEG_TIME_POINTS * 2]; // one for min, one for max
    neuralDataMinMax = new vertex2d[hardwareConf->NCHAN*EEG_TIME_POINTS*2];


    gotSourceFailSignal = false;
    gotHeadstageFailSignal = false;
    PSTHAuxTriggerChannel = -1;
    PSTHAuxTriggerState = true;

    int streamChannels = 0; // the number of channels in the current streamProcessor

    //TODO: have update* funtions provide an ntrode, and have updateChannels be update*, taking an ntrode
    //That should make updating things faster on 1024 channels

    connect(spikeConf, &SpikeConfiguration::updatedRef, this, &StreamProcessorManager::updateChannelsRef);
    connect(spikeConf, &SpikeConfiguration::updatedNotchFilter, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedFilter, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedFilterOn, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedFilter, this, &StreamProcessorManager::updateFilters);
    connect(spikeConf, &SpikeConfiguration::updatedModuleDataFilter, this, &StreamProcessorManager::updateFilters);
    connect(spikeConf, &SpikeConfiguration::updatedModuleData, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedLFPFilter, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedLFPMode, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedSpikeMode, this, &StreamProcessorManager::updateChannels);
    connect(spikeConf, &SpikeConfiguration::updatedAllModes, this, &StreamProcessorManager::updateChannels);

    nTrodeToProcessorThread.fill(-1, spikeConf->ntrodes.length());

    for (int n = 0; n < spikeConf->ntrodes.length(); n++)
        spikeDetectors.append(nullptr); // initialize with NULL

    QList<int> nTrodeList;
//    spikeTickBins.resize(spikeConf->ntrodes.length());
    spikeTickBins = new bool*[spikeConf->ntrodes.length()];
    for (int trodeCount = 0; trodeCount < spikeConf->ntrodes.length(); trodeCount++) {

        //Create lists to store spike times for all nTrodes
//        spikeTickBins[trodeCount].fill(false, EEG_TIME_POINTS);
        spikeTickBins[trodeCount] = new bool[EEG_TIME_POINTS];

        /* The goal of the code below is to create threads with NUMCHANNELSPERSTREAMPROCESSOR channels or fewer each.
         * This is only problematic if a single nTrodes has more than NUMCHANNELSPERSTREAMPROCESSOR channels, in which case it will
         * have it's own thread */

        if ((streamChannels + spikeConf->ntrodes[trodeCount]->maxDisp.length()) > NUMCHANNELSPERSTREAMPROCESSOR) {
            if (spikeConf->ntrodes[trodeCount]->maxDisp.length() > NUMCHANNELSPERSTREAMPROCESSOR){
                // issue a warning
                qDebug() << "[StreamProcessorManager] WARNING: nTrode index" << trodeCount << "has more than" << NUMCHANNELSPERSTREAMPROCESSOR << "channels; Creating dedicated thread";
            }
            if (streamChannels != 0) {
                // We have a set of channels from a previous iteration of the loop, so we create a thread for them and then move on
                createNewProcessorThread(nTrodeList);
                qDebug() << "[StreamProcessorManager] Starting new streamProcessorThread for nTrodes" << nTrodeList;
                createNewSpikeManagerThread(nTrodeList);
                nTrodeList.clear();
                streamChannels = 0;
            }
        }
        // add the current nTrode to the list
        nTrodeList.push_back(trodeCount);
        streamChannels += spikeConf->ntrodes[trodeCount]->maxDisp.length();
    }
    //Create a processor for the last group
    int numHeaderChannels = headerConf->headerChannels.length(); //total number of header channels (aux. analog and digital channels)

    if ((nTrodeList.length() > 0) && (numHeaderChannels == 0)) {
        createNewProcessorThread(nTrodeList);
        qDebug() << "[StreamProcessorManager] Starting new streamProcessorThread for nTrodes" << nTrodeList;
    } else if (numHeaderChannels > 0) {
        qDebug() << "[StreamProcessorManager] Starting new streamProcessorThread for just auxilliary channels";
        QList<int> headerChanList;
//        auxDataMinMax.resize(numHeaderChannels);
//        auxDataMinMax = new vertex2d*[numHeaderChannels]; // some might be dead
//        for (int i=0; i < numHeaderChannels; i++)
//            auxDataMinMax[i] = new vertex2d[EEG_TIME_POINTS * 2]; // one for min, one for max
        auxDataMinMax = new vertex2d[numHeaderChannels*EEG_TIME_POINTS*2];

        for (int headerChan = 0; headerChan < numHeaderChannels; headerChan++) {
            headerChanList.append(headerChan);
            DigitalStateChangeInfo dsci;
            dioStateChanges.append(dsci);
        }
        createNewProcessorThread(nTrodeList,headerChanList);
    }

//    qDebug() << nTrodeToProcessorThread;
    qRegisterMetaType<QVector<int> >("QVector<int>");

    if (nTrodeList.length() > 0) {
        createNewSpikeManagerThread(nTrodeList);
    }

    sumSquares = new int64_t[hardwareConf->NCHAN];
    sumSquaresN = new int[hardwareConf->NCHAN];
    sumSquaresCopy = new int64_t[hardwareConf->NCHAN];
    sumSquaresNCopy = new int[hardwareConf->NCHAN];
    for(int i = 0; i < hardwareConf->NCHAN; ++i){
        sumSquares[i] = 0;
        sumSquaresN[i] = 0;
    }
    connect(&RMSTimer, &QTimer::timeout, this, &StreamProcessorManager::calculateRMS);
}

StreamProcessorManager::~StreamProcessorManager()
{
//    for (int i=0; i < hardwareConf->NCHAN; i++)
//        delete [] neuralDataMinMax[i];
//    delete [] neuralDataMinMax;
    delete [] neuralDataMinMax;

    if (headerConf->headerChannels.length() > 0) {
//        for (int i=0; i < headerConf->headerChannels.length(); i++)
//            delete [] auxDataMinMax[i];
//        delete [] auxDataMinMax;
        delete [] auxDataMinMax;
    }
}

void StreamProcessorManager::sourceFail(bool fail) {
    if (fail && !gotSourceFailSignal) {
        gotSourceFailSignal = true;
        emit sourceFail_Sig(true);
    } else if (!fail) {
        gotSourceFailSignal = false;
        emit sourceFail_Sig(false);
    }
}

void StreamProcessorManager::headstageFail(bool fail) {
    if (fail && !gotHeadstageFailSignal) {
        gotHeadstageFailSignal = true;
        emit headstageFail_Sig(true);
    } else if (!fail) {
        gotHeadstageFailSignal = false;
        emit headstageFail_Sig(false);
    }
}

void StreamProcessorManager::digitalStateChanged(int headerChannelInd, quint32 t, bool state) {


    dioStateChanges[headerChannelInd].timeStamps.push_back(t);
    dioStateChanges[headerChannelInd].states.push_back(state);

    if (dioStateChanges[headerChannelInd].timeStamps.length() > NUMDIGSTATECHANGESTOKEEP) {
        dioStateChanges[headerChannelInd].timeStamps.pop_front();
        dioStateChanges[headerChannelInd].states.pop_front();
    }
}

void StreamProcessorManager::calculateRMS()
{
    QList<qreal> values;
    memcpy(sumSquaresCopy, sumSquares, sizeof(int64_t)*hardwareConf->NCHAN);
    memcpy(sumSquaresNCopy, sumSquaresN, sizeof(int)*hardwareConf->NCHAN);
    memset(sumSquares, 0, sizeof(int64_t)*hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*hardwareConf->NCHAN);

    for(int i = 0; i < hardwareConf->NCHAN; ++i){
        if(sumSquaresNCopy[i]){
            values.append( sqrt((qreal)sumSquaresCopy[i] / (qreal)sumSquaresNCopy[i]) * (qreal)AD_CONVERSION_FACTOR/65535.0);
        }
        else{
            values.append(0);
        }
    }
    emit sendRMSValues(values);
}

void StreamProcessorManager::clearAllDigitalStateChanges() {
    for (int i=0; i<dioStateChanges.length(); i++) {
        dioStateChanges[i].states.clear();
        dioStateChanges[i].timeStamps.clear();
    }
}

void StreamProcessorManager::receiveSpikeEvent(int nTrode, uint32_t time) {

    if (displaySpikeTicks) {
        //A spike occured-- keep track of which ntrode and which time bin in the continuous display the spike occured.
        int binsInPast = (currentTimeStamp-time)/streamProcessors[0]->raw_increment[0];
        int targetBin = streamProcessors[0]->dataIdx - binsInPast;
        if (targetBin < 0) {
            targetBin = EEG_TIME_POINTS+targetBin;
        }

        //If a spike occured, we change the bin to true. This will get reset to false by the streamProcessor threads when the display buffer loops.
        if ((targetBin >= 0) && (targetBin < EEG_TIME_POINTS)) {
            spikeTickBins[nTrode][targetBin] = true;
        }
    }

}

void StreamProcessorManager::setDisplaySpikeTicks(bool on) {

    displaySpikeTicks = on;
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList)
{
    //Creates a new thread and streamProcessor object
    int groupNumber = streamProcessors.length();
    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("StreamProcessor");
    //processorThreads.last()->setPriority(QThread::HighestPriority);
    streamProcessors.push_back(new StreamProcessor(nullptr, groupNumber, nTrodeList, this, conf_ptrs));
    connect(streamProcessors.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    connect(streamProcessors.last(), SIGNAL(sourceFail(bool)),this,SLOT(sourceFail(bool)));
    connect(streamProcessors.last(), SIGNAL(noHeadstageFail(bool)),this,SLOT(headstageFail(bool)));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,quint32,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));
    connect(streamProcessors.last(),SIGNAL(functionTriggerRequest(int)), this, SIGNAL(functionTriggerRequest(int)));
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));

    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start();

    for(int nt : nTrodeList){
        nTrodeToProcessorThread[nt] = groupNumber;
    }
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList, QList<int> auxChanList)
{
    //Creates a new thread and streamProcessor object

    int groupNumber = streamProcessors.length();
    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("StreamProcessor");
    //processorThreads.last()->setPriority(QThread::HighestPriority);
    streamProcessors.push_back(new StreamProcessor(nullptr, groupNumber, nTrodeList, this, conf_ptrs));
    streamProcessors.last()->addAuxChannels(auxChanList);
    connect(streamProcessors.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    connect(streamProcessors.last(), SIGNAL(sourceFail(bool)),this,SLOT(sourceFail(bool)));
    connect(streamProcessors.last(), SIGNAL(noHeadstageFail(bool)),this,SLOT(headstageFail(bool)));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,quint32,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));
    connect(streamProcessors.last(),SIGNAL(functionTriggerRequest(int)), this, SIGNAL(functionTriggerRequest(int)));
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));

    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start();

    for(int nt : nTrodeList){
        nTrodeToProcessorThread[nt] = groupNumber;
    }
}

void StreamProcessorManager::createNewSpikeManagerThread(QList<int> nTrodeList)
{
    spikeDetectorThreads.push_back(new QThread);
    spikeDetectorThreads.last()->setObjectName("SpikeDetector");
    spikeDetectorManagers.push_back(new SpikeDetectorManager(nullptr, nTrodeList, &spikeDetectors)); // spikeDetectors is going to be populated
    connect(spikeDetectorManagers.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    spikeDetectorManagers.last()->moveToThread(spikeDetectorThreads.last());
    connect(spikeDetectorThreads.last(), SIGNAL(started()), spikeDetectorManagers.last(), SLOT(setupAndRun()));
    connect(spikeConf, SIGNAL(newThreshold(int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int)));

    //connect(spikeConf, SIGNAL(newThreshold(int, int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int, int)));
    connect(spikeConf, SIGNAL(newTriggerMode(int, int, bool)), spikeDetectorManagers.last(), SLOT(updateSpikeMode(int, int, bool)));
    spikeDetectorThreads.last()->start();
}


void StreamProcessorManager::startAcquisition()
{
    gotSourceFailSignal = false;
    emit startAllProcessorLoops(); //sends signal to all processor threads to start their loops

}

void StreamProcessorManager::stopAcquisition()
{

    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->quitNow = 1;
    }
}

void StreamProcessorManager::removeAllProcessors()
{
    stopAcquisition();

    for (int i = 0; i < streamProcessors.length(); i++) {
        processorThreads[i]->quit();
        processorThreads[i]->deleteLater();
        streamProcessors[i]->deleteLater();
    }

    for (int i = 0; i < spikeDetectorManagers.length(); i++) {
        spikeDetectorManagers[i]->stopRunning();
    }

    //This is a bit of a hack, and will cause a crash upon shutdown if any of the StreamProcessors have
    //not finished running.  Ideally, the StreamProcessor threads would send a signal when they are done that
    //is used to delete the spike detection threads, but because there are multiple StreamProcessors and multiple
    //spikeDetectorThreads, this is complicated to set up.  So, for now, we sleep for long enough to ensure that
    //the StreamProcessors have all shut down.
    QThread::msleep(100);
    for (int i = 0; i < spikeDetectorThreads.length(); i++) {
        spikeDetectorThreads[i]->quit();
        spikeDetectorThreads[i]->deleteLater();
        spikeDetectorManagers[i]->deleteLater();

    }
}

QVector<uint32_t> StreamProcessorManager::getDigitalEventTimes() {

    QVector<uint32_t> outputTimes;
    if (PSTHAuxTriggerChannel < dioStateChanges.length() && PSTHAuxTriggerChannel > -1) {
        for (int i=0; i < dioStateChanges[PSTHAuxTriggerChannel].timeStamps.length(); i++) {
            if (dioStateChanges[PSTHAuxTriggerChannel].states[i] == PSTHAuxTriggerState) {
                outputTimes.push_back(dioStateChanges[PSTHAuxTriggerChannel].timeStamps[i]);
            }
        }
    }
    return outputTimes;

}

QString StreamProcessorManager::getPSTHTriggerID()
{
    if (headerConf != nullptr && PSTHAuxTriggerChannel > -1) {
        return headerConf->headerChannels[PSTHAuxTriggerChannel].idString;

    } else {
        return "";
    }
}

bool StreamProcessorManager::getPSTHTriggerState() {
    return PSTHAuxTriggerState;
}

void StreamProcessorManager::enableRMSCalculations(bool on)
{
    RMSEnabled = on;
    if(on){
        RMSTimer.start(1000);
    }
    else{
        RMSTimer.stop();
    }
}

void StreamProcessorManager::setOneSecBin(){
    if(RMSTimer.interval()==1000){
        return;
    }
    memset(sumSquares, 0, sizeof(int64_t)*hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*hardwareConf->NCHAN);
    RMSTimer.setInterval(1000);
}
void StreamProcessorManager::setTenSecBin(){
    if(RMSTimer.interval()==10000){
        return;
    }
    memset(sumSquares, 0, sizeof(int64_t)*hardwareConf->NCHAN);
    memset(sumSquaresN, 0, sizeof(int)*hardwareConf->NCHAN);
    RMSTimer.setInterval(10000);
}

void StreamProcessorManager::updateDataLength(double tlength)
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->newDataLength = hardwareConf->sourceSamplingRate * tlength;
        //When this flag is set to true, the thread's loop will update the trace length and reset the flag to false
        streamProcessors[i]->updateDataLengthFlag = true;
    }
}

void StreamProcessorManager::updateFilters(QList<int> ntrodes){
    for(int i = 0; i < ntrodes.length(); ++i){
        int nt = ntrodes[i];
        streamProcessors[nTrodeToProcessorThread[nt]]->updateChannelsFlag = 3;
    }
}

void StreamProcessorManager::updateChannels()
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->updateChannelsFlag = 1;
    }
}

void StreamProcessorManager::updateChannelsRef(){

    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->updateChannelsFlag = 2;
    }
}

void StreamProcessorManager::createSpikeLogs(QString dataDir)
{
//  for (int i = 0; i < nTrodeTriggerProcessors.length(); i++) {
//    nTrodeTriggerProcessors[i]->createLogFile(dataDir);
//  }
}

void StreamProcessorManager::setPSTHTrigger(int headerChannel, bool state)
{
    qDebug() << "[StreamProcessorManager] PSTH trigger set";
    PSTHAuxTriggerChannel = headerChannel;
    PSTHAuxTriggerState = state;
}

//This object is used to pull out raw data for a set of channels, filter the signals, and
//calculate what should be displayed on the stream display. It runs inside a seperate thread.

StreamProcessor::StreamProcessor(QObject*, int groupNumber, QList <int> nTList, StreamProcessorManager* managerPtr, TrodesConfigurationPointers c_ptrs) :
    conf_ptrs(c_ptrs),
    groupNum(groupNumber),
    nTrodeList(nTList),
    streamManager(managerPtr)
{


    dataLength = conf_ptrs.hardwareConf->sourceSamplingRate * conf_ptrs.streamConf->tLength;
    isLooping = false;
    nAuxChan = 0;
    hasDigIO = false;
    hasAuxAnalog = false;
    isSetup = false;

    nChan = 0;
    for (int trode = 0; trode < nTList.length(); trode++) {
        nChan = nChan + conf_ptrs.spikeConf->ntrodes[trode]->hw_chan.length();
        nTrodeIdList.push_back(conf_ptrs.spikeConf->ntrodes[nTrodeList[trode]]->nTrodeId);
    }

}

StreamProcessor::~StreamProcessor()
{
    if (contDataHandlers.length() > 0) {
        foreach(TrodesSocketMessageHandler * mh, contDataHandlers)
        {
            if (mh != nullptr) {
                mh->closeConnection();
                mh->deleteLater();
            }
        }
    }
    if (digitalIOHandler != nullptr) {
        if (digitalIOHandler != nullptr) {
            digitalIOHandler->closeConnection();
            digitalIOHandler->deleteLater();
        }
    }
    if (analogIOHandler != nullptr) {
        if (analogIOHandler != nullptr) {
            analogIOHandler->closeConnection();
            analogIOHandler->deleteLater();
        }
    }
    if (blockContinuousHandler != nullptr) {
        if (blockContinuousHandler != nullptr) {
            blockContinuousHandler->closeConnection();
            blockContinuousHandler->deleteLater();
        }
    }



    /*for (int i = 0; i < nTrodeList.length(); i++) {
        delete [] dataPoints[i];
    }
    delete [] dataPoints;*/

    delete neuralDataHandler;

}

void StreamProcessor::addAuxChannels(QList<int> auxList) {
    //Can only add aux channels before setUp() has been executed
    if (!isSetup) {
        for (int i=0; i<auxList.length(); i++) {
            auxChannelList.append(auxList[i]);
        }
        //auxChannelList = auxList;
        nAuxChan = auxList.length(); // For header group, this is just the length of header channels

        for (int i=0; i < nAuxChan; i++) {
            if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::DIGITALTYPE) {
                hasDigIO = true;
            } else if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::INT16TYPE) {
                hasAuxAnalog = true;
            } else if (conf_ptrs.headerConf->headerChannels[i].dataType == DeviceChannel::UINT32TYPE) {
                //Nothing here yet.
            }

        }
    }
}

void StreamProcessor::setUp() {
    //dataFilters = new BesselFilter[nChan];

    neuralDataHandler = new NeuralDataHandler(conf_ptrs,nTrodeList);

    //Calculate how many data points go into one display pixel on the stream plot
    raw_increment.resize(EEG_TIME_POINTS);
    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);
    }

    //Create a new semaphore for this group of channels.
    //if this is not the first time a workspace has been loaded, then there will already be semaphores in place.
    //Only create more if they are needed.
    rawDataAvailableMutex.lock();
    while (groupNum >= rawDataAvailable.length()) {
        rawDataAvailable.append(new QSemaphore);
    }
    rawDataAvailableMutex.unlock();

    nTrodeToLocal.fill(-1, conf_ptrs.spikeConf->ntrodes.length());

    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        int nt = nTrodeList.at(trode);
        spikeModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode);
        lfpModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode);
        triggered.push_back(false);
        nTrodeToLocal[nt] = trode;
    }
    ntrodesUpdated.reserve(nTrodeList.length());

    //dataPoints = new int16_t*[nTrodeList.length()];
    rawDataAvailableIdx = groupNum;
    for (int nt = 0; nt < nTrodeList.length(); nt++) {
        //Get channel-specific info from the loaded configuration file


        // if we haven't added it already, add this to the data available index list
        if (dataProvided.contNTrodeIndexList.indexOf(nTrodeList.at(nt)) == -1) {
            dataProvided.contNTrodeIndexList.push_back(nTrodeList.at(nt));
        }

        //dataMinMax is what keeps the currently plotted data (for streaming page)
        //For each horizontal pixel, it keeps the maximum and minimum values that occured
        //in the data stream for that channel during that time window. Minimum and maximum
        //values alternate (which is why the array is twice as long as EEE_TIME_POINTS).

        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length(); c++) {
            int ch = conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan[c];
//            streamManager->neuralDataMinMax[ch].resize((int)EEG_TIME_POINTS * 2);
            for (int j = 0; j < EEG_TIME_POINTS; j++) {
                int ind = ch*EEG_TIME_POINTS*2 + 2*j;
                streamManager->neuralDataMinMax[ind].x = (double)j;
                streamManager->neuralDataMinMax[ind].y = 0;
                streamManager->neuralDataMinMax[ind+1].x = (double)j;
                streamManager->neuralDataMinMax[ind+1].y = 0;
            }
        }

        //dataPoints[nt] = new int16_t[conf_ptrs.spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length()];
    }

    for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //This is not for neural channels (digital inputs, aux analog inputs,...)
        int hdrChan = auxChannelList.at(auxCh);
        if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
        } else if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::UINT32TYPE) &&
                   (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
           interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
       }
//        streamManager->auxDataMinMax[hdrChan].resize((int)EEG_TIME_POINTS * 2);
        for (int j = 0; j < EEG_TIME_POINTS; j++) {
            int ind = hdrChan*EEG_TIME_POINTS*2;
            streamManager->auxDataMinMax[ind + j*2].x = (double)j;
            streamManager->auxDataMinMax[ind + j*2].y = 0;
            streamManager->auxDataMinMax[ind + j*2+1].x = (double)j;
            streamManager->auxDataMinMax[ind + j*2+1].y = 0;
        }
    }

    rawIdx = 0;
    dataIdx = 0;


    dataProvided.moduleID = TRODES_ID;
    dataProvided.socketType = conf_ptrs.networkConf->dataSocketType;

    // Clear the handlers, for UDP they will be setup if thread's hadDigIO is set.
    // for TCP they'll be setup by the client
    digitalIOHandler = nullptr;
    analogIOHandler = nullptr;
    blockContinuousHandler = nullptr;

    // we need to create a server or servers to send out data. The type of server is specified
    // in the network configuration if it was defined. Otherwise default is TCP_IP
    if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        dataServer = new TrodesServer();
        if (conf_ptrs.networkConf->networkConfigFound) {
            //if the config file designates the address , use it
            dataServer->setAddress(conf_ptrs.networkConf->trodesHost);
        }
        dataServer->startLocalServer("streamManager");

        connect(dataServer, SIGNAL(newDataHandler(TrodesSocketMessageHandler*, qint16)), this, SLOT(newDataHandler(TrodesSocketMessageHandler*, qint16)));
        // Fill out the rest of the dataProvided structure for this streamManager
        dataProvided.hostName = dataServer->getCurrentAddress();
        dataProvided.hostPort = dataServer->getCurrentPort();
    }
    else if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
        dataProvided.hostName = conf_ptrs.networkConf->trodesHost;
        // will set up UDP sockets individually as we go through the dataTypes below
    }

    // set the dataType
    qDebug() << "[StreamProcessor] started server on port" << dataProvided.hostPort
             << "nTrodes indeces" << dataProvided.contNTrodeIndexList;

    dataProvided.dataType = 0;
    if (hasDigIO) {
        dataProvided.dataType |= TRODESDATATYPE_DIGITALIO;
        if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            // create a message handler for this socket. For TCPIP this will all happen as the client connects
            // create a udp server for this nTrode
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            digitalIOHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_DIGITALIO);
            dataProvided.digitalIOUDPPort = trodesUDPSocket->getCurrentPort(); // set the UDP port in dataProvided

        }
    }
    if (hasAuxAnalog) {
        dataProvided.dataType |= TRODESDATATYPE_ANALOGIO;
        if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            analogIOHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_ANALOGIO);
            dataProvided.analogIOUDPPort = trodesUDPSocket->getCurrentPort(); // set the UDP port in dataProvided
        }
    }
    if (nChan > 0) {
        dataProvided.dataType |= TRODESDATATYPE_CONTINUOUS;
        if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            for (int trode = 0; trode < nTrodeList.length(); trode++) {
                // create a udp server for this nTrode
                TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
                TrodesSocketMessageHandler *newHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_CONTINUOUS);
                // for continuous data we can now just call newDataHandler to add it to the list
                newDataHandler(newHandler, nTrodeList.at(trode));
                dataProvided.contNTrodeUDPPortList.append(trodesUDPSocket->getCurrentPort());

                qDebug() << " -- Creating a UDP socket for continuous data from trode "
                         << trode << " on port"
                         << trodesUDPSocket->getCurrentPort() ;
            }
        }

#if NUMCHANNELSPERSTREAMPROCESSOR >= 128
        dataProvided.dataType |= TRODESDATATYPE_BLOCK_CONTINUOUS;
#endif
        if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            // create a udp server for this nTrode
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            TrodesSocketMessageHandler *newHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_BLOCK_CONTINUOUS);
            newDataHandler(newHandler, 0);
            dataProvided.blockContinuousUDPPort = trodesUDPSocket->getCurrentPort();

            qDebug() << " -- Creating a UDP socket for block continuous data on port" <<
                        dataProvided.blockContinuousUDPPort ;
        }

    }

    // add that dataProvided structure to the main list
    emit addDataProvided(&dataProvided);

    quitNow = 0;
    updateChannelsFlag = 0;
    updateDataLengthFlag = false;

    // if this handles digital IO data, allocate space for a variable that will hold the current digital IO state
    if (hasDigIO || hasAuxAnalog) {

        digStates.resize(conf_ptrs.headerConf->headerChannels.length());
        // fill the array with false to initialize
        digStates.fill(false);

        /*
        digInState = new bool[headerConf->maxDigitalPort(true)];
        digOutState = new bool[headerConf->maxDigitalPort(false)];

        // fill the arrays with zeros to initialize
        memset(digInState, 0, headerConf->maxDigitalPort(true) * sizeof(bool));
        memset(digOutState, 0, headerConf->maxDigitalPort(false) * sizeof(bool));*/
    }


    connect(conf_ptrs.spikeConf, SIGNAL(updatedModuleData()), this, SLOT(updateModuleDataChan()));

    isSetup = true;
}


void StreamProcessor::updateDataLength(void)
{
    // Note that this is safe only because we've chosen to have a constant number of points for
    // the time base of the display. We would need to be more careful if raw_increment was going
    // to change size, in order to not have thread clashes....
    qDebug() << "[StreamProcessor] Old dataLength " << dataLength
             << "New dataLength " << newDataLength;

    dataLength = newDataLength;
    raw_increment.resize(dataLength);

    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);

        //There is really no need to update the xaxis on the traces, so for now we skip this step
        //This means that the xlabels are fixed to whatever values they had in the constructor,
        //and as long as we do not update them in the stream display widget, everything should work fine.
    }
    dataIdx = 0;
    qDebug() << "[StreamProcessor] New dataLength set";
}



void StreamProcessor::updateChannels(void)
{

    //Edit: Now keeping track of all relevant values in **local copies** instead of accessing globals.

//    qDebug() << "[StreamProcessor] Updating channels";
    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        //notchFiltersOn[trode] = spikeConf->ntrodes[nt]->notchFilterOn;
        //spikeFiltersOn[trode] = spikeConf->ntrodes[nt]->filterOn;
        //lfpFiltersOn[trode] = spikeConf->ntrodes[nt]->lfpFilterOn;
        spikeModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode;
        lfpModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode;

    }

    neuralDataHandler->updateChannels();

}

void StreamProcessor::updateChannelsRef(){
    /*for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        lfpRefOn[trode] = spikeConf->ntrodes[nt]->lfpRefOn;
        refOn[trode] = spikeConf->ntrodes[nt]->refOn;
        rawRefOn[trode] = spikeConf->ntrodes[nt]->rawRefOn;
        refChan[trode] = spikeConf->ntrodes.refOfIndex(nt)->hw_chan[spikeConf->ntrodes[nt]->refChan];
        groupRefOn[trode] = spikeConf->ntrodes[nt]->groupRefOn;
        refGroup[trode] = spikeConf->ntrodes[nt]->refGroup;
        //qDebug() << "Reference for nTrode" << trode << "is" << refChan[trode] << spikeConf->ntrodes[nt]->hw_chan[0] << lfpRefOn[trode] << refOn[trode] << rawRefOn[trode] << groupRefOn[trode];
    }*/

    neuralDataHandler->updateChannelsRef();
}

void StreamProcessor::updateFilters(){
    /*for(int i = 0; i < nTrodeList.length(); ++i){
        const int nt = nTrodeList[i];
        for(int c = 0; c < spikeConf->ntrodes[nt]->hw_chan.length(); ++c){
            spikeFilters[i][c]->setFilterRange(spikeConf->ntrodes[nt]->lowFilter, spikeConf->ntrodes[nt]->highFilter);
            notchFilters[i][c]->setBandwidth(spikeConf->ntrodes[nt]->notchBW);
            notchFilters[i][c]->setNotchFreq(spikeConf->ntrodes[nt]->notchFreq);
        }
        lfpFilters[i]->setFilterRange(0, spikeConf->ntrodes[nt]->moduleDataHighFilter);
        moduleDataChan[i] = spikeConf->ntrodes[nt]->moduleDataChan;
    }*/

    neuralDataHandler->updateFilters();
}

void StreamProcessor::setupdateFiltersFlag(){
    updateChannelsFlag = 3;
}


void StreamProcessor::updateModuleDataChan()
{
    for (int i = 0; i < contDataHandlers.length(); i++) {
        int tempIndex = contDataHandlers[i]->getNTrodeIndex();
        contDataHandlers[i]->setNTrodeChan(conf_ptrs.spikeConf->ntrodes[tempIndex]->moduleDataChan);
        int highFilterVal = conf_ptrs.spikeConf->ntrodes[tempIndex]->moduleDataHighFilter;
        filtersForContinuousSendChannels[i]->setFilterRange(0, highFilterVal);
    }

    if (filtersForBlockContinuousSendChannels.length() > 0) {
        for (int n = 0; n < nTrodeList.length(); n++) {
            int nt = nTrodeList.at(n); // the nTrodeList may not be identical to all the nTrodes
            int highFilterVal = conf_ptrs.spikeConf->ntrodes[nt]->moduleDataHighFilter;
            filtersForBlockContinuousSendChannels[n]->setFilterRange(0, highFilterVal);
            // Will update channel when looping
        }
    }
}



void StreamProcessor::newDataHandler(TrodesSocketMessageHandler *messageHandler, qint16 requestedNTrodeIndex)
{
    // check to the requested datatype
    if (messageHandler->getDataType() == TRODESDATATYPE_CONTINUOUS) {
//        qDebug() << "  StreamProcessor geting new data hander for nTrodeIndex " << requestedNTrodeIndex <<
//                    "local nTrodeIndeces" << nTrodeList << "port" << messageHandler->udpSocket->localPort();

        // set the information for this handler
        contDataHandlers.push_back(messageHandler);
        // check to see if the requested NTrode is available, and put out an error if not
        if (nTrodeIdList[nTrodeList.indexOf(requestedNTrodeIndex)] == -1) {
            qDebug() << "[StreamProcessor::newDataHandler] Error: got request for non-existent NTrodeIndex" << requestedNTrodeIndex;
            return;
        }
        contDataHandlers.last()->setNTrodeId(nTrodeIdList[nTrodeList.indexOf(requestedNTrodeIndex)]);
        contDataHandlers.last()->setNTrodeIndex(requestedNTrodeIndex);

        int tmpNTrodeIndex = contDataHandlers.last()->getNTrodeIndex();
        contDataHandlers.last()->setNTrodeChan(conf_ptrs.spikeConf->ntrodes[tmpNTrodeIndex]->moduleDataChan);

        filtersForContinuousSendChannels.push_back(new BesselFilter());

        //Not sure if we want the full sampling rate here...
        filtersForContinuousSendChannels.last()->setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);

        int highFilterVal = conf_ptrs.spikeConf->ntrodes[tmpNTrodeIndex]->moduleDataHighFilter;
        filtersForContinuousSendChannels.last()->setFilterRange(0, highFilterVal);
        //updateFiltersForContinuousSend();
        qDebug() << "StreamProcessor got new data hander #" << contDataHandlers.length() << "nTrodeIndex" << requestedNTrodeIndex << "highFilter" << highFilterVal;
    }
    else if (messageHandler->getDataType() == TRODESDATATYPE_DIGITALIO) {
        //qDebug() << "  StreamProcessor:" << "newDataHandler():" << "DigitalIOHandler";
        digitalIOHandler = messageHandler;
    }
    else if (messageHandler->getDataType() == TRODESDATATYPE_ANALOGIO) {
        analogIOHandler = messageHandler;
    }
    else if (messageHandler->getDataType() == TRODESDATATYPE_BLOCK_CONTINUOUS) {
        //qDebug() << "  StreamProcessor:" << "newDataHandler():" << "BlockContinuousHandler";
        blockContinuousHandler = messageHandler;

        for (int n = 0; n < nTrodeList.length(); n++) {
            filtersForBlockContinuousSendChannels.push_back(new BesselFilter());
            filtersForBlockContinuousSendChannels.last()->setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);
            int nt = nTrodeList.at(n); // the nTrodeList may not be identical to all the nTrodes
            int highFilterVal = conf_ptrs.spikeConf->ntrodes[nt]->moduleDataHighFilter;
            filtersForBlockContinuousSendChannels.last()->setFilterRange(0, highFilterVal);
        }
    }

    connect(messageHandler, SIGNAL(socketDisconnected()), this, SLOT(removeDataHandler()),Qt::QueuedConnection);
}

void StreamProcessor::removeDataHandler() {
    TrodesSocketMessageHandler* senderDataHandler = static_cast<TrodesSocketMessageHandler*>(sender());

    // Only remove datahandlers if TCPIP, if UDP then socket handlers should persist
    if (conf_ptrs.networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        if(senderDataHandler->getDataType() == TRODESDATATYPE_CONTINUOUS) {
            int senderInd = contDataHandlers.indexOf(senderDataHandler);
            if(senderInd != -1) {
                dataHandlersOn.removeOne(senderInd);
                contDataHandlers.removeAt(senderInd);
                filtersForContinuousSendChannels.removeAt(senderInd);
                qDebug() << "[StreamProcessor] Successfully removed continuous dataHandler index " << senderInd;
            } else {
                qDebug() << "[StreamProcessor] Error: tried to remove continuous dataHandler that doesn't exist";
            }
        } else if(senderDataHandler->getDataType() == TRODESDATATYPE_DIGITALIO) {
            digitalIOHandler = nullptr;
        } else if(senderDataHandler->getDataType() == TRODESDATATYPE_ANALOGIO) {
            analogIOHandler = nullptr;
        } else if(senderDataHandler->getDataType() == TRODESDATATYPE_BLOCK_CONTINUOUS) {
            blockContinuousHandler = nullptr;
        }
    }

}

void StreamProcessor::runLoop()
{
    //This is where the raw data is filtered, and where the streaming display is calculated.
    //In each display bin, we draw a vertical line connecting the minumim and maximum collected values
    //in that time bin.
    //When the 'stream from source' menu is chosen, this function is executed via an emitted signal
    //from the source controller

    int samplesToCopy = 0;

    quitNow = 0;
    isLooping = true;
    bool exitLoop = false;

    int16_t tmpDataPoint = 0;
    int16_t tmpMaxPoint = 0;
    int16_t tmpMinPoint = 0;
    int rdInd;

    bool newDigIOState;
    int port;
    char* startBytePtr; //for processing header channels
    uint8_t* interleavedDataIDBytePtr; //for processing header channels that are interleaved

    rawIdx = 0;
    dataIdx = 0;
    streamDataRead = 0;

    numLoopsWithNoData = 0;
    numLoopsWithNoHeadstageData = 0;

    int headerSize = conf_ptrs.hardwareConf->headerSize;
//    if (hardwareConf->sysTimeIncluded) {
//        headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//    } else {
//        headerSize = hardwareConf->headerSize;
//    }
    //Setting this to true enabled a simple closed-loop latency test.  If used, select one (and only one) of the digital
    //output channels for analysis.  Then, in stateScript, set the channel to high.  When the high state is dected, this processor will call function 1.
    //Function 1 should 1) Set the channel to low, and 2) set it back to 1 after about 100ms (the test interval). Streamprocessor will measure the amount of time it
    //takes from the high edge to the low edge for each test.

    bool closedLoopLatencyTest = false;


    uint32_t latencyReceiveEventTime;
    uint32_t latencyReturnEventTime;


    int hw_chan = 0;
    int stream_inc = 0;

    uint32_t timestamp;

    QVector<int16_t> lfpDataBlock(nTrodeList.length());

    for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //Reset any interleaved aux channels to 0
        int hdrChan = auxChannelList.at(auxCh);
        if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
        } else if ((conf_ptrs.headerConf->headerChannels[hdrChan].dataType == DeviceChannel::UINT32TYPE) &&
                   (conf_ptrs.headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
           interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hdrChan].idString] = 0;
       }
    }
    bool nodatafail = false;  //flag for no data error
    bool noheadstagefail = false;
    qDebug() << "[StreamProcessor] Starting stream processor";



    QThread::usleep(200); //This appears to solve a race condition that sometimes occurs.  TODO: a real solution

    while (!exitLoop) {
        // checking event loop after processing buffer
        QCoreApplication::processEvents();
        if (rawDataAvailable[rawDataAvailableIdx]->tryAcquire(1, 100)) {

            samplesToCopy = rawDataAvailable[rawDataAvailableIdx]->available();
            numLoopsWithNoData = 0;
            if (nodatafail) {
                nodatafail = false;
                emit sourceFail(false); //reset source failure signal
            }
            if (samplesToCopy > 15000) {
                emit bufferOverrun();
            }
            if (!rawDataAvailable[rawDataAvailableIdx]->tryAcquire(samplesToCopy))
                qDebug() << "[StreamProcessor] Error acquiring available samples, group " << groupNum;

            samplesToCopy += 1; // we acquired one at the beginning

            //Create a list of all data handlers that are on
            dataHandlersOn.clear();
            for (int d = 0; d < contDataHandlers.length(); d++) {
                if (contDataHandlers[d]->isModuleDataStreamingOn()) {
                    dataHandlersOn.push_back(d);
                }
            }


            for (int s = 0; s < samplesToCopy; s++) {
                // -----------------------------------------------------------------
                // LOAD NEW DATA STRUCTURE


                rdInd = rawIdx * conf_ptrs.hardwareConf->NCHAN;

                timestamp = rawData.timestamps[rawIdx];
                int sysTimestamp = -1;
                if (conf_ptrs.benchConfig->isRecordingSysTime()) {
                    sysTimestamp = rawData.sysTimestamps[rawIdx];
                }
                numLoopsWithNoHeadstageData++; //assume headstage data is bad/fake

                neuralDataHandler->processNextSamples(timestamp,rawData.data+rdInd,rawData.carvals+(rawIdx*conf_ptrs.spikeConf->carGroups.length()));
                if (!noheadstagefail && neuralDataHandler->numLoopsWithNoHeadstageData > 1000) {
                    noheadstagefail = true;
                    //qDebug() << "No data!";
                    emit noHeadstageFail(true);
                } else if (noheadstagefail && neuralDataHandler->numLoopsWithNoHeadstageData == 0) {
                    noheadstagefail = false;
                    emit noHeadstageFail(false);
                }
                for (int n = 0; n < nTrodeList.length(); n++) {
                    // the nTrodeList may not be identical to all the nTrodes in the case were there are multple streamProcessorThreads
                    int nt = nTrodeList.at(n);
                    int modDataChan = moduleDataChan[n];

                    for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {

                        hw_chan = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan[c];

                        //sum of squares calculation
                        int unRefVal = (int)neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_UNREFERENCED)[c];
                        streamManager->sumSquares[hw_chan] += unRefVal*unRefVal;
                        streamManager->sumSquaresN[hw_chan]++;

                        // Send data to modules if enabled
                        int nTrodeId = conf_ptrs.spikeConf->ntrodes.at(nt)->nTrodeId;
                        for (int d = 0; d < dataHandlersOn.length(); d++) {
                            int dHandlerInd = dataHandlersOn[d];
                            if ((contDataHandlers[dHandlerInd]->getNTrodeId() == nTrodeId) && (contDataHandlers[dHandlerInd]->getNTrodeChan() == c) ) {

                                int16_t sendDataPoint = filtersForContinuousSendChannels[dHandlerInd]->addValue(neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_UNREFERENCED)[c]);
                                if ((timestamp % contDataHandlers[dHandlerInd]->getDecimation()) == 0) {
                                    if (timestamp == 0) {
                                        qDebug() << "[StreamProcessor] WARNING: sent a 0 timestamp" << ". Current thread: " << QThread::currentThreadId();
                                    }
                                    contDataHandlers[dHandlerInd]->sendContinuousDataPoint(timestamp, sendDataPoint);
                                }

                            }
                        }

                        // Send data to modules if enabled
                        if (blockContinuousHandler != nullptr) {
                            if (blockContinuousHandler->isModuleDataStreamingOn()) {
                                if (modDataChan == c) {

                                    int16_t sendDataPoint = filtersForBlockContinuousSendChannels[n]->addValue(neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_UNREFERENCED)[c]);
                                    if ((timestamp % blockContinuousHandler->getDecimation()) == 0) {
                                        lfpDataBlock[n] = sendDataPoint;
                                    }
                                }
                            }
                        }

                        //Assign filtered values where apropriate to the display
                        if(lfpModeOn[n]){
                            // Assign display to lfp
                            tmpDataPoint = neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::LFP)[0];
                        } else if(spikeModeOn[n]){
                            // Assign stream display whatever the spike display gets
                            tmpDataPoint = neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::SPIKE)[c];
                        } else {
                            tmpDataPoint = neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_REFERENCED)[c];
                        }

                        tmpMaxPoint = tmpDataPoint;
                        tmpMinPoint = tmpDataPoint;


                        // Calulate the current max and min values in the current display bin
                        int curIndex = hw_chan*EEG_TIME_POINTS*2 + dataIdx*2;
                        if (stream_inc > 0) {
                           //Old bin, so compare to existing value
                            streamManager->neuralDataMinMax[curIndex].y =
                                    qMax((GLfloat)tmpMaxPoint, streamManager->neuralDataMinMax[curIndex].y);
                            streamManager->neuralDataMinMax[curIndex+1].y =
                                    qMin((GLfloat)tmpMinPoint, streamManager->neuralDataMinMax[curIndex+1].y);
                        }
                        else {
                            //New bin, so reset the value
                            streamManager->neuralDataMinMax[curIndex].y = tmpMaxPoint;
                            streamManager->neuralDataMinMax[curIndex+1].y = tmpMinPoint;
                        }

                    }
                    //MARK: time
                    //raw data sent to spike detectors
                    streamManager->spikeDetectors[nt]->newData(neuralDataHandler->getNTrodeSamples(n,AbstractNeuralDataHandler::SPIKE), timestamp, sysTimestamp);

                    if (stream_inc == 0) {
                        streamManager->spikeTickBins[nt][dataIdx] = false; //Reset the spike tick bin to 0
                    }

                }
                if (blockContinuousHandler != nullptr) {
                    if (blockContinuousHandler->isModuleDataStreamingOn()) {

                        if ((timestamp % blockContinuousHandler->getDecimation()) == 0) {
                            blockContinuousHandler->sendContinuousDataBlock(timestamp, lfpDataBlock);
                        }
                    }
                }


                //Now we process the auxilliary channels (digial I/O, analog I/O), if any
                for (int h = 0; h < auxChannelList.length(); h++) {
                    int hch = auxChannelList[h];
                    startBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[h].startByte;

                    if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        if (conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.

                                tmpDataPoint = (int16_t)((*startBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].digitalBit)) >>
                                                 conf_ptrs.headerConf->headerChannels[hch].digitalBit);
                                interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString] = tmpDataPoint;
                            } else {
                                //Use the last data point received
                                tmpDataPoint = interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString];
                            }
                        } else {
                            tmpDataPoint = (int16_t)((*startBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].digitalBit)) >>
                                             conf_ptrs.headerConf->headerChannels[hch].digitalBit);
                        }

                    }
                    else if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::INT16TYPE) {
                        // TO DO: add analogIO output
                        if (conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << conf_ptrs.headerConf->headerChannels[hch].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                                interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString] = tmpDataPoint;
                            } else {
                                //Use the last data point received
                                tmpDataPoint = interleavedAuxChannelStates[conf_ptrs.headerConf->headerChannels[hch].idString];
                            }
                        } else {

                            tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                        }
                    }


                    if (conf_ptrs.headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        newDigIOState = false;

                        port = conf_ptrs.headerConf->headerChannels[hch].port;

                        // check if this is an input, and if so, if the state of the port has changed
                        if ((bool)tmpDataPoint != digStates[hch]) {
                            newDigIOState = true;
                            digStates[hch] = (bool)tmpDataPoint;

                            //Send the change event info to the StreamProcessorManager, which
                            //keeps a record of all the events.
                            //We should have a gate for this to allow users to exclude
                            //channels that are changing often

                            if (conf_ptrs.headerConf->headerChannels[hch].storeStateChanges) {

                                //For latency testing
                                if (closedLoopLatencyTest) {
                                    if (digStates[hch]){
                                        emit functionTriggerRequest(1);
                                        latencyReceiveEventTime = timestamp;
                                    } else {
                                        latencyReturnEventTime = timestamp;
                                        qDebug() << "Closed loop latency: " << latencyReturnEventTime-latencyReceiveEventTime;
                                    }
                                }

                                // check to see if we are could send out data
                                if (digitalIOHandler != nullptr) {
                                    // if the state changed and we're supposed to stream data, send out the new port status.
                                    if ((newDigIOState) && (digitalIOHandler->isModuleDataStreamingOn())) {
                                        digitalIOHandler->sendDigitalIOData(timestamp, port,
                                                                            (char)conf_ptrs.headerConf->headerChannels[hch].input,
                                                                            (char)tmpDataPoint);
                                    }
                                }


                                emit digitalStateChanged(hch,timestamp,digStates[hch]);
                            }
                        }


                    }

                    //If the signals are huge, clip them in the display.  TODO:  use the max display setting for this channel as the clip.

                    //tmpMaxPoint = qMin(tmpDataPoint, 2);
                    //tmpMinPoint = qMax(tmpDataPoint, -1);

                    //Calulate the current max and min values in the current display bin
                    int curIndex = hch*EEG_TIME_POINTS*2 + dataIdx * 2;
                    if (stream_inc > 0) {
                        //Old bin, so compare to existing value
                        streamManager->auxDataMinMax[curIndex].y =
                                qMax((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[curIndex].y);
                        streamManager->auxDataMinMax[curIndex + 1].y =
                                qMin((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[curIndex + 1].y);
                    }
                    else {
                        //New bin, so reset the value
                        streamManager->auxDataMinMax[curIndex].y = tmpDataPoint;
                        streamManager->auxDataMinMax[curIndex + 1].y = tmpDataPoint;
                    }
                }


                rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;
                if (++stream_inc >= raw_increment[dataIdx]) {
                    dataIdx = (dataIdx + 1) % EEG_TIME_POINTS;
                    stream_inc = 0;
                }

            }

        } else if (quitNow == 1) {
            exitLoop = true;
            isLooping = false;
        } else {

            numLoopsWithNoData++;
            if (!nodatafail && numLoopsWithNoData > 5) {
                numLoopsWithNoData = 0;
                nodatafail = true;
                //qDebug() << "No data!";
                emit sourceFail(true);
            }
        }

        if (updateChannelsFlag.testAndSetAcquire(1, 0)) {
            updateChannels();
        }
        if (updateChannelsFlag.testAndSetAcquire(2, 0)){
            updateChannelsRef();
        }
        if (updateChannelsFlag.testAndSetAcquire(3, 0)){
            updateFilters();
        }
        if (updateDataLengthFlag.testAndSetAcquire(true, false)) {
            updateDataLength();
        }
        // check to see if data are available on any of the dataHandlers
        for (int i = 0; i < contDataHandlers.length(); i++) {
            if (((contDataHandlers[i]->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                    (contDataHandlers[i]->tcpSocket->waitForReadyRead(0))) ||
               ((contDataHandlers[i]->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                   (contDataHandlers[i]->udpSocket->hasPendingDatagrams()))) {
                //qDebug() << "reading message on dataHandler" << i;
                contDataHandlers[i]->readMessage();
            }
        }


        if (digitalIOHandler != nullptr) {
            if (((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                    (digitalIOHandler->tcpSocket->waitForReadyRead(0))) ||
                ((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                    (digitalIOHandler->udpSocket->hasPendingDatagrams()))) {
                //qDebug() << "reading message on digitalIOHandler";
                digitalIOHandler->readMessage();
            }
        }
        // TODO: Analog data
    }

    qDebug() << "[StreamProcessor] Stream processor loop ended.";
}
