/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STREAMPROCESSOR_H
#define STREAMPROCESSOR_H



#include <QtGui>
#include <QGLWidget>
#include "configuration.h"
#include "iirFilter.h"
#include "globalObjects.h"
#include "sharedVariables.h"
#include "trodesSocket.h"
#include "spikeDetectorThread.h"
#include "frequencyspectrum.h"
#include "spectrum.h"
#include "FFTRealFixLenParam.h"
#include "streamprocesshandlers.h"

#define NUMCHANNELSPERSTREAMPROCESSOR 64 //this defines the max number of channels per processor thread

#define NUMDIGSTATECHANGESTOKEEP 10000


typedef struct {
  int ch, ref;
} chan;

Q_DECLARE_METATYPE(QVector<int>);

class StreamProcessorManager;

class StreamProcessor: public QObject {

  Q_OBJECT

public:
  StreamProcessor(QObject *parent, int groupNumber, QList<int> nTList, StreamProcessorManager* managerPtr, TrodesConfigurationPointers c_ptrs);

  ~StreamProcessor();

  TrodesConfigurationPointers conf_ptrs;
  int groupNum;
  int quitNow;
  bool isLooping;
  QAtomicInt updateChannelsFlag; // triggers run loop to update channels
  QAtomicInt updateDataLengthFlag; // triggers run loop to update channels
  double newDataLength;
  int dataIdx;


  QList<int> nTrodeList;
  QList<int> nTrodeIdList;
  QVector<int> nTrodeToLocal;

  QVector<int> auxChannelList;

  quint64 streamDataRead;

  void addAuxChannels(QList<int> auxList);

  int rawIdx;
  QVector <int> raw_increment;


private:
  NeuralDataHandler* neuralDataHandler;
  int nChan; // number of eeg channels to display
  int nAuxChan;
  bool hasDigIO;
  bool hasAuxAnalog;

  double dataLength;
  int numLoopsWithNoData;
  int numLoopsWithNoHeadstageData;

  bool isSetup;


  StreamProcessorManager* streamManager;
  //QVector<vertex2d>* dataMinMax;

  QVector<bool> notchFiltersOn;
  QVector<bool> spikeFiltersOn; //Renamed from filtersOn
  QVector<bool> lfpFiltersOn;
  QVector<bool> triggered;
  QVector<bool> lfpModeOn;
  QVector<bool> spikeModeOn;
  QVector<bool> lfpRefOn;
  QVector<bool> refOn;
  QVector<bool> rawRefOn;
  QVector<int>  moduleDataChan;
  QVector<int>  refChan;
  QVector<bool> groupRefOn;
  QVector<int> refGroup;


  QVector<int> ntrodesUpdated;
  QVector<QVector<BesselFilter*> > spikeFilters;
  QVector<BesselFilter*> lfpFilters;
  QVector<QVector<NotchFilter*> > notchFilters;

//  SpikeConfStructure
  //each processor has their own set of spikeconf->ntrode objects
  //or just have each processor have their own set of local arrays for spikeConf

  QList<BesselFilter*> filtersForContinuousSendChannels;
  QList<BesselFilter*> filtersForBlockContinuousSendChannels;

  //BesselFilter* dataFilters;

  QMutex waitConditionMutex;
  int rawDataAvailableIdx;



  // If we are sending out continuous data to another module, we have a list of message handlers (one for each NTrode)
  // In addition, we have a single data handler for blocks of continuous data, and separate data handlers for digital or analog IO data
  QList<TrodesSocketMessageHandler *> contDataHandlers;
  TrodesSocketMessageHandler *blockContinuousHandler;
  TrodesSocketMessageHandler *analogIOHandler;
  TrodesSocketMessageHandler *digitalIOHandler;

  QVector<quint16> dataHandlersOn; // Copies the "moduleDataOn" flag from the handler

  QList<int> mhNTrodeIndex;  //

  QList<int> chanDataHandler; // the indeces of data handlers for the selected channels

  TrodesServer *dataServer;
  DataTypeSpec dataProvided;

  QHash<QString, int> interleavedAuxChannelStates;
  QVector<bool> digStates;

  //bool *digInState; // the last state of the configured digital input ports ; allocated during Setup
  //bool *digOutState; // the last state of the configured digital output ports ; allocated during Setup

  int decimation; // the factor by which the data should be decimated.

  int16_t** dataPoints;

public:
  void updateDataLength(void);
  void updateChannels(void);
  void updateChannelsRef();
  void updateFilters();

public slots:
  void runLoop();
  void setUp();
  void updateModuleDataChan();

  void setupdateFiltersFlag();
private slots:
  void newDataHandler(TrodesSocketMessageHandler* messageHandler, qint16 requestedNTrodeIndex);
  void removeDataHandler();


signals:
   void bufferOverrun();
   void addDataProvided(DataTypeSpec *dp);
   void sourceFail(bool fail);
   void noHeadstageFail(bool fail);
   void digitalStateChanged(int channelNum, quint32 t, bool state);
   void functionTriggerRequest(int funcNum);

};


struct DigitalStateChangeInfo {
    QList<uint32_t> timeStamps;
    QList<bool> states;
};

class StreamProcessorManager : public QObject {

  Q_OBJECT

public:
  StreamProcessorManager(QWidget *parent, TrodesConfigurationPointers c_ptrs);
  ~StreamProcessorManager();

  TrodesConfigurationPointers conf_ptrs;
  QVector<uint32_t>            getDigitalEventTimes();
  QString                      getPSTHTriggerID();
  bool                         getPSTHTriggerState();

  vertex2d                     *neuralDataMinMax;
  vertex2d                     *auxDataMinMax;
//  QVector<QVector<bool> >         spikeTickBins;
  bool **spikeTickBins; //something about using qt containers across threads causes crashes.
  bool                          displaySpikeTicks;
  //QList <Trigger*>             nTrodeTriggerProcessors; //each nTrode gets one thread to detect spike triggers
  QList<StreamProcessor*>      streamProcessors; //threads to process the continous streams of data
  QList<SpikeDetectorManager*>  spikeDetectorManagers; // pointers to containers of spike detectors
  QList<ThresholdSpikeDetector*> spikeDetectors; // list of spike detectors by ntrode

  int64_t                       *sumSquares; //used to calculate RMS
  int                           *sumSquaresN; //number of samples;
  int64_t                       *sumSquaresCopy;
  int                           *sumSquaresNCopy;
  bool                          RMSEnabled;
  QTimer                        RMSTimer;
  void enableRMSCalculations(bool on);

private:
  QList<QThread*>               processorThreads;
  QList<DigitalStateChangeInfo>    dioStateChanges;
  int                           PSTHAuxTriggerChannel;
  bool                          PSTHAuxTriggerState;



  bool                          gotSourceFailSignal;
  bool                          gotHeadstageFailSignal;
  void                          createNewProcessorThread(QList<int> nTrodeList);
  void                          createNewProcessorThread(QList<int> nTrodeLis,QList<int> auxChanList);
  QList<QThread*>               spikeDetectorThreads;
  void                          createNewSpikeManagerThread(QList<int> nTrodeList);

  QVector<int> nTrodeToProcessorThread;
public slots:
    void startAcquisition();
    void stopAcquisition();
    void removeAllProcessors();
    void updateDataLength(double tlength);
    void updateFilters(QList<int>);
    void updateChannels();
    void updateChannelsRef();
    void createSpikeLogs(QString dataDir);
    void sourceFail(bool fail);
    void headstageFail(bool fail);
    void setPSTHTrigger(int headerChannel, bool state);
    void clearAllDigitalStateChanges();
    void receiveSpikeEvent(int nTrode, uint32_t time);
    void setDisplaySpikeTicks(bool on);

    void setOneSecBin();
    void setTenSecBin();

private slots:
    void digitalStateChanged(int headerChannelInd, quint32 t, bool state);
    void calculateRMS();

signals:

    void startAllProcessorLoops();
    void addDataProvided(DataTypeSpec *dp);
    void bufferOverrun();
    void createSpikeLogs_signalRelay(QString dataDir);
    void signal_newContinuousHandler(TrodesSocketMessageHandler* messageHandler, qint16 nTrode);
    void moduleDataChannelChanged(int nTrode, int chan);
    void moduleDataChannelFilterChanged(int nTrode, int upperCutOff);
    void sourceFail_Sig(bool fail);
    void headstageFail_Sig(bool fail);
    void functionTriggerRequest(int funcNum);

    void sendRMSValues(QList<qreal> values);

};


#endif // STREAMPROCESSOR_H
