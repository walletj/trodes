/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USBDAQTHREAD_H
#define USBDAQTHREAD_H



#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"

#ifdef WIN32
    #include <windows.h>
	#include "ftd2xx.h"   
#endif
#ifdef __APPLE__
    #include "WinTypes.h"
    #include "ftd2xx.h"   
#endif
#ifdef linux
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif


#define VENDOR 0x0403
#define DEVICE 0x6010

extern FT_STATUS res;
extern FT_HANDLE ftdi;

class USBDAQRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  USBDAQRuntime(QObject *parent);
  QVector<unsigned char> buffer;

public slots:
  void Run(void);

};

class USBDAQInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  USBDAQInterface(QObject *parent);
  ~USBDAQInterface(void);
  int state;
  quint64 getTotalDroppedPacketEvents();
  HardwareControllerSettings lastControllerSettings;
  HeadstageSettings          lastHeadstageSettings;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  USBDAQRuntime *usbDataProcessor;
  QThread       *workerThread;

private slots:
  void restartThread();

public slots:
  void InitInterface(void);
  void StartAcquisition(void);
  void StartSimulation(void);
  void StopAcquisition(void);
  void CloseInterface(void);
  void SendSettleCommand(void);
  void SendSDCardUnlock(void);
  void ConnectToSDCard(void);
  void ReconfigureSDCard(int numChannels);
  void SendHeadstageSettings(HeadstageSettings s);
  void SendControllerSettings(HardwareControllerSettings s);
  HeadstageSettings GetHeadstageSettings();
  HardwareControllerSettings GetControllerSettings();
  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  int MeasurePacketLength(HeadstageSettings settings);
  static bool isSourceAvailable();
};



#endif // USBDAQTHREAD_H
