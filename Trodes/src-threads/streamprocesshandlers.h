#ifndef STREAMPROCESSHANDLERS_H
#define STREAMPROCESSHANDLERS_H

#include <stdint.h>
#include <QtGlobal>
#include "configuration.h"
#include "iirFilter.h"

class AbstractNeuralDataHandler
{
public:
    AbstractNeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in);
    virtual ~AbstractNeuralDataHandler();
    enum DataBand {RAW_UNREFERENCED,RAW_REFERENCED, LFP, SPIKE};

    //Virtual public methods (implemeted in abstract class, but can be reimplemented)
    virtual void updateChannels();
    virtual void updateChannelsRef();
    virtual void updateFilters();


    //Must be implemented in inheriting class!
    virtual void processNextSamples(uint32_t timestamp,int16_t* neuralDataBlock, int16_t* CARDataBlock) = 0;

    //Public methods
    const int16_t* getNTrodeSamples(int nTrodeIndex,DataBand d);

    void resetFilters();
    void setSpikeInvert(bool invert);
    void setInterpMode(bool on, uint32_t maxGapSize);
    int getSamplesLeftToInterpolate();
    void stepPreviousSample();
    bool gapOccured();
    uint32_t getSamplesProcessed();

    //Error handling access
    int numLoopsWithNoHeadstageData;
    bool latestDataHasNan;

 protected:

    //Protected methods
    void switchDataPointers();

    //Assorted protected variables
    int NCHAN;
    TrodesConfigurationPointers conf_ptrs;
    QList<int> nTrodeList;
    bool spikeInvert;

    bool interpMode;
    uint32_t maxInterpGap;
    int numSamplesLeftToInterp;
    bool interpValuesReadyToFetch;
    bool gapInDataOccured;
    uint32_t samplesProcessed;

    //Settings
    QVector<bool> notchFiltersOn;
    QVector<bool> spikeFiltersOn; //Renamed from filtersOn
    QVector<bool> lfpFiltersOn;
    QVector<bool> lfpRefOn;
    QVector<bool> refOn;
    QVector<bool> rawRefOn;
    QVector<int>  moduleDataChan;
    QVector<int>  refChan;
    QVector<bool> groupRefOn;
    QVector<int>  refGroup;

    //Filters
    QVector<QVector<BesselFilter*> > spikeFilters;
    QVector<BesselFilter*> lfpFilters;
//    QVector<QVector<NotchFilter*> > notchFilters;

    //Timestamp
    uint32_t timestamp_latest;
    uint32_t timestamp_previous;

    //Pointers to data for the latest and the previous packets
    int16_t** spike_band_dataPoints_latest;
    int16_t* lfp_band_dataPoints_latest;
    int16_t** raw_band_dataPoints_latest;
    int16_t** raw_unreferenced_band_dataPoints_latest;
    int16_t** spike_band_dataPoints_previous;
    int16_t* lfp_band_dataPoints_previous;
    int16_t** raw_band_dataPoints_previous;
    int16_t** raw_unreferenced_band_dataPoints_previous;
    int16_t** spike_band_dataPoints_interpolated;
    int16_t* lfp_band_dataPoints_interpolated;
    int16_t** raw_band_dataPoints_interpolated;
    int16_t** raw_unreferenced_band_dataPoints_interpolated;
    bool storageGate; //directs which container to point to (latest vs previous packet)

    //data containers for latest and previous packets
    int16_t** spike_band_dataPoints;
    int16_t* lfp_band_dataPoints;
    int16_t** raw_band_dataPoints;
    int16_t** raw_unreferenced_band_dataPoints;
    int16_t** spike_band_dataPoints2;
    int16_t* lfp_band_dataPoints2;
    int16_t** raw_band_dataPoints2;
    int16_t** raw_unreferenced_band_dataPoints2;

private:



};

class NeuralDataHandler : public AbstractNeuralDataHandler
{
public:
    NeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in);
    //Public methods

    virtual void processNextSamples(uint32_t timestamp,int16_t* neuralDataBlock, int16_t* CARDataBlock);


};


class AuxDataHandler
{
public:
    AuxDataHandler(TrodesConfigurationPointers conf, QVector<int> auxChannelList_in);
    ~AuxDataHandler();
    void processNextSamples(uint32_t timestamp,int16_t* auxDataBlock);

private:
    TrodesConfigurationPointers conf_ptrs;
    QVector<int> auxChannelList;
    bool hasDigIO;
    bool hasAuxAnalog;
    QHash<QString, int> interleavedAuxChannelStates;
    QVector<bool> digStates;
    QVector<bool> triggered;

    //Timestamp
    uint32_t timestamp_latest;
    uint32_t timestamp_previous;
};

#endif // STREAMPROCESSHANDLERS_H
