include(../build_defaults.pri)

TARGET = Trodes

QT += opengl widgets xml multimedia multimediawidgets testlib charts

#The TRODES_CODE define is necessary to allow code to be excluded for modules
DEFINES += TRODES_CODE

INCLUDEPATH += src-main
INCLUDEPATH += src-display
INCLUDEPATH += src-config
INCLUDEPATH += src-threads
INCLUDEPATH += $$TRODES_REPO_DIR/Modules/workspaceGUI/
INCLUDEPATH += $$PWD/../3rdParty/fftreal


#########################################
#Source files and libraries
#########################################
# Use Qt Resource System for images, across mac/linux/win
RESOURCES += \
    $$PWD/../Resources/Images/buttons.qrc


HEADERS   += src-main/sharedVariables.h \
    src-config/configuration.h \
    src-display/rfwidget.h \
#    src-threads/dockusbthread.h \
    src-threads/dockusbthread.h \
    src-threads/streamprocesshandlers.h \
    src-threads/usbdaqThread.h \
    src-threads/audioThread.h \
    src-main/iirFilter.h \
    src-threads/triggerThread.h \
    src-main/globalObjects.h\
    src-main/mainWindow.h\
    src-threads/streamProcessorThread.h \
    src-threads/recordThread.h \
    src-display/streamDisplay.h \
    src-main/cocoaInitializer.h \
    src-threads/simulateDataThread.h \
    src-main/sourceController.h \
    src-threads/fileSourceThread.h \
    src-display/spikeDisplay.h \
    src-display/dialogs.h \
    src-main/networkMessage.h \
    src-main/trodesSocket.h\
    src-main/customApplication.h \
    src-main/trodesSocketDefines.h \
    src-threads/ethernetSourceThread.h \
    src-main/abstractTrodesSource.h \
    src-threads/simulateSpikesThread.h \
    src-threads/spikeDetectorThread.h \
    src-main/trodesdatastructures.h \
    src-main/eventHandler.h \
    src-main/trodesSplashScreen.h \
    ../Modules/workspaceGUI/workspaceEditor.h \
    src-display/sharedtrodesstyles.h \
    src-main/benchmarkWidget.h \
    src-display/quicksetup.h \
    src-config/hardwaresettings.h \
    src-display/workspaceeditordialog.h \
    src-main/analyses.h \
    src-display/cargrouppanel.h \
    src-display/rmsplot.h

SOURCES   += \
    src-display/rfwidget.cpp \
#    src-threads/dockusbthread.cpp \
    src-threads/dockusbthread.cpp \
    src-threads/streamprocesshandlers.cpp \
    src-threads/usbdaqThread.cpp \
    src-threads/audioThread.cpp \
    src-main/iirFilter.cpp \
    src-threads/triggerThread.cpp \
    src-config/configuration.cpp\
    src-main/main.cpp\
    src-main/mainWindow.cpp\
    src-threads/streamProcessorThread.cpp \
    src-threads/recordThread.cpp \
    src-display/streamDisplay.cpp \
    src-threads/simulateDataThread.cpp \
    src-main/sourceController.cpp \
    src-threads/fileSourceThread.cpp \
    src-display/spikeDisplay.cpp \
    src-display/dialogs.cpp \
    src-main/trodesSocket.cpp \
    src-main/customApplication.cpp \
    src-threads/ethernetSourceThread.cpp \
    src-main/abstractTrodesSource.cpp \
    src-threads/simulateSpikesThread.cpp \
    src-threads/spikeDetectorThread.cpp \
    src-main/trodesdatastructures.cpp \
    src-main/eventHandler.cpp \
    src-main/trodesSplashScreen.cpp \
    ../Modules/workspaceGUI/workspaceEditor.cpp \
    src-display/sharedtrodesstyles.cpp \
    src-main/benchmarkWidget.cpp \
    src-display/quicksetup.cpp \
    src-display/workspaceeditordialog.cpp \
    src-main/analyses.cpp \
    src-display/cargrouppanel.cpp \
    src-display/rmsplot.cpp

# FFTReal
HEADERS  += ../3rdParty/fftreal/Array.h \
            ../3rdParty/fftreal/Array.hpp \
            ../3rdParty/fftreal/DynArray.h \
            ../3rdParty/fftreal/DynArray.hpp \
            ../3rdParty/fftreal/FFTRealFixLen.h \
            ../3rdParty/fftreal/FFTRealFixLen.hpp \
            ../3rdParty/fftreal/FFTRealFixLenParam.h \
            ../3rdParty/fftreal/FFTRealPassDirect.h \
            ../3rdParty/fftreal/FFTRealPassDirect.hpp \
            ../3rdParty/fftreal/FFTRealPassInverse.h \
            ../3rdParty/fftreal/FFTRealPassInverse.hpp \
            ../3rdParty/fftreal/FFTRealSelect.h \
            ../3rdParty/fftreal/FFTRealSelect.hpp \
            ../3rdParty/fftreal/FFTRealUseTrigo.h \
            ../3rdParty/fftreal/FFTRealUseTrigo.hpp \
            ../3rdParty/fftreal/OscSinCos.h \
            ../3rdParty/fftreal/OscSinCos.hpp \
            ../3rdParty/fftreal/def.h

# Wrapper used to export the required instantiation of the FFTRealFixLen template
HEADERS  += ../3rdParty/fftreal/fftreal_wrapper.h
SOURCES  += ../3rdParty/fftreal/fftreal_wrapper.cpp

#Code for using fftreal for spectral analysis (from Qt)
HEADERS  += ../3rdParty/fftreal/frequencyspectrum.h \
            ../3rdParty/fftreal/spectrum.h \
            ../3rdParty/fftreal/spectrumanalyser.h \
            ../3rdParty/fftreal/utils.h

SOURCES  += ../3rdParty/fftreal/frequencyspectrum.cpp \
            ../3rdParty/fftreal/spectrumanalyser.cpp \
            ../3rdParty/fftreal/utils.cpp





OTHER_FILES += \
    src-main/cocoaInitializer.mm

unix:!macx {
    INCLUDEPATH += Libraries/Linux
    LIBS += -LLibraries/Linux -lftd2xx
    HEADERS    += Libraries/Linux/WinTypes.h Libraries/Linux/ftd2xx.h
}

win32{
    RC_ICONS += trodesIcon.ico
    LIBS += -lOpengl32

    INCLUDEPATH += Libraries/Windows
    HEADERS    += Libraries/Windows/ftd2xx.h
#    win32-g++ { # MinGW
#        LIBS += -L$$quote($$PWD/Libraries/Windows/FTDI/i386) -lftd2xx
#    }
#    else {
        !contains(QT_ARCH, x86_64) : { # 32-bit
            LIBS   += $$quote($$PWD/Libraries/Windows/FTDI/i386/ftd2xx.lib)
        } else { # 64-bit
            LIBS   += $$quote($$PWD/Libraries/Windows/FTDI/amd64/ftd2xx.lib)
        }
#    }
}

#stuff specific for mac
macx {
    ICON        = src-main/trodesIcon.icns
    QMAKE_INFO_PLIST += src-main/Info.plist


    INCLUDEPATH += Libraries/Mac
    HEADERS    += Libraries/Mac/WinTypes.h \
                    Libraries/Mac/ftd2xx.h
    LIBS       += $$quote($$PWD/Libraries/Mac/libftd2xx.a)

#    LIBS += -L./Libraries/Mac -lftd2xx
}
#########################################
#INSTALLATION: LINUX
#########################################
unix:!macx{
    #FTDXX library
    #libraries.path is set in build_defaults.pri
    libraries.files += $$PWD/Libraries/Linux/libftd2xx.so*
}

#########################################
#INSTALLATION: WINDOWS
#########################################
win32:{
#libraries.path is set in build_defaults.pri
#    win32-g++ { # MinGW
#        libraries.files += $$PWD/Libraries/Windows/FTDI/i386/ftd2xx.dll
#    }
#    else {
        !contains(QMAKE_TARGET.arch, x86_64) : { # 32bit
            libraries.files += $$PWD/Libraries/Windows/FTDI/i386/ftd2xx.dll
        } else { # 64bit
            libraries.files += $$PWD/Libraries/Windows/FTDI/amd64/ftd2xx64.dll
        }
#    }

    ftdxxsetup.path = $$INSTALL_DIR/Resources/SetupHelp/Windows/
    ftdxxsetup.files += $$TRODES_REPO_DIR/Resources/SetupHelp/Windows/CDM21228_Setup.exe
    INSTALLS += ftdxxsetup
}

#########################################
#INSTALLATION: MACOS
#########################################
macx{
#    libraries.files +=  $$PWD/Libraries/Mac/libftd2xx.dylib \
#                        $$PWD/Libraries/Mac/libftd2xx.1.4.4.dylib
}
#########################################
#Install
#########################################

#Test files
testing.path = $$INSTALL_DIR
testing.files += $$TRODES_REPO_DIR/Resources/Testing

#Matlab placeholder
matlabplaceholder.path = $$INSTALL_DIR
matlabplaceholder.files += $$TRODES_REPO_DIR/Resources/trodes_path_placeholder.m

#Release notes
releasenotes.path = $$INSTALL_DIR
releasenotes.files += $$TRODES_REPO_DIR/ReleaseNotes

#Rest of resources
resources.path = $$INSTALL_DIR/Resources
resources.files += \
    $$TRODES_REPO_DIR/Resources/SetupHelp \
    $$TRODES_REPO_DIR/Resources/SampleWorkspaces \
    $$TRODES_REPO_DIR/Resources/Images \
    $$TRODES_REPO_DIR/Modules/stateScript/matlabObserver \
    $$TRODES_REPO_DIR/Modules/stateScript/pythonObserver

#!macx{
INSTALLS += libraries testing matlabplaceholder releasenotes resources
#}
#else{
#QMAKE_BUNDLE_DATA += libraries testing matlabplaceholder releasenotes resources
#}

#########################################
#          OLD
##################################################################################
##-------------------------------------------------
## Settings required to use the Intan/OpenEphys interface
##
## NOTE: you must link libudev.so to libudev.so.0!!
##  For Ubuntu 14.04, this is ln -s /usr/lib/x86_64-linux-gnu/libudev.so /usr/lib/x86_64-linux-gnu/libudev.so.0
##
## Uncomment the following line for OpenEphys ability
##CONFIG += RHYTHMINTERFACE

#RHYTHMINTERFACE {
#DEFINES += RHYTHM

#SOURCES +=     src-threads/rhythmThread.cpp \
#    \ # src-threads/rhythm-api/okFrontPanelDLL.cpp \
#    src-threads/rhythm-api/rhd2000datablock.cpp \
#    src-threads/rhythm-api/rhd2000evalboard.cpp \
#    src-threads/rhythm-api/rhd2000registers.cpp
#HEADERS +=     src-threads/rhythmThread.h \
#    src-threads/rhythm-api/okFrontPanelDLL.h \
#    src-threads/rhythm-api/rhd2000datablock.h \
#    src-threads/rhythm-api/rhd2000evalboard.h \
#    src-threads/rhythm-api/rhd2000registers.h

##stuff specific for mac
#macx {
#    LIBS += /usr/local/lib/libftd2xx.dylib
#    LIBS += -L"Libraries/Mac" -lokFrontPanel
#    #LIBS += -F$$quote($$PWD/Libraries/Mac) $$quote($$PWD/Libraries/Mac/libokFrontPanel.dylib)
#    copy_opalkelly_lib.commands = cp $$quote($$PWD/Libraries/Mac/libokFrontPanel.dylib) $$quote($$DESTDIR/Trodes.app/Contents/MacOS/); \
#                                  cp $$quote($$PWD/../Resources/Rhythm/main.bit) $$quote($$DESTDIR/Trodes.app/Contents/MacOS/)

## Set the install_name of libokFrontPanel.dylib to its absolute location so that it will
## be linked correctly
##OPAL_DYLIB=$${PWD}/Libraries/Mac/libokFrontPanel.dylib
##copy_opalkelly_lib.commands += "install_name_tool -id $$OPAL_DYLIB $$OPAL_DYLIB && echo \"==> libokFrontPanel.dylib install_name updated before build:\" ;"
##copy_opalkelly_lib.commands += "otool -L $$OPAL_DYLIB | head -2;"



#}

#unix:!macx {
#    LIBS += -ldl # needed for dynamic linking to FrontPanel library
#    LIBS += -L$$quote($$PWD/Libraries/Linux) -lokFrontPanel
#    copy_opalkelly_lib.commands = cp $$quote($$PWD/Libraries/Linux/libokFrontPanel.so) $$quote($$DESTDIR/); \
#                                  cp $$quote($$PWD/../Resources/Rhythm/main.bit) $$quote($$DESTDIR/);
#    libraries.files += $$PWD/Libraries/Linux/libokFrontPanel.so
#}

#win32 {
#    win32-g++ { # MinGW
#       # LIBS  += -L$$shell_path($$PWD/Trodes/Libraries/Windows/OpalKelly/Win32/) -lokFrontPanel
#        LIBS += -L$$quote($$PWD/Libraries/Windows/OpalKelly/Win32) -lokFrontPanel

#        copy_opalkelly_lib.commands =  cmd /c copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
#                                         cmd /c copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))

##        copy_opalkelly_lib.commands =  cp $$quote($$shell_path($$PWD/Trodes/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)$$escape_expand(\n\t)) $$quote($$shell_path($$DESTDIR/)) && \
##                                       cp $$quote($$shell_path($$PWD/Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
#    }
#    else {
#        !contains(QMAKE_TARGET.arch, x86_64) : { # MSVC-32
#            LIBS   += $$quote($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.lib)
#            copy_opalkelly_lib.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/Win32/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
#                                         copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
#        } else { # MSVC-64
#            LIBS   += $$PWD/Libraries/Windows/OpalKelly/x64/okFrontPanel.lib
#            copy_opalkelly_lib.commands = copy /y $$quote($$shell_path($$PWD/Libraries/Windows/OpalKelly/x64/okFrontPanel.dll)) $$quote($$shell_path($$DESTDIR/)) && \
#                                        copy /y $$quote($$shell_path($$PWD/../Resources/Rhythm/main.bit)) $$quote($$shell_path($$DESTDIR/))
#        }
#    }
#}


##QMAKE_EXTRA_TARGETS += copy_opalkelly_lib
##POST_TARGETDEPS += copy_opalkelly_lib
#first.depends = $(first) copy_opalkelly_lib
#export(first.depends)
#export(copy_opalkelly_lib.commands)
#QMAKE_EXTRA_TARGETS += first copy_opalkelly_lib
#}
##-------------------------------------------------


