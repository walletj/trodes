#ifndef WORKSPACEEDITORDIALOG_H
#define WORKSPACEEDITORDIALOG_H

#include <QtWidgets>
#include "workspaceEditor.h"
#include "hardwaresettings.h"

class WorkspaceEditorDialog : public QDialog {
    Q_OBJECT
public:
    WorkspaceEditorDialog(QWidget *parent = 0);
    void loadFileIntoWorkspaceGui(QString filename);
//    void enableOpenButton();
    void fillInWorkspace(HeadstageSettings headstageSettings, bool ecuconnected, bool rfconnected, int chansperntrode, int psize, bool useSysClock = true);
    WorkspaceEditor *workspaceGui;
protected:
    void closeEvent(QCloseEvent * event);

private:
    QVBoxLayout     *mainLayout;
    QGridLayout     *buttonBar;

    QAction *actionSave;
    QAction *actionSaveAs;
    QAction *actionLoad;

    QPushButton     *buttonCancel;
    QPushButton     *buttonSave;
    QPushButton     *buttonSaveAs;
    QPushButton     *buttonLoadChannelMapping;
    QPushButton     *buttonOpen;

public slots:
    int openEditor(void);
    int openReconfigEditor(void);
    void clearGUI(void);


private slots:
    void buttonCancelPressed(void);
    void buttonSavePressed(void);
    void buttonSaveAsPressed(void);
    void buttonLoadChannelMapPressed(void);



signals:
    void sig_openTempWorkspace(QString path);
};
#endif // WORKSPACEEDITORDIALOG_H
