var classwaveform_generator_dialog =
[
    [ "waveformGeneratorDialog", "classwaveform_generator_dialog.html#ae74e1c6f2ab77259bc7e79ffa958c4ad", null ],
    [ "closeEvent", "classwaveform_generator_dialog.html#a7103d8cb39bbcf258d9db14fded115bc", null ],
    [ "windowClosed", "classwaveform_generator_dialog.html#a8d2a57bc8d46ea5b7c95ce27d9f6a165", null ],
    [ "ampDisplay", "classwaveform_generator_dialog.html#ad8a185ba3b9c7c23d5cb7b22e25e8b58", null ],
    [ "ampSlider", "classwaveform_generator_dialog.html#a99ad4693d1f6caa62af8cb7ffbfe0155", null ],
    [ "ampTitle", "classwaveform_generator_dialog.html#adf444881d35611185c0787f71a1d1274", null ],
    [ "freqDisplay", "classwaveform_generator_dialog.html#a86926ef5bbdac369751e47cf9f5663da", null ],
    [ "freqSlider", "classwaveform_generator_dialog.html#affd5d471b40ac82aa1a4f1ccb4748ad9", null ],
    [ "freqTitle", "classwaveform_generator_dialog.html#a963f9034bef77a4c3353d49c986e6183", null ]
];