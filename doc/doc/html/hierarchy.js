var hierarchy =
[
    [ "_EVENT_HANDLE", "struct___e_v_e_n_t___h_a_n_d_l_e.html", null ],
    [ "_ft_device_list_info_node", "struct__ft__device__list__info__node.html", null ],
    [ "_FTCOMSTAT", "struct___f_t_c_o_m_s_t_a_t.html", null ],
    [ "_FTDCB", "struct___f_t_d_c_b.html", null ],
    [ "_FTTIMEOUTS", "struct___f_t_t_i_m_e_o_u_t_s.html", null ],
    [ "_OVERLAPPED", "struct___o_v_e_r_l_a_p_p_e_d.html", null ],
    [ "_SECURITY_ATTRIBUTES", "struct___s_e_c_u_r_i_t_y___a_t_t_r_i_b_u_t_e_s.html", null ],
    [ "chan", "structchan.html", null ],
    [ "CocoaInitializer", "class_cocoa_initializer.html", null ],
    [ "eegDataBuffer", "structeeg_data_buffer.html", null ],
    [ "ft_eeprom_2232", "structft__eeprom__2232.html", null ],
    [ "ft_eeprom_2232h", "structft__eeprom__2232h.html", null ],
    [ "ft_eeprom_232b", "structft__eeprom__232b.html", null ],
    [ "ft_eeprom_232h", "structft__eeprom__232h.html", null ],
    [ "ft_eeprom_232r", "structft__eeprom__232r.html", null ],
    [ "ft_eeprom_4232h", "structft__eeprom__4232h.html", null ],
    [ "ft_eeprom_header", "structft__eeprom__header.html", null ],
    [ "ft_eeprom_x_series", "structft__eeprom__x__series.html", null ],
    [ "ft_program_data", "structft__program__data.html", null ],
    [ "headerChannel", "classheader_channel.html", null ],
    [ "iirFilter", "classiir_filter.html", null ],
    [ "libusb_config_descriptor", "structlibusb__config__descriptor.html", null ],
    [ "libusb_control_setup", "structlibusb__control__setup.html", null ],
    [ "libusb_device_descriptor", "structlibusb__device__descriptor.html", null ],
    [ "libusb_endpoint_descriptor", "structlibusb__endpoint__descriptor.html", null ],
    [ "libusb_interface", "structlibusb__interface.html", null ],
    [ "libusb_interface_descriptor", "structlibusb__interface__descriptor.html", null ],
    [ "libusb_iso_packet_descriptor", "structlibusb__iso__packet__descriptor.html", null ],
    [ "libusb_pollfd", "structlibusb__pollfd.html", null ],
    [ "libusb_transfer", "structlibusb__transfer.html", null ],
    [ "libusb_version", "structlibusb__version.html", null ],
    [ "NTrode", "class_n_trode.html", null ],
    [ "nTrodeStream", "classn_trode_stream.html", null ],
    [ "CocoaInitializer::Private", "class_cocoa_initializer_1_1_private.html", null ],
    [ "QGLWidget", null, [
      [ "scatterWindow", "classscatter_window.html", null ],
      [ "streamWidgetGL", "classstream_widget_g_l.html", null ]
    ] ],
    [ "QIODevice", null, [
      [ "Generator", "class_generator.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "fileSourceInterface", "classfile_source_interface.html", null ],
      [ "headerDisplayConfiguration", "classheader_display_configuration.html", null ],
      [ "NTrodeTable", "class_n_trode_table.html", null ],
      [ "simulateDataInterface", "classsimulate_data_interface.html", null ],
      [ "sourceController", "classsource_controller.html", null ],
      [ "SpikeConfiguration", "class_spike_configuration.html", null ],
      [ "streamConfiguration", "classstream_configuration.html", null ],
      [ "streamDisplayManager", "classstream_display_manager.html", null ],
      [ "USBDAQInterface", "class_u_s_b_d_a_q_interface.html", null ]
    ] ],
    [ "QThread", null, [
      [ "audioThread", "classaudio_thread.html", null ],
      [ "fileSourceRuntime", "classfile_source_runtime.html", null ],
      [ "recordThread", "classrecord_thread.html", null ],
      [ "simulateDataRuntime", "classsimulate_data_runtime.html", null ],
      [ "streamProcessor", "classstream_processor.html", null ],
      [ "trigger", "classtrigger.html", null ],
      [ "USBDAQRuntime", "class_u_s_b_d_a_q_runtime.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "ntrodeDisplayWidget", "classntrode_display_widget.html", null ],
      [ "scopeWindow", "classscope_window.html", null ],
      [ "soundDialog", "classsound_dialog.html", null ],
      [ "spikeDisplayWidget", "classspike_display_widget.html", null ],
      [ "triggerScopeDisplayWidget", "classtrigger_scope_display_widget.html", null ],
      [ "triggerScopeSettingsWidget", "classtrigger_scope_settings_widget.html", null ],
      [ "waveformGeneratorDialog", "classwaveform_generator_dialog.html", null ]
    ] ],
    [ "SingleSpikeTrodeConf", "class_single_spike_trode_conf.html", null ],
    [ "vertex2d", "structvertex2d.html", null ]
];