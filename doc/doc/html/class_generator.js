var class_generator =
[
    [ "Generator", "class_generator.html#a855bd3ed9e3b8b1ff6df9c5c1e1fcae2", null ],
    [ "~Generator", "class_generator.html#acc85fbe22690003267ba899bacf777d1", null ],
    [ "bytesAvailable", "class_generator.html#ab06a2f7a10bdcf3edd3fd0a11ac6d53f", null ],
    [ "bytesWaiting", "class_generator.html#ae0365b142ef480e8d0a1fac56340674b", null ],
    [ "checkForSamples", "class_generator.html#a7c4639220f4a2dcb63dcdf02339a63c2", null ],
    [ "readData", "class_generator.html#ac818d4583f77c0f29bbeafbb31995ff8", null ],
    [ "setChannel", "class_generator.html#a9547e25fa065729a32b39dfe0ea8189a", null ],
    [ "setRefChannel", "class_generator.html#a971d3034fccdc7383515a65257201251", null ],
    [ "start", "class_generator.html#a3f0bc6d7fed7eaf8d497d3e50149982b", null ],
    [ "stop", "class_generator.html#a698e9aa26989e005a709c297ebea3492", null ],
    [ "writeData", "class_generator.html#a16d82d88527ec5a0be4052b13d63417a", null ],
    [ "bufferLength", "class_generator.html#a88d1ca4e5c6f68c29d3f467eb2b2c015", null ],
    [ "currentValue", "class_generator.html#aaf54a28bc7c1a0123422ab6a8dfed409", null ],
    [ "filterObj", "class_generator.html#a003ae1af1ea6c12d08a309132305196a", null ],
    [ "filterOn", "class_generator.html#a4cd3ba69f187bd5b553860719379199b", null ],
    [ "inputFreqHz", "class_generator.html#ad67bc5df1191a1420008e1f535dd7fff", null ],
    [ "interpRatio", "class_generator.html#a52206c1ab27bdba74ecd18f04bb19100", null ],
    [ "listenChannel", "class_generator.html#a5dc5387af85f4ed20b1f27e1ef841a11", null ],
    [ "listenHWChannel", "class_generator.html#a097aaa1b06d6c84c70a8e404116a1007", null ],
    [ "listenRef", "class_generator.html#a3a7be4a2b3f6726c5f2804a82d9845a9", null ],
    [ "listenRefHWChan", "class_generator.html#ae5148944266186116b4592582cd11abb", null ],
    [ "m_buffer", "class_generator.html#ad74d57a7e649e786f3fbf608a7b0804f", null ],
    [ "masterGain", "class_generator.html#a1f2b8a91a384cf081f4be9321a374714", null ],
    [ "nextValue", "class_generator.html#ac2a282b410d376cb4c9cac9c88663be4", null ],
    [ "rawReadIdx", "class_generator.html#a05c39dd95e08566bb2cb599c19ae5ca3", null ],
    [ "readHead", "class_generator.html#aeebfe615c9113f4a2d94247113d69f71", null ],
    [ "soundDataRead", "class_generator.html#a022fac328afca9fb2b6edda810370a56", null ],
    [ "thresh", "class_generator.html#a30d14f2f85c0bca3eecf0da3b2d7f468", null ],
    [ "threshMicroVolts", "class_generator.html#a4e68563ee58f5096a85f066ec7536532", null ],
    [ "totalReadHead", "class_generator.html#a2c4231a80924803489ed9684e762028f", null ],
    [ "totalWriteHead", "class_generator.html#af1ada1dd7f5c96587ff6016623713575", null ],
    [ "writeHead", "class_generator.html#ad71998feef5fd8f88e92cfdb7aab1d9f", null ]
];