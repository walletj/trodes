var structlibusb__endpoint__descriptor =
[
    [ "bDescriptorType", "structlibusb__endpoint__descriptor.html#a609c257394a574af229293bddf26986e", null ],
    [ "bEndpointAddress", "structlibusb__endpoint__descriptor.html#a111d087a09cbeded8e15eda9127e23d2", null ],
    [ "bInterval", "structlibusb__endpoint__descriptor.html#a3194f3f04ebd860d59cbdb07d758f9d8", null ],
    [ "bLength", "structlibusb__endpoint__descriptor.html#aa47a5fa31c179e7cb92818c0572c18a3", null ],
    [ "bmAttributes", "structlibusb__endpoint__descriptor.html#a932b84417c46467f9916ecf7b679160b", null ],
    [ "bRefresh", "structlibusb__endpoint__descriptor.html#a9176a6d206a48731244e33a89a2bea0b", null ],
    [ "bSynchAddress", "structlibusb__endpoint__descriptor.html#ab8408ca33f4e135039b1900c6f50ce6d", null ],
    [ "extra", "structlibusb__endpoint__descriptor.html#a27a637679d4cd75fd6af5b8ea19a88d6", null ],
    [ "extra_length", "structlibusb__endpoint__descriptor.html#a0b34ea2919834df309eba8f2775491ae", null ],
    [ "wMaxPacketSize", "structlibusb__endpoint__descriptor.html#ac1feafc3d7310b2c8ab360513bcfa7b4", null ]
];