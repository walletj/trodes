var classtrigger =
[
    [ "trigger", "classtrigger.html#a2bcac928492ac9ed904b1ed9307a7980", null ],
    [ "~trigger", "classtrigger.html#ac23fa48c403e7fd41e5705e66f47b27c", null ],
    [ "addValue", "classtrigger.html#aaa7627c94c8fa5b43fc51b4cd4724595", null ],
    [ "endTrigger", "classtrigger.html#aa2ccf475083d04785a089cc05e0351cb", null ],
    [ "pullTimerExpired", "classtrigger.html#a3c833e3b6a23b8d8ce6e7fffd07bec06", null ],
    [ "run", "classtrigger.html#acd9ec241f7982398233994b30410a770", null ],
    [ "setThresh", "classtrigger.html#a392077802f5aa0abe803c7d36c63c32f", null ],
    [ "triggerEvent", "classtrigger.html#ac883e234a575bdaf1f1cc6342563e807", null ],
    [ "data", "classtrigger.html#ae3a92a87a6745a422a890409ac9d5070", null ],
    [ "nTrodeNum", "classtrigger.html#aa9cf65aa57cea63bea8dd5495f767d03", null ],
    [ "numChannels", "classtrigger.html#a2e34407cf07894d83edba12434cc4cf5", null ],
    [ "peaks", "classtrigger.html#abf96637ffdefae1b401c5ad59a59d3b4", null ],
    [ "pointsInWaveform", "classtrigger.html#aced378fed53cd7fa964859e1742e0839", null ],
    [ "pointsToRewind", "classtrigger.html#a6b55ccdb41b30986f036fb2411cdb4aa", null ],
    [ "pullTimer", "classtrigger.html#ac6a4ab702b630ad038325a2137777de7", null ],
    [ "waveForms", "classtrigger.html#a8e1fcfd2d3619783264045f805e1a8bd", null ]
];