var classstream_configuration =
[
    [ "streamConfiguration", "classstream_configuration.html#a5a552620ae766886d1a801387760cc3b", null ],
    [ "~streamConfiguration", "classstream_configuration.html#a81bbf814bd04464dde8c11b889bf0055", null ],
    [ "loadFromXML", "classstream_configuration.html#adf283c2515d6e1a788ed2ef1d515f037", null ],
    [ "saveToXML", "classstream_configuration.html#a4b1a0c4635a85c303f1e11a4bd908e18", null ],
    [ "setTLength", "classstream_configuration.html#a2d3a42679e3e745902e43feb38d79ca3", null ],
    [ "updatedTLength", "classstream_configuration.html#aaf3788f660c33aca720cdcabee83aef0", null ],
    [ "dataFilters", "classstream_configuration.html#a3f376f3f1b4d7e4e32d97dff2740c3c5", null ],
    [ "FS", "classstream_configuration.html#ab2741d4541fdd3424b2b1435d07507d4", null ],
    [ "nColumns", "classstream_configuration.html#aebee6042e914297e86116a9514d8aae2", null ],
    [ "tLength", "classstream_configuration.html#a8fb6b8aa33d47b1c77131497300695e2", null ],
    [ "trodeChannelLookup", "classstream_configuration.html#a81bd2f24b4326ac73c45aaa15087d474", null ],
    [ "trodeLookup", "classstream_configuration.html#a09938f69ba123a858c1c19f3fc35f6a1", null ]
];