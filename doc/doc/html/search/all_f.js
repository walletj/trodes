var searchData=
[
  ['scatterwindow',['scatterWindow',['../classscatter_window.html',1,'']]],
  ['scopewindow',['scopeWindow',['../classscope_window.html',1,'']]],
  ['simulatedatainterface',['simulateDataInterface',['../classsimulate_data_interface.html',1,'']]],
  ['simulatedataruntime',['simulateDataRuntime',['../classsimulate_data_runtime.html',1,'']]],
  ['singlespiketrodeconf',['SingleSpikeTrodeConf',['../class_single_spike_trode_conf.html',1,'']]],
  ['sounddialog',['soundDialog',['../classsound_dialog.html',1,'']]],
  ['sourcecontroller',['sourceController',['../classsource_controller.html',1,'']]],
  ['spikeconfiguration',['SpikeConfiguration',['../class_spike_configuration.html',1,'']]],
  ['spikedisplaywidget',['spikeDisplayWidget',['../classspike_display_widget.html',1,'']]],
  ['status',['status',['../structlibusb__iso__packet__descriptor.html#aab21ee2a5835a0e53d7ac5844ee34371',1,'libusb_iso_packet_descriptor::status()'],['../structlibusb__transfer.html#a64b2e70e76d52a7cd23daa3cd4fb397e',1,'libusb_transfer::status()']]],
  ['streamconfiguration',['streamConfiguration',['../classstream_configuration.html',1,'']]],
  ['streamdisplaymanager',['streamDisplayManager',['../classstream_display_manager.html',1,'']]],
  ['streamprocessor',['streamProcessor',['../classstream_processor.html',1,'']]],
  ['streamwidgetgl',['streamWidgetGL',['../classstream_widget_g_l.html',1,'']]]
];
