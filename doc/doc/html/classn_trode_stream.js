var classn_trode_stream =
[
    [ "nTrodeStream", "classn_trode_stream.html#a754b099de8f1e8a23621e34573b8b01e", null ],
    [ "~nTrodeStream", "classn_trode_stream.html#a0512c4a867c5505adce9d39830aefc80", null ],
    [ "findEvent", "classn_trode_stream.html#a90a24d09076f3d6c1b758a0696bbcfe3", null ],
    [ "readData", "classn_trode_stream.html#a61379e37d784ac5e1e6cb0e85c84aec9", null ],
    [ "setThresh", "classn_trode_stream.html#aaf056fd7aeb4790242192fa5afd80702", null ],
    [ "writeData", "classn_trode_stream.html#a2a4ce9026f3cfd5c4e66bbaacbabfcae", null ],
    [ "bufferSize", "classn_trode_stream.html#aa92c6f7977a5b51813d151f67ae28bb0", null ],
    [ "dataBuffer", "classn_trode_stream.html#aa9f39ae2431e624ee9557620a9c347bd", null ],
    [ "numChannels", "classn_trode_stream.html#a2c30f0cb57e2d4d23c61a1d18b2bccda", null ],
    [ "numSamplesBelowThresh", "classn_trode_stream.html#aac73ad9ce1775e97a929598165696449", null ],
    [ "readBufferPosition", "classn_trode_stream.html#a28c1f03d88220364de4f6b79d68dc76e", null ],
    [ "samplesRead", "classn_trode_stream.html#a7a6ac045730ee2bd204560f14c70a5d3", null ],
    [ "samplesWritten", "classn_trode_stream.html#a3da2ec6c3c08f54a1d77e95457841670", null ],
    [ "thresholds", "classn_trode_stream.html#a400caaacf13ed170b502bb0e44b24930", null ],
    [ "totalTriggerState", "classn_trode_stream.html#afa928f63667959e6801ef18be17e552a", null ],
    [ "triggerStates", "classn_trode_stream.html#a7ca410839cd2c919b9774127a522bfb8", null ],
    [ "writeBufferPosition", "classn_trode_stream.html#ad7aa53e1fbe570256fa4908fbe119240", null ]
];