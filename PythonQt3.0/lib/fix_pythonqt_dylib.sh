#!/bin/bash

# Path to directory containing dylibs (use 1st arg; if no arg, use PWD)
[ $# -eq 0 ] && echo "Must provide path to PythonQt dylibs." && exit 1

dylibdir=$1

[[ ! "$dylibdir" = /* ]] && \
    echo "Dylibdir ($dylibdir) must be an absolute path." && exit 1

# use debug_ext if provided (otherwise debug_ext will evaluate to the empty string)
[ $# -eq 2 ] && debug_ext=$2

# Test whether this dir contains the correct dylibs
[[ ! -e "$dylibdir/libPythonQt$debug_ext.dylib" || ! -e "$dylibdir/libPythonQt_QtAll$debug_ext.dylib" ]] && \
    echo "$dylibdir doesn't contain libPythonQt*.dylib files, aborting." && \
    exit 1

# Get internal libPythonQt library path from libPythonQt_QtAll.dylib
# (We need exact string match for 'install_name_tool -change ...', below)
lpq_lpath=`otool -L $dylibdir/libPythonQt_QtAll$debug_ext.dylib | grep -o "\S*libPythonQt$debug_ext\..*\.dylib"`
[ -z $lpq_lpath ] && \
    echo "Could not extract libPythonQt$debug_ext path from libPythonQt_QtAll$debug_ext dylib, aborting." && \
    exit 1

# update install name of each lib ( so that it will be linked correctly )
install_name_tool -id $dylibdir/libPythonQt$debug_ext.1.dylib $dylibdir/libPythonQt$debug_ext.dylib || exit 1
install_name_tool -id $dylibdir/libPythonQt_QtAll$debug_ext.1.dylib $dylibdir/libPythonQt_QtAll$debug_ext.dylib || exit 1

# update internal libary path of libPythonQt within libPythonQt_QtAll, so that it will run
install_name_tool -change $lpq_lpath @loader_path/libPythonQt$debug_ext.1.dylib $dylibdir/libPythonQt_QtAll$debug_ext.dylib || exit 1

# report success
echo "==> PythonQt dylib paths updated before build:"
## diagnostic output--show results
otool -L $dylibdir/libPythonQt$debug_ext{,_QtAll$debug_ext}.dylib |grep libPython
